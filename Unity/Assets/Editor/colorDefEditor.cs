﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[CustomEditor (typeof(DPcolorDef)), CanEditMultipleObjects] 
//this multiple edit is strange, for prefab instance, need to apply
//for scene, non prefab instance, need to save the scene
public class colorDefEditor : Editor
{

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		serializedObject.Update();
		//colorDef theTarget = (colorDef)target;
		DPcolorDef theTarget = (DPcolorDef)target;
		Image image = theTarget.gameObject.GetComponent<Image> ();
		switch (theTarget.color) {

		case DPColo.Red:
			image.color = new Color(1.0f,0.3f,0.15f);
			break;
		case DPColo.Blue:
			image.color = new Color(0.22f,0.85f,1.0f);
			break;
		case DPColo.Green:
			image.color = new Color(0.2f,1.0f,0.5f);
			break;
		default:
			image.color = new Color(1.0f,0.3f,0.15f);
			break;
		}

		serializedObject.ApplyModifiedProperties();
	}
}
