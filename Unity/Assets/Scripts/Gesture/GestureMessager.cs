﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//This emission of callback of gesture in and gesture out are in promiscuous mode,

//for each gesture, PtrEnter and PtrExit are paired occurring
public class GestureMessager
{
	public delegate void PtrCrossDele (PointerEventData data);

	public enum ReignType
	{
		OUTBOUND,
		MIDDLE,
		INNER,
		UNDEF
	}

	public enum MsgType
	{
		InEnter,
		InExit,
		OutEnter,
		OutExit
	}

	public ReignType CurrentReign{
		get{
			return currentReign;
		}
	}

	ReignType OnSetReign {
		set {

			if (currentReign == ReignType.UNDEF)
			if (value != ReignType.MIDDLE) {
				Debug.LogWarning ("Some assumption wrong, As first set Value should be from OutEnter");
			}

			currentReign = value;
		}
	}

	//logical reign is set after receive msg before invoke
	ReignType currentReign = ReignType.UNDEF;
	List<MsgContent> promiscuousFlood;
	Queue<MsgContent> ordered;
	MsgType anticpatedMsg;

	public GestureMessager ()
	{
		promiscuousFlood = new List<MsgContent> ();
		ordered = new Queue<MsgContent> ();
	}

	bool CyclicAnticipate (MsgContent cnt)
	{
		switch (CurrentReign) {

		case ReignType.UNDEF:
		case ReignType.OUTBOUND:
			if (cnt.MsgType == MsgType.OutEnter) {
				OnSetReign = ReignType.MIDDLE;
				return true;
			}
			break;

		case ReignType.MIDDLE:
			if (cnt.MsgType == MsgType.InEnter) {
				OnSetReign = ReignType.INNER;
				return true;
			}

			if (cnt.MsgType == MsgType.OutExit) {
				OnSetReign = ReignType.OUTBOUND;
				return true;
			}
			break;

		case ReignType.INNER:
			if (cnt.MsgType == MsgType.InExit) {
				OnSetReign = ReignType.MIDDLE;
				return true;
			}
			break;
		}

		return false;
	}


	public void Enqueue (MsgType msg, PtrCrossDele callback)
	{

		var tmp = new MsgContent (currentReign, msg, callback);
		promiscuousFlood.Add (tmp);

		while (true) {
			if (!ProcessSort ())
				break;
		}
	}

	bool ProcessSort ()
	{
		bool sthSorted = false;
		promiscuousFlood.FindAll (cnt => !cnt.Sorted).
		ForEach (unsorted => {

			if (CyclicAnticipate (unsorted)) {
				ordered.Enqueue (unsorted);
				unsorted.Sorted = true;
				sthSorted = true;
			}

		});

		promiscuousFlood.RemoveAll (x => x.Deletable);

		if (promiscuousFlood.Count > 8 || ordered.Count > 5) {
			Debug.LogWarning ("The queues are suspicious jammed");
		}

		return sthSorted;
	}

	public bool Invoke (PointerEventData data)
	{

		bool sthInvoked = false;

		while (ordered.Count > 0) {
			var theFirst = ordered.Dequeue ();
			theFirst.Invoke (data);
			sthInvoked = true;
		}

		return sthInvoked;
	}

	public class MsgContent
	{
		
		ReignType instantReign;
		MsgType msgType;
		PtrCrossDele callback;

		bool sorted, invoked;

		public bool Sorted {
			set {
				sorted = value;
			}
			get {
				return sorted;
			}
		}

		public bool Deletable {
			get {
				return (sorted && invoked);
			}
		}

		public ReignType Reign {
			get {
				return instantReign;
			}
		}

		public MsgType MsgType {
			get {
				return msgType;
			}
		}

		public MsgContent (ReignType _reign, MsgType _msg, PtrCrossDele _cb)
		{
			instantReign = _reign;
			msgType = _msg;
			callback = _cb;

			sorted = false;
			invoked = false;
		}

		public void Invoke (PointerEventData data)
		{

			callback.Invoke (data);
			invoked = true;
		}
	}

}


