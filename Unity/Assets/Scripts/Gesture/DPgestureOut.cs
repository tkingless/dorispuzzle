﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Tkingless.DorisPuzzle.constants;

public class DPgestureOut : DPgestureBase
{
	void Awake ()
	{
		specificAct = DPblock.ActType.ROTATE;
	}

	public override void OnPointerEnter (PointerEventData eventData)
	{
		parentBlk.massager.Enqueue (GestureMessager.MsgType.OutEnter, PtrEnterCB);
		parentBlk.massager.Invoke (eventData);
	}

	public override void OnPointerExit (PointerEventData eventData)
	{
		parentBlk.massager.Enqueue (GestureMessager.MsgType.OutExit, PtrExitCB);
		parentBlk.massager.Invoke (eventData);
	}

	public void PtrEnterCB (PointerEventData eventData)
	{
//		Debug.Log ("1 out, OnPointerEnter"+ parentBlk.name);
	}

	public void PtrExitCB (PointerEventData eventData)
	{
//		Debug.Log ("4 out, OnPointerExit"+ parentBlk.name);
		if (DPBlockManager.Selected == null) {
			return;
		}
		
		if (DPBlockManager.Selected != parentBlk) {
			return;
		}
		
		if (DPBlockManager.Selected.Free) {
			DPBlockManager.TriggerEvent (DPconstant.DP_GRP_PTR_LEAVE, parentBlk);
			DPBlockManager.TriggerEvent (DPconstant.DP_GSTR_RAD_PTR_LEAVE, parentBlk);
			DPBlockManager.TriggerEvent (DPconstant.DP_PTR_LEAVE_DEREG_BLK, parentBlk);
		
		}
	}
}
