﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Tkingless.DorisPuzzle.constants;

/// <summary>
/// Protocol to pointer gesture, entry point for handling user pointer input
/// </summary>
public class DPgestureIn : DPgestureBase
{
	void Awake(){
		specificAct = DPblock.ActType.TRANSLATE;
	}

	public override void OnPointerEnter (PointerEventData eventData)
	{
		parentBlk.massager.Enqueue (GestureMessager.MsgType.InEnter, PtrEnterCB);
		parentBlk.massager.Invoke (eventData);

	}

	//It is strange, but the fact that when dragging, both OnPointerEnter and OnPointerExit triggered ocassionally
	//while dragging
	public override void OnPointerExit (PointerEventData eventData)
	{
		parentBlk.massager.Enqueue (GestureMessager.MsgType.InExit, PtrExitCB);
		parentBlk.massager.Invoke (eventData);
	}


	public void PtrEnterCB (PointerEventData eventData)
	{
//		Debug.Log ("2 in, OnPointerEnter"+ parentBlk.name);
		
		if (DPBlockManager.Selected != null) {
			if (parentBlk.Free && parentBlk.IsSelected) {
				DPBlockManager.TriggerEvent (DPconstant.DP_GSTR_FREE_INZONE_ENTER);
			}
			return;
		}
		
		if (!parentBlk.IsSelected) {
			DPBlockManager.TriggerEvent (DPconstant.DP_REG_BLK, parentBlk);
		}
		
		//		Debug.Log ("OnPointerEnter detected: " + parentBlk.name);
	}

	public void PtrExitCB (PointerEventData eventData)
	{
//		Debug.Log ("3 in, OnPointerExit: " + parentBlk.name);
		//Strange case that, pointerExit triggered entered null object,
		//maybe it is Unity's design that user no using same monobehavior
		//to listen both drag and pointer exit simultaneously
		if (DPBlockManager.Selected == null) {
			return;
		}
		
		if (!parentBlk.IsSelected) {
			return;
		}
		//		Debug.Log ("GestureIn OnPointerExit detected: " +  parentBlk.name);

		if (parentBlk.Free) {
			DPBlockManager.TriggerEvent (DPconstant.DP_GSTR_FREE_INZONE_EXIT);
		}

	}
}
