﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Tkingless.DorisPuzzle.constants;

public class DPgestureBase : MonoBehaviour, IDPGestureHandler
{

	public DPblock parentBlk;

	protected DPblock.ActType specificAct = DPblock.ActType.UNDEF;

	public virtual void OnPointerEnter (PointerEventData eventData)
	{
	}

	public virtual void OnPointerExit (PointerEventData eventData)
	{
	}

	public virtual void OnPointerClick (PointerEventData eventData)
	{
	}

	public virtual void OnPointerDown (PointerEventData eventData)
	{
		DPBlockManager.TriggerEvent (DPconstant.DP_GSTR_PTR_DOWN);
	}

	//Sometimes BeginDrag detected after exit the zone, render BPBlockManager Selected null
	//you might tune the drag threshold of EventSystem, but can easily trigger drag in de-favor of multiple click
	public virtual void OnBeginDrag (PointerEventData eventData)
	{
		if (parentBlk != DPBlockManager.Selected) {
			return;
		}

		if (parentBlk.GetActType != specificAct)
			return;

		//		Debug.Log("DPGesture: OnBeginDrag");

		DPBlockManager.Selected.SetState (DPblock.BLK_STATE.Actioning);

		DPBlockManager.TriggerEvent (DPconstant.DP_GRP_DRAG_BGN);
		DPBlockManager.TriggerEvent (DPconstant.DP_GSTR_DRAG_BGN);

		//the fact that when dragging, both OnPointerEnter and OnPointerExit triggered ocassionally
		//while dragging
		parentBlk.SetCanvasBlockRay = false;

	}

	public virtual void OnDrag (PointerEventData eventData)
	{
		if (parentBlk.GetActType != specificAct)
			return;

		if (Input.touchCount > 1) {
			return;
		}

		DPBlockManager.TriggerEvent (DPconstant.DP_GSTR_DRAGGING);
	}

	public virtual void OnEndDrag (PointerEventData eventData)
	{
		if (parentBlk.GetActType != specificAct)
			return;
		//		Debug.Log("DPGesture: OnEndDrag");

		DPBlockManager.TriggerEvent (DPconstant.DP_GRP_DRAG_END);
		DPBlockManager.TriggerEvent (DPconstant.DP_GSTR_DRAG_END);

		parentBlk.SetCanvasBlockRay = true;
	}
}
