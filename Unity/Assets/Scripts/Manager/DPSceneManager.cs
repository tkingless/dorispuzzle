﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DPSceneManager : SceneSingleton<DPSceneManager>
{
	//target implement scene reload
	//target implement scene init

	protected override void Awake(){
		base.Awake ();
	}

	public static bool IsSlotScene ()
	{
		return GameObject.FindObjectOfType<SlotSceneMng> () != null;
	}

	public static bool IsPuzScene ()
	{
		return GameObject.FindObjectOfType (typeof(PuzzleSceneMng)) != null;
	}

	public static void ResetCurrentScene(){
		Scene loadedLevel = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (loadedLevel.buildIndex);
	}
}
