﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotSceneMng : DPSceneManager {

	public PuzzleSlot puzSlotObj;
	public DPgrpRadar grpForbiddenObj;
    public static bool Spawned = false;

    new void Awake() {
		
		base.Awake ();
		//Requires BlockManager, 24 Puzzle Mng on scene
		if (FindObjectOfType (typeof(DPBlockManager)) == null) {
			Debug.LogError ("Cannot find DPBlockManager");
			return;
		}

		if(FindObjectOfType (typeof(PuzzleSceneMng)) == null) {
			Debug.LogError ("Cannot find PuzzleSceneMng");
			return;
		}

		StartCoroutine ("WaitSpawn");
	}

	IEnumerator WaitSpawn (){

		while (!PuzzleSceneMng.Spawned) {
			yield return null;
		}

		var PuzOnScene = Instantiate (puzSlotObj, DPBlockManager.GetPlaygroundAnchor.parent);
		int siblingOrder = DPBlockManager.GetPlaygroundAnchor.GetSiblingIndex () + 1;
        var RT = PuzOnScene.GetComponent<RectTransform>();
        RT.SetSiblingIndex(siblingOrder);
        RT.localPosition = new Vector3(-Screen.width / 2 + 20, 0, 0);
        RT.localScale = Vector3.one;

		var GrpForbiddenOnScen = Instantiate (grpForbiddenObj, DPBlockManager.GetPlaygroundAnchor.parent);
		var RT2 = GrpForbiddenOnScen.GetComponent<RectTransform>();
		RT2.SetSiblingIndex(siblingOrder);
		RT2.localPosition = new Vector3(-Screen.width / 2 + 20, 0, 0);
		RT2.localScale = Vector3.one;

        Spawned = true;

        //PuzOnScene.transform.localPosition = new Vector3 (0,-18,0);
        //((RectTransform)PuzOnScene.transform).sizeDelta = new Vector2 (118,47.08f);
        //PuzOnScene.transform.localScale = Vector3.one;

		//Dr So requested
		if (true) {
			Transform puzSlotTrans = GameObject.FindObjectOfType<PuzzleSlot> ().transform;
			DPBlockManager.Instance.AllBlks.ForEach (x => {
				x.PretendOnSlotted();
				x.transform.SetParent (puzSlotTrans);
			});
		}

    }
}
