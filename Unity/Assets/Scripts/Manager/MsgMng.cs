﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MsgMng : SceneSingleton<MsgMng>
{
    public RectTransform messager_board;
    public RectTransform prefab_message;
    private List<DPMessage> collection = new List<DPMessage>();


    public void test()
    {
        MsgMng.Instance.show(new DPdiagloue("Function Not Ready!", DPMessage.STATE.danger));
    }

	float goodTimeInterval = 2.0f;
	float badTimeInterval = 2.0f;
	float goodTime = 2f;
	float badTime = 2f;
	void Update(){
		if (goodTime < goodTimeInterval)
			goodTime += Time.deltaTime;
		if (badTime < badTimeInterval)
			badTime += Time.deltaTime;
	}

	//tkingless/message: to avoid too frequent trigger
	public static void ShowGoodMsg(String textContent){
		if (Instance.goodTime >= Instance.goodTimeInterval) {
			Instance.show (new DPdiagloue (textContent, DPMessage.STATE.info));
			Instance.goodTime = 0f;
		}
	}

	public static void ShowBadMsg(String textContent){
		if (Instance.badTime >= Instance.badTimeInterval) {
			Instance.show (new DPdiagloue (textContent, DPMessage.STATE.danger));
			Instance.badTime = 0f;
		}
	}

    public void show(IDiagloue data)
    {
        DPMessage msg = createDPMessage();
        msg.state = data.state;
        msg.text = data.content;
        msg.duration = data.duration;
        msg.Enter();
        collection.Add(msg);
    }


    private DPMessage createDPMessage()
    {
        RectTransform t = Instantiate<RectTransform>(prefab_message, messager_board);
        {
            t.SetSiblingIndex(0);
            t.localPosition = Vector3.zero;
            t.localScale = Vector3.one;
        }
        return t.GetComponent<DPMessage>();
    }
}



public class DPdiagloue : IDiagloue
{
    private string _txt;
    private DPMessage.STATE _state;
    private float _duration;

    public DPdiagloue(string txt, DPMessage.STATE state, float duration = 2.0f)
    {
        this._txt = txt;
        this._state = state;
        this._duration = duration;
    }

    public string content
    {
        get
        {
            return this._txt;
        }
    }

    public float duration
    {
        get
        {
            return this._duration;
        }
    }

    public DPMessage.STATE state
    {
        get
        {
            return this._state;
        }
    }
}