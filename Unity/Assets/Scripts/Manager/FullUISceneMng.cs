﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class FullUISceneMng : DPSceneManager
{
    public Camera screen_cap_camera;
    public RectTransform screen_cap_form;
    public Image screen_cap_preview;
    public InputField screen_cap_email;

	//TODO tkingless/message: tmp only
	public static bool isFullUIScene = false;

    new void Awake()
    {
        base.Awake();

        //Requires SlotSceneMng
        if (FindObjectOfType(typeof(SlotSceneMng)) == null)
        {
            Debug.LogError("Cannot find SlotSceneMng");
            return;
        }
        cancelSCForm();
		isFullUIScene = true;
    }

    public void screenCapture()
    {

        /* Variable Declare */
        Texture2D screenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        RenderTexture rt = new RenderTexture(Screen.width, Screen.height, 24);

        /* Get Image From Camera */
        screen_cap_camera.targetTexture = rt;
        screen_cap_camera.Render();

        {
            RenderTexture.active = rt;
            {
                screenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
                screenShot.Apply();
                screen_cap_camera.targetTexture = null;
            }
            RenderTexture.active = null;
            Destroy(rt);
        }

        /* Preview Image */
        {
            Sprite sprite = Sprite.Create(screenShot, new Rect(0, 0, screenShot.width, screenShot.height), new Vector2(.5f, .5f));
            screen_cap_preview.sprite = sprite;
        }

        /* Form Show */
        screen_cap_form.gameObject.SetActive(true);
    }

    public void cancelSCForm()
    {
        screen_cap_form.gameObject.SetActive(false);
    }

    public void confirmSCForm()
    {
        /* Validate Email */
        Regex rx = new Regex(@"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
        if (!rx.IsMatch(screen_cap_email.text))
        {
            cancelSCForm();
            return;
        }

        /* Send Email */
        DPMailer.send(screen_cap_email.text);

        /* Close Form */
        cancelSCForm();
    }
}
