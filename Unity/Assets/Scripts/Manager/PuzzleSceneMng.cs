﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PuzzleSceneMng : DPSceneManager
{
	public GameObject[] ColorsPrefabs;
	public GameObject noColoBlkPrefab;

	//Bonus make this class as a singleton, at most one at a scene
	public static bool Spawned = false;

	new void Awake ()
	{
		base.Awake ();
		StartCoroutine ("SpawnBlks");
	}

	IEnumerator SpawnBlks ()
	{
		IEnumerator enumr = ColorsPrefabs.GetEnumerator ();
		int counter = 0;


		while (enumr.MoveNext ()) {

			var noColoBlk = Instantiate (noColoBlkPrefab, DPBlockManager.GetPlaygroundAnchor);
			((GameObject)noColoBlk).name += " " + counter;

			var coloGO = (GameObject) Instantiate ((GameObject)enumr.Current, noColoBlk.transform);
			noColoBlk.transform.localScale = new Vector3 (.7f, .7f, 1f);
			coloGO.transform.localScale = Vector3.one;
			int targetSiblingIndex = noColoBlk.transform.FindChild ("core").GetSiblingIndex () + 1;
			coloGO.transform.SetSiblingIndex (targetSiblingIndex);

			noColoBlk.transform.localPosition = new Vector3 (-150f + 75f * (counter % 5),
				150f - 75f * Mathf.FloorToInt(counter/5f),
				0f);

			if (counter != 3 && counter != 4 && counter != 8 && counter != 9) {
				noColoBlk.transform.localRotation = Quaternion.Euler (Vector3.forward * 45f);
			}
			
			counter++;

			if (counter == 12)
				counter++;
			
//			yield return null;

		}


		Spawned = true;

		//crossover with DPBlockManager
		DPBlockManager.SetAllBlks(((DPblock[])GameObject.
			FindObjectsOfType (typeof(DPblock))).
			ToList ());

		yield return null;
	}

	public void ResetScene(){
		DPSceneManager.ResetCurrentScene ();
	}
}


