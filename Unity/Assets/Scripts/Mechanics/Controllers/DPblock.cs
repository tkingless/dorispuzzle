﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tkingless.DorisPuzzle.constants;
using UnityEngine;
using UnityEngine.EventSystems;

/* TODO tkingless/ResRescaling: the UI elements and collider triggers are redrawn on screen Res rescaled
 * (1) the slot, safe net, grp radar all problematic when resolution changed
 * (2) blk are fixed pixel only
 */

/* TODO tkingless/Debug: if slotted, actioned but failed then retreat, the blk can stuck occasionally and not deregistered legally
 * need to click restart button, fatal bug
 */

//TODO tkingless/Improvement: two grouped blks have no interaction of matching

public class DPblock : MonoBehaviour
{
	//TODO tkingless/optimization  hide these states for those repetivie parentBlk referencing, be done at master branch, create Block skeleton class
	//Bonus create interface, implement OnState....OffState...
	public enum BLK_STATE
	{
		Free,
		Actioning,
		Engaged
	}

	public enum BLK_SUB_STATE
	{
		Slotted,
		Grouped,
		Default
	}

	public enum ActType
	{
		TRANSLATE,
		ROTATE,
		UNDEF
	}

	private BLK_STATE state = BLK_STATE.Free;
	private BLK_SUB_STATE subState = BLK_SUB_STATE.Default;
	private ActType actType = ActType.UNDEF;

	public bool Free {
		get {
			return state == BLK_STATE.Free;
		}
	}

	public bool Engaged {
		get {
			return (state == BLK_STATE.Engaged);
		}
	}

	public bool IsActioning {
		get {
			return state == BLK_STATE.Actioning;
		}
	}

	public bool IsSelected {
		get {
			return (this == DPBlockManager.Selected);
		}
	}

	public bool IsOverlapping {
		get {
			try {
				return boundObj.GetComponent<PolygonCollider2D> ().IsTouchingLayers (DPconstant.BOUND_LAYER_MASK);
			} catch (Exception e) {
				Debug.LogError (e);
			}
			//for safety
			return true;
		}
	}

	public BLK_SUB_STATE SubState {
		get {
			return subState;
		}
	}

	public bool SetCanvasBlockRay {
		set {
			GetComponent<CanvasGroup> ().blocksRaycasts = value;
		}
	}

	public BLK_SUB_STATE SetSubState {
		set {

			BLK_SUB_STATE prev = subState;

			if (value == prev) {
				Debug.LogWarning ("dangerous zone, repetitive setting same subState: " + value);
				return;
			}

			if (value != BLK_SUB_STATE.Default && prev != BLK_SUB_STATE.Default) {
				Debug.LogWarning ("transition between slotted and grouped not allowed");
				return;
			}

			subState = value;

			if (value == BLK_SUB_STATE.Default) {
				if (prev == BLK_SUB_STATE.Slotted) {
					OffSlotted ();
				}
				if (prev == BLK_SUB_STATE.Grouped) {
					OffGrouped ();
				}
			}

			if (value == BLK_SUB_STATE.Slotted) {
				OnSlotted ();
			}
			if (value == BLK_SUB_STATE.Grouped) {
				OnGrouped ();
			}
		}
	}

	public ActType GetActType {
		get {
			return actType;
		}
	}

	public ActType OnSetActType {
		set {

			if (actType == value) {
				Debug.LogWarning ("Already same Act..strange case: " + actType);
				return;
			}

			if (value == ActType.UNDEF) {
				DPBlockManager.StopListening (DPconstant.DP_GSTR_PTR_DOWN, ActiveAction.OnReady);
				DPBlockManager.StopListening (DPconstant.DP_GSTR_FREE_INZONE_ENTER, OnSwitchActToTrans); //activeAct is rotation, by ptr leave on deregister
				DPBlockManager.StopListening (DPconstant.DP_GSTR_FREE_INZONE_EXIT, OnSwitchActToRotate); //For overlap, safe back zone
				actType = value;
				return;
			}

			if (actType != ActType.UNDEF) {
				DPBlockManager.StopListening (DPconstant.DP_GSTR_PTR_DOWN, ActiveAction.OnReady);
			}

			actType = value;
			uiObj.OnSwitchAct (actType);

			if (actType == ActType.TRANSLATE) {
				DPBlockManager.StartListening (DPconstant.DP_GSTR_FREE_INZONE_EXIT, OnSwitchActToRotate);
				DPBlockManager.StopListening (DPconstant.DP_GSTR_FREE_INZONE_ENTER, OnSwitchActToTrans);
			}

			if (actType == ActType.ROTATE) {

				DPBlockManager.StartListening (DPconstant.DP_GSTR_FREE_INZONE_ENTER, OnSwitchActToTrans);
				DPBlockManager.StopListening (DPconstant.DP_GSTR_FREE_INZONE_EXIT, OnSwitchActToRotate);

			}

			DPBlockManager.StartListening (DPconstant.DP_GSTR_PTR_DOWN, ActiveAction.OnReady);
		}
	}

	public DPaction ActiveAction {
		get {
			if (actType == ActType.TRANSLATE)
				return GetComponent<DPtranslateFree> ();

			if (actType == ActType.ROTATE)
				return GetComponent<DProtationFree> ();

			return null;
		}
	}

	public DPUIeffect uiObj;
	public DPradar radObj;
	public DPgrpRadar grpRadObj;
	/// <summary>
	/// The DPboundary object
	/// </summary>
	public DPboundary boundObj;
	public DPgestureIn gestureInObj;
	public DPgestureOut gestureOutObj;
	public GestureMessager massager;
	public Transform grpDragHolder;
	GroupChecker grpSelector;

	//Design: when a GroupCheck updated, old must be destroyed by this procedure explained
	public GroupChecker SetGrpSelector {
		set {

			if (grpSelector != null) {
				grpSelector.Term ();
			}

			if (value != null) {
				value.EnableGrpBtnListening ();
			} else {
				if (grpSelector == null) {
					//case: actions that deregister with no connection state change
				}
			}

			grpSelector = value;
		}
	}

	public bool GrpSelectorNull {
		get {
			return grpSelector == null;
		}
	}

	void Start ()
	{
		gameObject.AddComponent<DPtranslateFree> ();
		gameObject.AddComponent<DProtationFree> ();	
		massager = new GestureMessager ();

		//tmp workaround
		transform.FindChild ("GroupDragHolder").SetAsFirstSibling ();
	}

	#region On State Change callbacks

	public void OnRegister ()
	{
//		Debug.Log ("OnRegister");
		TDflowCtrl.Vapourize (this);
		TDflowCtrl.ManUpdate ();

		uiObj.OnSelected (gestureInObj.transform);

		new GroupChecker (this);

		//workaround: deal with the PuzzleSlot grid layout group flashing ordering problem
		if (subState != BLK_SUB_STATE.Slotted) {
			transform.SetAsLastSibling ();
		}
			
		if (SubState != BLK_SUB_STATE.Grouped) {
			radObj.InitSelRadared ();
		}

		OnSwitchActToTrans ();

		//workaround
		//Jacky/masterF: cancel this
//		if (DPSceneManager.IsSlotScene ()) {
//			if (SubState == BLK_SUB_STATE.Slotted) {
//				DPBlockManager.GetDragAnchor.
//			GetComponent<UnityEngine.UI.Mask> ().enabled = false;
//			}
//		}
	}

	//Selected input is no use, but coupled with JOB_DONE event, not to delete TODO tkingless/Optimization: really valid still?
	public void OnDeregister (DPblock selected)
	{
//		Debug.Log ("OnDeregister");
		if (SubState != BLK_SUB_STATE.Grouped) {
			radObj.TermSelRadared ();
		}
			
		OnSetActType = ActType.UNDEF;

		uiObj.OnDeselected ();

		TDflowCtrl.Condense (this);
		TDflowCtrl.ManUpdate ();

		//workaround
		//Jacky/masterF: cancel this
//		if (DPSceneManager.IsSlotScene ()) {
//			DPBlockManager.GetDragAnchor.
//			GetComponent<UnityEngine.UI.Mask> ().enabled = true;
//		}
	}

	void OnSwitchActToTrans ()
	{
		OnSetActType = ActType.TRANSLATE;
	}

	void OnSwitchActToRotate ()
	{
		if (SubState != BLK_SUB_STATE.Default)
			return;
		
		OnSetActType = ActType.ROTATE;
	}

	void OnEngaged (DPblock engagedBlk)
	{

		if (engagedBlk != this) {
			return;
		}

		if (radObj == null) {
			Debug.LogError ("null radObj");
			return;
		}

		radObj.OnEngaged (engagedBlk);
	}

	void OnDegaged (DPblock engagedBlk)
	{

		if (engagedBlk != this) {
			return;
		}

		radObj.OnDegaged (engagedBlk);

		//workaround: overwritting for slotted case but overlapping
		if (DPBlockManager.Selected != null)
		if (DPBlockManager.Selected.subState == BLK_SUB_STATE.Slotted) {
			DisableEngagedListening ();
			state = BLK_STATE.Free;
			return;
		}

		if (IsOverlapping) {
			Debug.LogError ("On Degaged, but still overlapping");
//			return;
		}

		DisableEngagedListening ();
		state = BLK_STATE.Free;
	}

	public void PretendOnSlotted(){
		radObj.gameObject.SetActive (false);
		boundObj.isSlotted = true;
		subState = BLK_SUB_STATE.Slotted;
	}

	void OnSlotted ()
	{
		//Cleaning all trigger data inside
		//================================
		if (IsOccupied) {
			GetActiveTDs.ForEach (actTD => {
				actTD.OnSlotUrgentPurge ();
			});
		}

		//tkingless/Invalidation: need to purge Invalid touch too
		AllTrigPurgeWrgTches ();
		//bonus tkingless/Debug: somehow, need to purge topPrioirties too?
			
		TDflowCtrl.SlottedPurge (this);

		//workaround
		//Jacky/masterF: cancel this
//		if (DPSceneManager.IsSlotScene ()) {
//			DPBlockManager.GetDragAnchor.
//			GetComponent<UnityEngine.UI.Mask> ().enabled = false;
//		}

		ActiveAction.OnSlotted ();
		radObj.OnSlotted ();
		boundObj.OnSlotted ();
		uiObj.OnSlotted ();
	}

	void OffSlotted ()
	{
		//workaround
		//Jacky/masterF: cancel this
//		if (DPSceneManager.IsSlotScene ()) {
//			DPBlockManager.GetDragAnchor.
//			GetComponent<UnityEngine.UI.Mask> ().enabled = true;
//		}

		ActiveAction.OffSlotted ();
		radObj.OffSlotted ();
		boundObj.OffSlotted ();
		uiObj.OffSlotted ();
	}

	void OnGrouped ()
	{
		if (IsSelected) {
			radObj.OnGrouped ();
		}
		//no finally wanted
		boundObj.gameObject.GetComponent<PolygonCollider2D> ().enabled = false;
		uiObj.OnGrouped ();
	}

	void OffGrouped ()
	{
		if (IsSelected && Free) {
			radObj.OffGrouped ();
		}
		boundObj.gameObject.GetComponent<PolygonCollider2D> ().enabled = true;
		uiObj.OffGrouped ();
	}

	void OnJobDone (DPblock selected)
	{
		switch (state) {
		case BLK_STATE.Actioning:
			DisableActionListening ();
			state = BLK_STATE.Free;
			break;
		default:
			break;
		}

		DPBlockManager.StopListening (DPconstant.DP_JOB_DONE, OnJobDone);

		#region grouping post check
		DPBlockManager.TriggerEvent (DPconstant.DP_GRP_JOB_DONE);
		//tkingless/Grouping call new GroupChecker(this), and see if need to toggle DetachGO, competition: jobdone or ptr leave quickier? jobdone
		if (SubState != BLK_SUB_STATE.Slotted) {
			//order important
			uiObj.RespawnCursor ();
			new GroupChecker (this);
		} else {
			SetGrpSelector = null;
		}
		#endregion

		#region post check
		if (actType == ActType.ROTATE)
		if (massager.CurrentReign == GestureMessager.ReignType.INNER) {
			OnSwitchActToTrans ();
		}

		if (actType == ActType.TRANSLATE)
		if (massager.CurrentReign == GestureMessager.ReignType.MIDDLE) {
			OnSwitchActToRotate ();
		}

		if (massager.CurrentReign == GestureMessager.ReignType.OUTBOUND) {
			//Design: if slotted, DP_GSTR_RAD_PTR_LEAVE has no effect on DPradar virtually
			DPBlockManager.TriggerEvent (DPconstant.DP_GRP_PTR_LEAVE, this);
			DPBlockManager.TriggerEvent (DPconstant.DP_GSTR_RAD_PTR_LEAVE, this);
			DPBlockManager.TriggerEvent (DPconstant.DP_PTR_LEAVE_DEREG_BLK, this);
		}
		#endregion


	}

	//Desgin: 09jpae handling active control of active GroupChecker, interface for GroupChecker
	public void HandleToggleBtn ()
	{
		if (uiObj != null) {
			if (IsOccupied) {
				uiObj.ShowDetachGOToggle (true);
			} else {
				uiObj.ShowDetachGOToggle (false);
			}
		} else {
			Debug.LogWarning ("uiObj not supposed null");
		}
	}

	//set state, when called, all blks should be not listening anything
	public void SetState (BLK_STATE s)
	{

		if (state == BLK_STATE.Free) {
			
			switch (s) {
			case BLK_STATE.Actioning:
				EnableActionListening ();
				DPBlockManager.StartListening (DPconstant.DP_JOB_DONE, OnJobDone);
				break;
			case BLK_STATE.Engaged:
				EnableEngagedListening ();
				break;
			default:
				break;
			}
			state = s;
		}
	}

	#endregion

	#region handle event listener

	void EnableActionListening ()
	{
		DPBlockManager.StartListening (DPconstant.DP_GSTR_DRAG_BGN, ActiveAction.OnActionInit);
		DPBlockManager.StartListening (DPconstant.DP_GSTR_DRAGGING, ActiveAction.OnActionProc);
		DPBlockManager.StartListening (DPconstant.DP_GSTR_DRAG_END, ActiveAction.OnActionEnd);

		DPBlockManager.StartListening (DPconstant.DP_GSTR_DRAG_BGN, uiObj.OnBeginDrag);
		DPBlockManager.StartListening (DPconstant.DP_GSTR_DRAG_END, uiObj.OnEndDrag);

		DPBlockManager.StartListening (DPconstant.DP_OVERLAP_ON, uiObj.OnOverlap);
		DPBlockManager.StartListening (DPconstant.DP_OVERLAP_OFF, uiObj.OffOverlap);
		DPBlockManager.StartListening (DPconstant.DP_OVERLAP_ON, ActiveAction.OnOverlap);
		DPBlockManager.StartListening (DPconstant.DP_OVERLAP_OFF, ActiveAction.OffOverlap);
	}

	void DisableActionListening ()
	{
		DPBlockManager.StopListening (DPconstant.DP_GSTR_DRAG_BGN, ActiveAction.OnActionInit);
		DPBlockManager.StopListening (DPconstant.DP_GSTR_DRAGGING, ActiveAction.OnActionProc);
		DPBlockManager.StopListening (DPconstant.DP_GSTR_DRAG_END, ActiveAction.OnActionEnd);

		DPBlockManager.StopListening (DPconstant.DP_GSTR_DRAG_BGN, uiObj.OnBeginDrag);
		DPBlockManager.StopListening (DPconstant.DP_GSTR_DRAG_END, uiObj.OnEndDrag);

		DPBlockManager.StopListening (DPconstant.DP_OVERLAP_ON, uiObj.OnOverlap);
		DPBlockManager.StopListening (DPconstant.DP_OVERLAP_OFF, uiObj.OffOverlap);
		DPBlockManager.StopListening (DPconstant.DP_OVERLAP_ON, ActiveAction.OnOverlap);
		DPBlockManager.StopListening (DPconstant.DP_OVERLAP_OFF, ActiveAction.OffOverlap);
	}

	void EnableEngagedListening ()
	{
		DPBlockManager.StartListening (DPconstant.DP_RADAR_ON, OnEngaged);

		DPBlockManager.StartListening (DPconstant.DP_RADAR_OFF, OnDegaged);
	}

	void DisableEngagedListening ()
	{
		DPBlockManager.StopListening (DPconstant.DP_RADAR_ON, OnEngaged);

		DPBlockManager.StopListening (DPconstant.DP_RADAR_OFF, OnDegaged);
	}

	#endregion

	#region Block Interaction accessors

	public List<DPtrigger> GetDPTriggs {
		get {
			List<DPtrigger> list = new List<DPtrigger> ();

			foreach (DPtrigger t in gameObject.GetComponentsInChildren<DPtrigger>()) {
				list.Add (t);
			}
			return list;
		}
	}

	public bool IsOccupied {
		get {
			return GetDPTriggs.Find (x => x.IsOccupied) ? true : false;
		}
	}

	public List<DPblock> GetNeighborOccupiedBlks {
		get {
			HashSet<DPblock> tmp = new HashSet<DPblock> ();

			GetDPTriggs.FindAll (trig => trig.IsOccupied).
			ForEach (occupiedTrig => {
				occupiedTrig.ActiveTD.GetOppTrigs (occupiedTrig.Trig).
				ForEach (oppTrig => {
					tmp.Add (oppTrig.DPobj.parentBlk);
				});
			});

			if (tmp.Count == 0)
				return null;

			return tmp.ToList ();
		}
	}

	public List<ValidTouchData> GetTopPriorities {
		get {
			HashSet<ValidTouchData> tmp = new HashSet<ValidTouchData> ();

			GetDPTriggs.ForEach (x => {
				if (x.TopPriority != null) {
					tmp.Add (x.TopPriority);
				}
			});

			return tmp.ToList ();
		}
	}

	public List<ValidTouchData> GetActiveTDs {
		get {
			HashSet<ValidTouchData> tmp = new HashSet<ValidTouchData> ();

			GetDPTriggs.ForEach (x => {
				if (x.IsOccupied) {
					tmp.Add (x.Trig.ActiveTD);
				}
			});

			return tmp.ToList ();
		}
	}

	public ValidTouchData GetClosestTchTD {
		get {

			List<ValidTouchData> tmp = GetTopPriorities;

			if (tmp.Count > 0) {
				var tmpList = tmp.OrderBy (x => x.MinDistance ());
				return tmpList.First ();
			}

			return null;
		}
	}

	//Use: from perspectives of selected only
	public bool IsConnectedto (DPblock oppBlk)
	{

		if (GetActiveTDs.Any (x => x.GetEngagedBlk == oppBlk)) {
			return true;
		}
		return false;
	}

	public bool IsAllTrigPurgeWrgTches {
		get {
			return GetDPTriggs.TrueForAll (dpTrig => dpTrig.Trig.AllWrongTchesClear);
		}
	}

	public bool IsInvalidColoTched {
		get {
			bool found = false;
			GetDPTriggs.SelectMany (dpTrig => dpTrig.Trig.WrongTches).
			ToList ().ForEach (wrgTch => {
				if (!found)
				if ((wrgTch.invalidType & InvalidTouchType.COLOR) != 0) {
					found = true;
				}
			});
			return found;
		}
	}

	//Not used
	public bool IsInvalidGeomTched {
		get {
			bool found = false;
			GetDPTriggs.SelectMany (dpTrig => dpTrig.Trig.WrongTches).
			ToList ().ForEach (wrgTch => {
				if (!found)
				if ((wrgTch.invalidType & InvalidTouchType.GEOMETRY) != 0) {
					found = true;
				}
			});
			return found;
		}
	}

	public bool IsMonoConnectedNoverlap {
		get {
			return IsOverlapping && GetActiveTDs.Count == 1;
		}
	}

	#endregion

	public void AllTrigPurgeWrgTches ()
	{
		GetDPTriggs.ForEach (dpTrig => {
			if (!dpTrig.Trig.AllWrongTchesClear) {
				dpTrig.Trig.PurgeAllWrongTches ();
			}
		});
	}

	public void OnPause_ActionEndDrag ()
	{
		//order is important
		DPBlockManager.TriggerEvent (DPconstant.DP_GRP_DRAG_END);
		DPBlockManager.TriggerEvent (DPconstant.DP_GSTR_DRAG_END);
		SetCanvasBlockRay = true;
	}
		
}
