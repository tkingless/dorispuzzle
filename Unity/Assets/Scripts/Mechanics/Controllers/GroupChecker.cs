﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Tkingless.DorisPuzzle.constants;

//Design: only when a selected blk holds a GroupChecker, it can enter Grouped substate
//and must enter back default state before release a GroupChecker
public class GroupChecker
{
	DPblock selected;
	//configuration for whether grouped
	static bool GroupedWhenOccupied = true;

	public GroupChecker (DPblock _selected)
	{
		selected = _selected;

		Init ();
		selected.HandleToggleBtn ();
	}

	HashSet<DPblock> groupList;

	void Init ()
	{
		if (selected.IsOccupied) {
			SearchBlksInGrp ();
			selected.SetGrpSelector = this;

			if (GroupedWhenOccupied) {
				ActivateGrouping ();
			}
		}
	}

	public void Term ()
	{
		DisableGrpBtnListening ();
		if (selected.SubState == DPblock.BLK_SUB_STATE.Grouped) {
			DeactivateGrouping ();
		} else {
			//case that selected blk detached from grouped state, and onJobDone, new GroupChecker replaced it
			//still ungrouped...
		}
	}

	public List<DPblock> SearchBlksInGrp ()
	{
		groupList = new HashSet<DPblock> ();
		NodeSearch (groupList, selected);
		groupList.Remove (selected);

		return groupList.ToList ();
	}

	//Design: logically the callback order is OnActioninit->OnActionEnd->OnJobDone->OnPtrLeave
	public void EnableGrpBtnListening ()
	{
		//Desgin: 09jpae handling passive event of active GroupChecker
		DPBlockManager.StartListening (DPconstant.DP_BTN_GRP_CLK, OnGrpBtnClick);
		DPBlockManager.StartListening (DPconstant.DP_GRP_DRAG_BGN, OnActionInit);
		DPBlockManager.StartListening (DPconstant.DP_GRP_DRAG_END, OnActionEnd);
		DPBlockManager.StartListening (DPconstant.DP_GRP_JOB_DONE, OnJobDone);
		DPBlockManager.StartListening (DPconstant.DP_GRP_PTR_LEAVE, OnPtrLeave);

	}

	public void DisableGrpBtnListening ()
	{
		DPBlockManager.StopListening (DPconstant.DP_BTN_GRP_CLK, OnGrpBtnClick);
		DPBlockManager.StopListening (DPconstant.DP_GRP_DRAG_BGN, OnActionInit);
		DPBlockManager.StopListening (DPconstant.DP_GRP_DRAG_END, OnActionEnd);
		DPBlockManager.StopListening (DPconstant.DP_GRP_JOB_DONE, OnJobDone);
		DPBlockManager.StopListening (DPconstant.DP_GRP_PTR_LEAVE, OnPtrLeave);
	}

	void OnGrpBtnClick (DPblock _selected)
	{

		if (this.selected != _selected)
			return;

		if (selected.SubState == DPblock.BLK_SUB_STATE.Default) {
			ActivateGrouping ();
		} else if (selected.SubState == DPblock.BLK_SUB_STATE.Grouped) {
			DeactivateGrouping ();
		}
	}

	void NodeSearch (HashSet<DPblock> path, DPblock node)
	{

		if (!path.Contains (node))
			path.Add (node);
		else
			return;

		node.GetNeighborOccupiedBlks.ForEach (neighb => {
			NodeSearch (path, neighb);
		});
	}

	void SetAllGrouped (bool val)
	{
		DPblock.BLK_SUB_STATE toSet = val ? DPblock.BLK_SUB_STATE.Grouped : DPblock.BLK_SUB_STATE.Default;
		selected.SetSubState = toSet;
		groupList.ToList ().ForEach (x => {
			x.SetSubState = toSet;
		});
	}

	void SetEngagedParentTo (Transform parent)
	{
		groupList.ToList ().ForEach (x => {
			x.transform.SetParent (parent);
		});
	}

	public void ActivateGrouping ()
	{
		if (selected.SubState == DPblock.BLK_SUB_STATE.Default) {
			SetAllGrouped (true);
			ActivateGrpRadars ();
			DPBlockManager.StartListening (DPconstant.DP_GRP_ACTIONING_ON_FORBIDDEN, OnGroupActioningForbidden);
			DPBlockManager.StartListening (DPconstant.DP_GRP_ACTIONING_OFF_FORBIDDEN, OffGroupActioningForbidden);
		} else {
			Debug.LogWarning ("repeated call ActivateGrouping()");
		}
	}

	public void DeactivateGrouping ()
	{
		if (selected.SubState == DPblock.BLK_SUB_STATE.Grouped) {
			DPBlockManager.StopListening (DPconstant.DP_GRP_ACTIONING_ON_FORBIDDEN, OnGroupActioningForbidden);
			DPBlockManager.StopListening (DPconstant.DP_GRP_ACTIONING_OFF_FORBIDDEN, OffGroupActioningForbidden);
			DeactivateGrpRadars ();
			SetAllGrouped (false);
		} else {
			Debug.LogWarning ("repeated call DeactivateGrouping()");
		}
	}

	void ActivateGrpRadars ()
	{
		selected.grpRadObj.Setup (DPgrpRadar.Role.Grouped);

		groupList.ToList ().ForEach (x => {
			x.grpRadObj.Setup(DPgrpRadar.Role.Grouped);
		});
	}

	void DeactivateGrpRadars ()
	{
		groupList.ToList ().ForEach (x => {
			x.grpRadObj.Setup(DPgrpRadar.Role.Obstabcle);
		});

		selected.grpRadObj.Setup (DPgrpRadar.Role.Obstabcle);
	}

	void OnActionInit ()
	{
		if (selected.SubState == DPblock.BLK_SUB_STATE.Grouped) {
			SetEngagedParentTo (selected.grpDragHolder);
		}
	}

	HashSet<DPblock> forbiddens = new HashSet<DPblock> ();
	void OnGroupActioningForbidden(DPblock blk){
		forbiddens.Add (blk);
		if (forbiddens.Count > 0) {
			groupList.ToList ().ForEach (x => x.uiObj.OnGroupedForbidden ());
		}
	}

	void OffGroupActioningForbidden(DPblock blk){
		forbiddens.Remove (blk);
		if (forbiddens.Count == 0) {
			groupList.ToList ().ForEach (x => x.uiObj.OffGroupedForbidden ());
		}
	}

	void OnActionEnd ()
	{
		if (selected.SubState == DPblock.BLK_SUB_STATE.Grouped) {
			if (forbiddens.Count > 0) {
				selected.grpRadObj.Clear ();
				groupList.ToList ().ForEach (x => x.grpRadObj.Clear ());
				selected.ActiveAction.OnGroupForbidden ();
			}
		}
	}

	void OnJobDone ()
	{
		if (selected.SubState == DPblock.BLK_SUB_STATE.Grouped) {
			SetEngagedParentTo (DPBlockManager.GetPlaygroundAnchor);
			//workaround as uncontrolled engaged go back to PlaygroundAnchor
			selected.transform.SetAsLastSibling ();

		}
	}

	void OnPtrLeave (DPblock DPBlkselected)
	{
		if (selected != DPBlkselected)
			return;

		//bonus: tkingless/Grouping: workaround, pre off the halo for non selected grouped
		//better this logic should be put into individual DPblk offGrouped()
		groupList.ToList ().ForEach (x => {
			x.uiObj.PtrOffGrouped ();
		});

		selected.SetGrpSelector = null;
	}

}