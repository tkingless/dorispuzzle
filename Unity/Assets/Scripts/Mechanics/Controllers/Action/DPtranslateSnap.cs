﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public sealed class DPtranslateSnap : DPaction
{

	Vector3 transInitPos;
	Vector3 offsetToMouse;
	float zDistToCam;

	//Monobehavior callback declared on subclass will ignore ones in base class


	public override void OnActionInit ()
	{
		base.OnActionInit ();
		transInitPos = transform.position;

		zDistToCam = Mathf.Abs (transInitPos.z - Camera.main.transform.position.z);

		offsetToMouse = transInitPos - Camera.main.ScreenToWorldPoint (
			new Vector3 (Input.mousePosition.x, Input.mousePosition.y, zDistToCam)
		);

	}

	public override void OnActionProc ()
	{
		base.OnActionProc ();

		transform.position = Camera.main.ScreenToWorldPoint (
			new Vector3 (Input.mousePosition.x, Input.mousePosition.y, zDistToCam)
		) + offsetToMouse;
	}

	public override void OnActionEnd ()
	{
		base.OnActionEnd ();

		offsetToMouse = Vector3.zero;
		state = ActionState.terminate;
	}
		
}
