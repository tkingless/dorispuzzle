﻿using Tkingless.DorisPuzzle.constants;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DProtationFree : DPaction
{

	float linearSpan;
	float rotationZoffset;
	const int octagonAngle = 360 / DPconstant.POLYGON_SIDES;
	//set how (striaght line)/(circle) to rotate
	bool rotateLikeWheel = true;

	void Start ()
	{
		linearSpan = Camera.main.pixelHeight / 2;
	}

	float rotationDirection = 1.0f;

	public override void OnActionInit ()
	{
//		Debug.Log("DProtationSnap: OnActionInit");
		base.OnActionInit ();

		rotationZoffset = transform.rotation.eulerAngles.z;

		float argumentDiff = GetArgumentDiff ();
		if (argumentDiff > 0) {
			rotationDirection = 1.0f;

		} else {
			rotationDirection = -1.0f;
		}
	}

	float dragToRotateFactor = 1.0f;
	Vector2 dragVec;

	public override void OnActionProc ()
	{
		base.OnActionProc ();

		dragVec = TruncV3toV2 (Input.mousePosition) - dragInitPos;
		//Debug.Log ("OnDrag, position: " + position);

		//Want to make when |position| = linearSpan, the transform rotate 2Pi, also directional
		float rotationClockwiseMag = Vector2.Dot (dragAnchorNormal, dragVec);

		Quaternion rotateQ;
		if (rotateLikeWheel) {
			rotateQ = Quaternion.Euler (0, 0, rotationZoffset + GetArgumentDiff (false));
		} else {
			rotateQ = Quaternion.Euler (0, 0, rotationZoffset + GetArgumentDiff (false) + 360.0f * rotationDirection * rotationClockwiseMag * dragToRotateFactor / linearSpan);
		}
		transform.rotation = Quaternion.Slerp (transform.rotation, rotateQ, 1);
	}

	public override void OnActionEnd ()
	{
		base.OnActionEnd ();

		dragVec = Vector2.zero;
		rotationZoffset = transform.rotation.eulerAngles.z;
		//Debug.Log ("rotationZoffset: " + rotationZoffset);

		state = ActionState.terminate;

	}

	void SnapToClosestAngle ()
	{
		int endAngle = Mathf.RoundToInt (rotationZoffset);
		if (endAngle <= 0) {
			endAngle += 360;
		}
		int remainder = Mathf.Abs (endAngle) % octagonAngle;
		//Debug.Log ("remainder: " + remainder + ", endAngle: " + endAngle + ", octagonAngle: " + octagonAngle);
		//Debug.Log("result angle: " + octagonAngle * (Mathf.Abs (endAngle) / octagonAngle));

		Quaternion rotateQ;
		rotateQ = Quaternion.Euler (0, 0, octagonAngle * (Mathf.Abs (endAngle) / octagonAngle));
		transform.rotation = rotateQ;

		if (remainder > octagonAngle / 2) {
			transform.Rotate (Vector3.forward * octagonAngle);
		}
	}

	Vector2 rotateDirectionInit, rotateDirectionTail;

	float GetArgumentDiff (bool infinitesimal = true)
	{
		rotateDirectionInit = dragInitPos - orig;
		rotateDirectionInit.Normalize ();

		rotateDirectionTail = TruncV3toV2 (Input.mousePosition) - orig;
		rotateDirectionTail.Normalize ();

		float psudoDiff = aTanToDegree (rotateDirectionTail.y, rotateDirectionTail.x) - aTanToDegree (rotateDirectionInit.y, rotateDirectionInit.x);

		if (infinitesimal) {
			if (psudoDiff > 180f) {
				psudoDiff = 360 - psudoDiff;
			} 

			if (psudoDiff < -180f) {
				psudoDiff = 360 + psudoDiff;
			}
		}

		return psudoDiff;
	}

}
