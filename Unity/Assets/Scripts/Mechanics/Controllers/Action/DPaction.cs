﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.EventSystems;
using Tkingless.DorisPuzzle.constants;


public class DPaction : MonoBehaviour
{
	protected enum ActionState
	{
		init,
		proc,
		end,
		terminate,
		silent
	}

	[System.Flags]
	protected enum HamperedFlag
	{
		NONE = 0,
		OVERLAP = 1,
		INVALID_COLO_TOUCH = 2,
		INVALID_GEOM_TOUCH = 4,
		SLOTTED = 8,
		UNSLOTTED = 16,
		OUTBOUND = 32,
		GRP_FORBIDEEN = 64,

		UNSLOTTEDBUTOVERLAP = OVERLAP | UNSLOTTED,
		INVALID_TCHES = INVALID_COLO_TOUCH | INVALID_GEOM_TOUCH,
		ALL = OVERLAP | INVALID_COLO_TOUCH | INVALID_GEOM_TOUCH | NONE | SLOTTED
	}


	Vector3 safeInitPos = Vector3.zero;
	Quaternion safeInitRot = Quaternion.identity;
	//workaround for distinguish slot-playground / plaground-playround transition
	Transform prevAnchor;

	protected ActionState state = ActionState.silent;
	protected HamperedFlag hFlags = HamperedFlag.NONE;

	protected ActionState State {
		get {
			return state;
		}
	}

	protected DPblock parentBlk {
		get {
			return GetComponent<DPblock> ();
		}
	}

	/// <summary>
	/// the transform position in camera coordinate at begining of an action
	/// </summary>
	protected Vector2 orig;
	protected Vector2 dragInitPos;
	//The normalized vector of drag begun vector
	protected Vector2 dragAnchorNormal;

	void Update ()
	{

		if (state == ActionState.terminate) {

			//Safest case
			if (hFlags == HamperedFlag.NONE) {
//				Debug.Log ("Back to normal silent state");

				//SLot feature
				if (parentBlk.SubState == DPblock.BLK_SUB_STATE.Slotted) {
					PuzzleSlot.OnDropPuzzleSlot (parentBlk.transform);
				} else {
					PuzzleSlot.OffDropPuzzleSlot (parentBlk.transform);
				}

				state = ActionState.silent;
				StartCoroutine ("SafeWaitDone");

			} else if ((hFlags & HamperedFlag.GRP_FORBIDEEN) != 0) {

				#region grouned forbideen case
				OffGroupForbidden();
				state = ActionState.end;
				transform.position = safeInitPos;
				transform.rotation = safeInitRot;
				state = ActionState.terminate;
				#endregion

			} else if ((hFlags & HamperedFlag.OUTBOUND) != 0) {

				#region outbound case
				state = ActionState.end;
				transform.position = safeInitPos;
				transform.rotation = safeInitRot;
				state = ActionState.terminate;
				#endregion

			} else if ((hFlags & HamperedFlag.OVERLAP) != 0) {

				#region deal with overlapping convolutionized situaion with slotting
				if ((hFlags & HamperedFlag.UNSLOTTED) != 0) {
					//set state to end for coroutine
					if (prevAnchor == PuzzleSlot.GetTrans) {
						state = ActionState.end;
						StartCoroutine ("RepolateOffSlottedButOverlap");
					} else {
						//Force like usual overlap case,jump to 04tsdj2 on next frame
						hFlags &= ~HamperedFlag.UNSLOTTED;
					}

				} else if ((hFlags & HamperedFlag.SLOTTED) != 0) {
					//Force exceptional offOverlap() for slotted
					hFlags &= ~HamperedFlag.OVERLAP;
				} else {
					//04tsdj2
					state = ActionState.end;
//					StartCoroutine ("RepolateSafeState"); TODO tkingless/BugComputeSafeZone
//					========
					if (!parentBlk.IsMonoConnectedNoverlap) {
						transform.position = safeInitPos;
						transform.rotation = safeInitRot;
					} else {
						transform.position = parentBlk.GetActiveTDs.
							First ().RetreatMonoConncOverlapPivot ().position;
					}
					state = ActionState.terminate;
				}
				#endregion

			} else if ((hFlags & HamperedFlag.INVALID_TCHES) != 0) {

				#region deal with invalid touches convolutionized situaion with slotting
				if ((hFlags & HamperedFlag.UNSLOTTED) != 0) {
					//set state to end for coroutine
					if (prevAnchor == PuzzleSlot.GetTrans) {
						state = ActionState.end;
						StartCoroutine ("RepolateOffSlottedButOverlap");
					} else {
						//Force like usual invalid case, jump to 04tsdj on next frame
						hFlags &= ~HamperedFlag.UNSLOTTED;
					}

				} else if ((hFlags & HamperedFlag.SLOTTED) != 0) {
					//Invalid Color tch should be exclusive from Slotting
					Debug.LogError ("Not supposed");
					hFlags &= ~HamperedFlag.INVALID_COLO_TOUCH;
					hFlags &= ~HamperedFlag.INVALID_GEOM_TOUCH;
				} else {
					//04tsdj
					state = ActionState.end;
					transform.position = safeInitPos;
					transform.rotation = safeInitRot;
					state = ActionState.terminate;
				}
				#endregion

			} else {
				
				if ((hFlags & HamperedFlag.SLOTTED) != 0) {
					PuzzleSlot.OnDropPuzzleSlot (parentBlk.transform);
					hFlags &= ~HamperedFlag.SLOTTED;
					state = ActionState.terminate;
				} else if ((hFlags & HamperedFlag.UNSLOTTED) != 0) {
					PuzzleSlot.OffDropPuzzleSlot (parentBlk.transform);
					hFlags &= ~HamperedFlag.UNSLOTTED;
					state = ActionState.terminate;
				}

			}

		}
	}

	public void OnReady ()
	{
		orig = TruncV3toV2 (Camera.main.WorldToScreenPoint (transform.position));
		dragInitPos = TruncV3toV2 (Input.mousePosition);
		prevAnchor = parentBlk.transform.parent;
	}

	#region IDPActionHandler

	public virtual void OnActionInit ()
	{
		parentBlk.transform.SetParent (DPBlockManager.GetDragAnchor);
		parentBlk.transform.SetAsLastSibling ();

		//Debug.Log ("There is response from DP transform OnBeginDrag()");
		state = ActionState.init;

		dragAnchorNormal = TruncV3toV2 (Input.mousePosition) - dragInitPos;
		dragAnchorNormal.Normalize ();

		safeInitPos = transform.position;
		safeInitRot = transform.rotation;
	}

	public virtual void OnActionProc ()
	{
		state = ActionState.proc;

		if (hFlags.Equals (HamperedFlag.NONE)) {
			//Debug.Log ("lastSave pos: " + lastSafe.pos);
		}
			
//		Debug.Log ("There is response from DP transform OnDrag()");
	}

	public virtual void OnActionEnd ()
	{
//		Debug.Log ("There is response from DP transform OnEndDrag()");
		state = ActionState.end;
	}

	IEnumerator SafeWaitDone ()
	{
		yield return null;
		DPBlockManager.TriggerEvent (DPconstant.DP_JOB_DONE, GetComponent<DPblock> ());
	}

	IEnumerator RepolateOffSlottedButOverlap ()
	{
		Vector3 init = transform.position;
		transform.rotation = safeInitRot;

		float frac = 0;
		float totalTime = 0.15f;

		while (frac < 1f) {
			transform.position = Vector3.Lerp (init, safeInitPos, frac);
			frac += Time.deltaTime / totalTime;
			yield return null;
		}

		transform.position = safeInitPos;

		state = ActionState.terminate;
	}

	IEnumerator RepolateSafeState ()
	{
			
		#region new
		Vector3 init = transform.position;
		Collider2D bndCol = DPBlockManager.Selected.boundObj.GetComponent<Collider2D> ();
		Vector3 tmp = init - FindOverlapCentroid (bndCol);
		Vector2 probeVec;

		probeVec.x = tmp.x;
		probeVec.y = tmp.y;
		probeVec.Normalize ();

		bool computed = false;
		Vector2 targetOffset = ComputeSafeZone (probeVec, out computed, 40f, bndCol);

		if (!computed) {
			Debug.LogError ("Failed in stepwise search");
			yield break;
		}

		Vector2 walkedOffset = targetOffset;

		float frac = 0;
		Vector3 offset3 = new Vector3 (walkedOffset.x, walkedOffset.y, 0);
		float totalTime = 0.15f;
		//Debug.DrawRay (init, offset3.magnitude * probeVec, Color.green, 10);
		while (frac < 1f) {
			transform.position = Vector3.Lerp (init, init + offset3, frac);
			frac += Time.deltaTime / totalTime;
			yield return null;
		}

		transform.position = init + offset3;
		yield return null;


		if (DPBlockManager.Selected.IsOverlapping) {
			Debug.LogWarning ("BackToSafe some problem");
			transform.position = init + 0.5f * offset3;
		}

		#endregion

		if (DPBlockManager.Selected.IsOverlapping) {
			transform.position = safeInitPos;
			transform.rotation = safeInitRot;
			Debug.LogWarning ("BackToSafe some problem, directly back to safeInit pos and rot");
			yield return new WaitForFixedUpdate ();
		}

		state = ActionState.terminate;
		yield return null;
	}

	public void OnOverlap ()
	{
		hFlags |= HamperedFlag.OVERLAP;
	}

	public void OffOverlap ()
	{
//		Debug.Log ("DPAction OffOverlap () called");
		if (DPBlockManager.Selected.IsOverlapping) {
			return;
		}

		hFlags &= ~HamperedFlag.OVERLAP;
	}

	public void OnSlotted ()
	{
		hFlags |= HamperedFlag.SLOTTED;
		hFlags &= ~HamperedFlag.UNSLOTTED;
	}

	public void OffSlotted ()
	{
		hFlags |= HamperedFlag.UNSLOTTED;
		hFlags &= ~HamperedFlag.SLOTTED;
	}

	public void OnInvalidColorTched ()
	{
		hFlags |= HamperedFlag.INVALID_COLO_TOUCH;
	}

	public void OffInvalidColorTched ()
	{
		hFlags &= ~HamperedFlag.INVALID_COLO_TOUCH;
	}

	//Not used
	public void OnInvalidGeomTched ()
	{
		hFlags |= HamperedFlag.INVALID_GEOM_TOUCH;
	}

	//Not used
	public void OffInvalidGeomTched ()
	{
		hFlags &= ~HamperedFlag.INVALID_GEOM_TOUCH;
	}

	public void OnOutBound ()
	{
		hFlags |= HamperedFlag.OUTBOUND;
	}

	public void OffOutBound ()
	{
		hFlags &= ~HamperedFlag.OUTBOUND;
	}

	public void OnGroupForbidden ()
	{
		hFlags |= HamperedFlag.GRP_FORBIDEEN;
	}

	void OffGroupForbidden ()
	{
		hFlags &= ~HamperedFlag.GRP_FORBIDEEN;
	}

	#endregion

	#region utility func

	public static Vector2 TruncV3toV2 (Vector3 V3)
	{
		return new Vector2 (V3.x, V3.y);
	}

	public static Vector2 TransformDirection2D (Transform trans, Vector2 V2)
	{
		return TruncV3toV2 (trans.TransformDirection (V2.x, V2.y, 0));
	}

	protected float aTanToDegree (float y, float x)
	{
		float deg = 0.0f;
		deg = 180f * (Mathf.Atan (y / x) / Mathf.PI);
		if (y >= 0) {
			if (x >= 0) {
				//first quadrant
				//do nothing
			} else {
				//second quadrant
				deg += 180f;
			}
		} else {
			if (x >= 0) {
				//fourth quadrant
				deg += 360f;
			} else {
				//third quadrant
				deg += 180f;
			}
		}
		return deg;
	}

	Vector3 FindOverlapCentroid (Collider2D bndCol)
	{
		RaycastHit2D[] overlapped = new RaycastHit2D[6];
		bndCol.Cast (Vector2.zero, overlapped, 0.00f);

		Vector3 centroid = Vector3.zero;
		int num = 0;

		foreach (RaycastHit2D hit in overlapped) {
			if (hit.collider != null) {
				centroid += hit.collider.transform.position;
				num++;
			}
		}

		centroid = centroid / num;
		//Debug.DrawRay (Vector3.zero, centroid, Color.blue, 10);
		return centroid;
	}

	#endregion

	//BONUS this is a global and local minimum problem
	//BONUS (i) can carry over frames, (ii)can apply to rotation too
	Vector2 ComputeSafeZone (Vector2 probeDirec, out bool optimized, float maxDist, Collider2D bndCol)
	{

		const float retriveThreshold = 0.05f;
		float stepUnit = 2f * bndCol.bounds.extents.magnitude;
		const int unitPartition = 100;
		const int maxAttempt = 350;
		Vector2 initOffset = bndCol.offset;
		Vector2 homingOffset = Vector2.zero;
		Vector2 relativeProbeDir;
		optimized = false;

		//Algorithm:  double tempering walk TODO: make this adaptive stepsize
		int attempt = 0;
		int microStepHoming = 0;
		float stepsize = stepUnit / unitPartition;
		float dist = 0;

		Vector3 tmp = new Vector3 (probeDirec.x, probeDirec.y, 0);
		relativeProbeDir = bndCol.transform.InverseTransformDirection (tmp);

		while (!optimized && attempt < maxAttempt) {
			RaycastHit2D[] hits = new RaycastHit2D[5];

			if (microStepHoming == 0) {

				if (stepsize < stepUnit / 4) {
					stepsize += stepUnit / unitPartition;
					dist += stepsize;
				} else {
					dist += stepUnit / 4;
				}
			} else {
				dist += stepsize;
			}
			bndCol.offset = dist * relativeProbeDir;
			//Debug.DrawRay (attempt*Vector3.up + transform.position, relativeProbeDir * dist, Color.yellow, 10);

			bndCol.Cast (relativeProbeDir, hits, 0.0f);

			bool thisLoopEsacped = true;
			foreach (RaycastHit2D hit in hits) {
				if (hit.collider != null) {
					//dist += stepsize;
					attempt++;
					thisLoopEsacped = false;
					break;
				}
			}

			if (!thisLoopEsacped) {

				if ((bndCol.offset - initOffset).magnitude > maxDist) {
					Debug.LogWarning ("Exceed max probe dist, terminate");
					break;
				}

				continue;
			}

			if (microStepHoming == 0) {
				//Debug.DrawRay (2*Vector3.down + transform.position, relativeProbeDir * dist, Color.yellow, 10);
				dist -= Mathf.Min (stepsize, stepUnit / 4);
				stepsize = stepUnit / unitPartition;
				bndCol.offset = dist * relativeProbeDir;
				//Debug.DrawRay (Vector3.down + transform.position, relativeProbeDir * dist, Color.red, 10);
				microStepHoming = 1;
				continue;
			}

			optimized = true;

		}

		if (optimized) {
			homingOffset = bndCol.offset + relativeProbeDir * retriveThreshold;
			homingOffset = bndCol.transform.TransformDirection (homingOffset);
			//Debug.Log ("WalkProbe() Esacped, attemp: " + attempt);
		}

		bndCol.offset = initOffset;
		return homingOffset;
	}

	IEnumerator AnimateLerpPos ()
	{
		yield return null;
	}
}
