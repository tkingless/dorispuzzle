﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.EventSystems;

public sealed class DPtranslateFree : DPaction
{

	Vector3 transInitPos;
	Vector3 offsetToMouse;
	float zDistToCam;
	AutoRot autoRot;

	public override void OnActionInit ()
	{
		if (state != ActionState.silent)
			return;

		base.OnActionInit ();
		transInitPos = transform.position;

		zDistToCam = Mathf.Abs (transInitPos.z - Camera.main.transform.position.z);

		offsetToMouse = transInitPos - Camera.main.ScreenToWorldPoint (
			new Vector3 (Input.mousePosition.x, Input.mousePosition.y, zDistToCam)
		);

		autoRot = new AutoRot (parentBlk);

	}

	public override void OnActionProc ()
	{
		if (state != ActionState.proc && state != ActionState.init)
			return;
		
		base.OnActionProc ();

		transform.position = Camera.main.ScreenToWorldPoint (
			new Vector3 (Input.mousePosition.x, Input.mousePosition.y, zDistToCam)
		) + offsetToMouse;
			
		if (autoRot.ShouldAct ()) {
			autoRot.Proc ();
		}

	}

	public override void OnActionEnd ()
	{
		if (state != ActionState.proc)
			return;

		base.OnActionEnd ();

		offsetToMouse = Vector3.zero;
		autoRot = null;
		state = ActionState.terminate;
	}
		
}

class AutoRot
{

	DPblock ctrlr;
	//TODO tkingless/AutoRot being considered only
	#pragma warning disable 0414
	static float maxTolerateDegOnOccupied = 10.0f;
	#pragma warning restore 0414

	public delegate void VoidDel ();

	VoidDel onOccupied;
	//check need to within the max tolerate deg, otherwise try auto rotate if no error like overlap, deoccupied,etc.
	//Distance should be in unit of a block size (2) , rotation in unit of radian
	float distOnAct;

	//Not considered yet
	float maxim;
	//The rotational distance of init and final rot

	public AutoRot (DPblock _ctrlr)
	{
		ctrlr = _ctrlr;
	}

	ValidTouchData closestTch;
	Vector3 targetNml, selTrigNml;
	DPtrigger selTrig;
	//define the mechanism when to update tch, what to do
	public bool ShouldAct ()
	{

		if (!ctrlr.IsOccupied) {
			var tmpTD = ctrlr.GetClosestTchTD;
			if (tmpTD != null) {
				Update (tmpTD);

				return true;
			}
		}

		return false;
	}

	bool Update (ValidTouchData updated)
	{

		if (closestTch != updated) {
			closestTch = updated;
			selTrig = closestTch.GetASelTrig;
			targetNml = closestTch.GetTargetNml (selTrig.Trig);
			selTrigNml = selTrig.GetTchHeadDir ();

			return true;
		}

		return false;
	}

	void Rotate ()
	{
		selTrigNml = selTrig.GetTchHeadDir ();
		Quaternion rot = Quaternion.FromToRotation (selTrigNml, targetNml);

		//bonus more rigorous logic
		if (rot.eulerAngles.z > 300f) {
			Vector3 newRot = rot.eulerAngles;
			newRot.z -= 360f;
			ctrlr.transform.Rotate (newRot * Time.fixedDeltaTime * 10f);
			return;
		}

		ctrlr.transform.Rotate (rot.eulerAngles * Time.fixedDeltaTime * 10f);
	}

	public void Proc ()
	{
		//Define the mechanics
//		if (closestTch.MinDistance () < 0.5f) {
		Rotate ();
//		}
	}





}
