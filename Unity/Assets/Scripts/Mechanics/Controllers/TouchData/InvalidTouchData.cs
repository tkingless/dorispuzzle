﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tkingless.DorisPuzzle.Models;
using UnityEngine;

public class InvalidTouchData : TouchDataBase, IStateableTchData<InvalidTDtask.State>
{
	//tkingless/Invalidation: only the engaged turn color
	//tkingless/Invalidation: selected will know hflag when actioning
	/* tkingless/Invalidation: for each pair of selected and engaged not affect top priority.
	* if connected by valid touch data or off all connected of ITD, then diminish stepwisely
		*/

	//Desgin: only created when the selected is not connected to the engaged

	InvalidTDtask.State state = InvalidTDtask.State.Disposed;

	public InvalidTDtask.State SetState {
		set {
			state = value;
		}
	}

	public InvalidTDtask.State GetState {
		get {
			return state;
		}
	}

	public override int GetStateNum {
		get {
			return (int)state;
		}
	}

	public override int SetStateNum {
		set {
			state = (InvalidTDtask.State)value;
		}
	}

	public InvalidTouchType invalidType = InvalidTouchType.UNDEF;

	public InvalidTouchData () : base ()
	{
		ttype = TouchingType.Failure;
	}

	public void MsgOnConnected ()
	{
		InvalidTDtask task = new InvalidTDtask (1, this, OnConnected);
		TDflowCtrl.RegisterTask (task);
	}

	public void MsgOffConnected ()
	{
		InvalidTDtask task = new InvalidTDtask (2, this, OffConnected);
		TDflowCtrl.RegisterTask (task);
	}

	void OnConnected ()
	{
//		Debug.Log ("OnConnected(): " + ToString());
		triggHashset.ToList ().ForEach (x => x.AddWrongTch = this);
		triggHashset.ToList ().ForEach (x => x.OnInvalidTched ());
	}

	public void OffConnected ()
	{
//		Debug.Log ("OffConnected(): " + ToString());
		triggHashset.ToList ().ForEach (x => x.RmWrongTch = this);
		triggHashset.ToList ().ForEach (x => x.OffInvalidTched ());
	}

	public void StepwisePurge ()
	{
		if (GetState == InvalidTDtask.State.Connected) {
			MsgOffConnected ();
		} 
		TDflowCtrl.ManUpdate ();
	}

	public override string ToString ()
	{
		return string.Format ("[<color=orange>ITouchData</color>: type={0} GetState={1}, livingIdx={2}]",
			invalidType, GetState, TDflowCtrl.GetTDorder (this));
	}
}

[System.Flags]
public enum InvalidTouchType
{
	UNDEF = 0,
	//In fact, not used, the physics2D disabled the triggering
	/* TODO tkingless/Invalid Geom Matching: Leo/Invalid Geom Matching:
	 * to turn on this, just go to physics2D setting, enable collision BlkPoint with BlkVault and Blkhalves, but this derive
	 * another problem, good msg and bad msg appear together....and some lagging suspected
	 */
	GEOMETRY = 1,
	COLOR = 2,
}

public class InValidTouchDataFactory
{

	DPradar bufferSender;
	InvalidTouchType bufferedInvalidity;
	List<TrigConnection> bufferConnc;
	InvalidTouchData buffer;


	public InValidTouchDataFactory (DPradar senderEngaged)
	{
		bufferSender = senderEngaged;
		bufferedInvalidity = InvalidTouchType.UNDEF;
		bufferConnc = new List<TrigConnection> ();
		buffer = new InvalidTouchData ();
	}

	public void AddConnPair (TrigConnection _conn)
	{
		bufferConnc.Add (_conn);
	}

	public InvalidTouchData Instantiate ()
	{
		buffer.Sender = bufferSender;
		buffer.invalidType = bufferedInvalidity;
		bufferConnc.ForEach (x => buffer.AddConnc (x));

		return buffer;
	}

	public static InvalidTouchData InvalidalityAnalysis (Trigger selected, Trigger engaged)
	{
		
		InValidTouchDataFactory factory = new InValidTouchDataFactory (engaged.DPobj.parentBlk.radObj);
		factory.AddConnPair (new TrigConnection (selected.DPobj, engaged.DPobj));

		//Check if matchable gemoetry, point no way to match edge
		HashSet<Type> hsetType = new HashSet<Type> ();
		hsetType.Add (selected.DPobj.GetType ());
		hsetType.Add (engaged.DPobj.GetType ());
		if (hsetType.Count > 1 && hsetType.Contains (typeof(DPvertex))) {
			factory.bufferedInvalidity |= InvalidTouchType.GEOMETRY;
		}

		//Check if same color
		if (selected.color != engaged.color) {
			factory.bufferedInvalidity |= InvalidTouchType.COLOR;
		}

		if (factory.bufferedInvalidity != InvalidTouchType.UNDEF) {
			return factory.Instantiate ();
		} else {
			return null;
		}
	}
}