﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Tkingless.DorisPuzzle.Models;
using Tkingless.DorisPuzzle.Util;

public class TrigConnection
{

	public enum State
	{
		attached,
		detached,
		undefined
	}

	public Trigger[] pair = new Trigger[2];

	State state = State.undefined;

	public TrigConnection (DPtrigger selected, DPtrigger engaged)
	{

		if (pair != null) {
			pair [0] = selected.Trig;
			pair [1] = engaged.Trig;
		}

		Check ();
	}

	public void Check ()
	{
		state = pair [0].TouchedBy (pair [1]) ? State.attached : State.detached;
	}

	public bool Attached {
		get {
			return state == State.attached;
		}
	}

	public float DP_Distance ()
	{
		float tmp = -1f;

		Vector2 zeroPos = UtilFunc.TruncV3toV2 (pair [0].DPobj.transform.position);
		Vector2 firstPos = UtilFunc.TruncV3toV2 (pair [1].DPobj.transform.position);
		tmp = (zeroPos - firstPos).magnitude;

		return tmp;
	}

	public bool Detached {
		get {
			return state == State.detached;
		}
	}

	public Trigger GetOppTrig (Trigger self)
	{
		if (pair.Contains (self)) {
			foreach (Trigger trig in pair) {
				if (trig != self) {
					return trig;
				}
			}
		} else {
			Debug.Log ("This pair not containing the self");
		}

		return null;
	}

	public bool Contains (Trigger test)
	{
		return pair.Contains (test);
	}
}
