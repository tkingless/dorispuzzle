﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Tkingless.DorisPuzzle.constants;
using Tkingless.DorisPuzzle.Models;
using Tkingless.DorisPuzzle.Util;

public abstract class TouchDataBase
{
	public TouchingType ttype;

	public List<TrigConnection> connections;
	protected HashSet<Trigger> triggHashset;

	public List<Trigger> Triggs {
		get {
			return triggHashset.ToList ();
		}
	}

	protected DPradar sender;

	//Sender is from Engaged blk
	public DPradar Sender {
		get {
			return sender;
		}
		set {
			sender = value;
		}
	}

	public TouchDataBase () {
		connections = new List<TrigConnection> ();
		triggHashset = new HashSet<Trigger> ();
	}

	#region connection construction
	public void AddConnc (TrigConnection _connc)
	{
		connections.Add (_connc);
		_connc.pair.ToList ().ForEach (x => triggHashset.Add (x));
	}

	/// <summary>
	/// Gets the connection related to.
	/// </summary>
	/// <returns>Get the connection related.</returns>
	/// <param name="trig">Trig.</param>
	protected List<TrigConnection> GetConnsRelTo (DPtrigger trig)
	{
		List<TrigConnection> tmpList = new List<TrigConnection> ();

		connections.ForEach (x => {
			if (x.pair.Contains (trig.Trig)) {
				tmpList.Add (x);
			}
		});

		return tmpList;
	}

	public bool Involves (DPtrigger trig)
	{
		return triggHashset.Contains (trig.Trig);
	}

	protected void CheckConnections ()
	{
		connections.ForEach (x => x.Check ());
	}

	//Design: beware of All_Disconnected and DPradars physics leave are uncontrollable as they using different
	//sets of colliders
	public bool All_Disconnected ()
	{
		CheckConnections ();
		return connections.TrueForAll (x => x.Detached);
	}

	public bool All_Connected ()
	{
		CheckConnections ();
		return connections.TrueForAll (x => x.Attached);
	}
	#endregion

	#region Utility
	public float MinDistance ()
	{

		float tmp = -1f;

		foreach (TrigConnection con in connections) {
			if (tmp < 0)
				tmp = con.DP_Distance ();

			if (con.DP_Distance () < tmp)
				tmp = con.DP_Distance ();
		}

		if (tmp < 0f) {
			Debug.LogError ("no suppposed");
			return 0f;
		}

		return tmp;
	}

	public bool HasSomeFromSel {
		get {
			return triggHashset.ToList ().Find (x => x.DPobj.IsSelected) != null;
		}
	}

	//workaround
	public Vector3 GetTargetNml (Trigger frmSel)
	{

		Vector3 noMeaning = frmSel.DPobj.GetTchHeadDir ();

		if (!Involves (frmSel.DPobj)) {
			Debug.Log ("suppose wrong usage");
			return noMeaning;
		}
		List<TrigConnection> tmp = GetConnsRelTo (frmSel.DPobj);
		if (tmp.Count > 0) {

			Trigger tmpTrig = tmp [0].
				GetOppTrig (frmSel);

			Vector3 tmpDir = tmpTrig.DPobj.
				GetTchHeadDir () * -1f;

			return tmpDir;
		}

		return noMeaning;
	}

	public DPtrigger GetASelTrig {
		get {
			return triggHashset.First (trig => trig.DPobj.IsSelected).DPobj;
		}
	}

	public DPtrigger GetAEngTrig{
		get{
			return triggHashset.First (trig => !trig.DPobj.IsSelected).DPobj;
		}
	}

	public DPradar GetEngRadar {
		get {
			return triggHashset.
				First (trig => !trig.DPobj.IsSelected).
				DPobj.parentBlk.radObj;
		}
	}

	public DPblock GetSelectedBlk {
		get{
			return triggHashset.First (trig => trig.DPobj.IsSelected).
				DPobj.parentBlk;
		}
	}

	public DPblock GetEngagedBlk {
		get{
			return triggHashset.First (trig => !trig.DPobj.IsSelected).
				DPobj.parentBlk;
		}
	}

	public bool IsAnyFromSelected {
		get {
			return triggHashset.Any (trig => trig.DPobj.IsSelected);
		}
	}

	public List<Trigger> GetOppTrigs (Trigger selected)
	{

		HashSet<Trigger> tmp = new HashSet<Trigger> ();

		connections.ForEach (x => {
			if (x.Contains (selected)) {
				tmp.Add (x.GetOppTrig (selected));
			}
		});

		return tmp.ToList ();
	}
	#endregion

	#region interface part

	//Desgin: must be defined in subclass for functioning
	public virtual int GetStateNum{
		get{
			return -1;
		}
	}

	public virtual int SetStateNum{
		set{
			//nothing to do
		}
	}
	#endregion
}

public interface IStateableTchData<StateType> {
	StateType GetState{ get; }
	StateType SetState{ set; }
}



/// <summary>
/// All the type name is read from Engaged to Selected Block
/// </summary>
public enum TouchingType
{
	Failure,

	//Fully docked
	eFull1sFull,
	eFull2sHalf,
	e2Half2sHalf,
	eHalf1sFull,

	//Half docked
	eHalf1sHalf,
	eFull1sHalf,
	//not used

	Point
}
