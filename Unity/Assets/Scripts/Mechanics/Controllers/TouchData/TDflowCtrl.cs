﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tkingless.DorisPuzzle.Models;
using Tkingless.DorisPuzzle.TouchData;

public class TDflowCtrl : SceneSingleton<TDflowCtrl>
{
	int indx = 1;

	LivingTDs livingTDs = new LivingTDs ();
	FrozenTDs frozenTDs = new FrozenTDs ();
	PromiscuousTasks promisTasks = new PromiscuousTasks ();
	OrderedTasks preorderTasks = new OrderedTasks ();
	OrderedTasks orderedTasks = new OrderedTasks ();
	OverwriteQueue overwritingQ = new OverwriteQueue ();

	/// <summary>
	/// the function really do updating, return true if updated sth, and ask SortTask to sort again
	/// and check if there is update needed again
	/// </summary>
	bool updated ()
	{
		bool isUpdated = false;

		preorderTasks.ExecuteInOrder (out isUpdated);
		orderedTasks.ExecuteInOrder (out isUpdated);

		promisTasks.DeregInvokedTasks ();

		//Def living TD is removed if itself is disposed and no pending task
		var disposedLivingTDs = livingTDs.
			GetLivingTDofState<ValidTouchData> ((int)ValidTDtask.State.Disposed).
			Cast<TouchDataBase> ().ToList ();
		
		promisTasks.GetIdleDisposedTDs (disposedLivingTDs).ForEach (x => {
			var vtd = (ValidTouchData)x;

			if (overwritingQ.ContainsKey (vtd)) {
				var orphan = overwritingQ.Read (vtd);

				if (ShouldForgetPendedTask (orphan)) {
					Debug.Log ("<color=olive>Deleted orphan: " + orphan.ToString () + "</color>");
					ForgetPendedTaskAndTD (orphan);
				} else {
					if (overwritingQ.ContainsKey (orphan)) {
						var oorphan = overwritingQ.Read (orphan);
						Debug.Log ("<color=red>Never called, orphan has candidate: " + oorphan + "</color>");
					}
				}
			}

			livingTDs.Remove (x);
			isUpdated = true;
		});

		return isUpdated;
	}

	static int sTinit = 0;
	static int sTterm = 0;

	/// <summary>
	/// Sorts the tasks of promiscuous and overwriting tasks, really too long...
	/// never from instace of TDflowCtrl call Msg...On off
	/// </summary>
	void SortTask ()
	{
		sTinit++;
		if (sTinit - sTterm != 1) {
			Debug.LogError ("strange, init:" + sTinit + ", term: " + sTterm);
			sTinit = 0;
			sTterm = 0;
		}
			
		List<ValidTDtask> sortedClone = promisTasks.GetSortedValidTaskList;

		//deal with subOrder, not overwriting cases:
		List<ValidTDtask> indepTasks = overwritingQ.ExceptPendedTask (sortedClone);
		//For debug only, not for function:
		indepTasks.FindAll (x => !x.IsInvokable).ForEach (x => {
			switch (x.GetStepNum) {
			case 0:
				indepTasks.Remove (x);
				promisTasks.Remove (x);
				Debug.Log ("<color=yellow>|stepsize| >2 or ==0: " + x.GetStepNum + ", " +
				x.ToString () + " the TD: " + x.Host.ttype + ", state: " + ((ValidTouchData)x.Host).GetState + "</color>");
				break;
			default:
				//case > 1 or < -1
				break;
			}
		});

		HashSet<ValidTouchData> allIndepTDs = new HashSet<ValidTouchData> (indepTasks.Select (task => (ValidTouchData)task.Host));

		allIndepTDs.ToList ().ForEach (td => {

			var related = indepTasks.FindAll (x => x.Host == td).ToList ();
			//transisble count always is one
			var transisble = related.FindAll (x => x.IsInvokable).ToList ();

			if (transisble.Count == 1) {
				bool Should_NoIntersectThenEnqueue = true;
				//get the first distinct task of different hosts, check if state and action matches
				var transTask = transisble.First ();

				//Case of about overwritting prio
				if (transTask.subOrder == 1) {

					ValidTouchData _host = (ValidTouchData)transTask.Host;
					int _TDorder = transTask.TDorder;

					//Debug workaround: purge those overwriting pended trig:
					promisTasks.AnyLowerOrderAndIntersectTrigsList (_TDorder, _host).ForEach (x => {
						if (overwritingQ.ContainsValue (x)) {
							livingTDs.Remove (x);
							promisTasks.RemoveAllByHost (x);
							overwritingQ.RemoveByVal (x);
//							Debug.Log ("<color=white>such a bug deled</color>");
						}
					});

					//confirm again overwritting OnPrio, not forcefully wipe out some trigger's legit TopPriority
					if (promisTasks.AnyLowerOrderAndIntersectTrigsWith (_TDorder, _host)) {
						Debug.LogWarning ("<color=brown>there is intersection, safe hold took effect: " + transTask.Host
						+ "stuck by: " + "</color>");
						
						Should_NoIntersectThenEnqueue = false;

						promisTasks.AnyLowerOrderAndIntersectTrigsList (_TDorder, _host).ForEach (x => {
							Debug.LogWarning ("<color=brown> x: " + x + "</color>");
						});

						if (promisTasks.AnyLowerOrderAndIntersectTrigsList (_TDorder, _host).
							TrueForAll (x => overwritingQ.ContainsValue (x))) {
							Debug.LogWarning ("<color=brown> rectified</color>");
							Should_NoIntersectThenEnqueue = true;
						}
					}
				}

				if (Should_NoIntersectThenEnqueue) {
					orderedTasks.Enqueue (transTask);

					//Enqueue consecutive tasks
					List<ValidTDtask> consecutives = related.FindAll (x => !x.Equals (transTask));
					int curSubOrder = transTask.subOrder;

					consecutives.ForEach (x => {
						if (x.subOrder - curSubOrder == 1) {
							orderedTasks.Enqueue (x);
							//							Debug.Log ("<color=olive> consecutive enqueued: " + x.ToString () + "</color>");
							curSubOrder = x.subOrder;
						}
					});
				}
			}
		});





		//deal with overwritting cases:
		overwritingQ.GetReplacealeTDs ().ForEach (existingTD => {
			var candidate = overwritingQ.Read (existingTD);

			overwritingQ.Remove (existingTD);

			//Overwritting tasks
			var OWtasks = promisTasks.GetTasksByHost (candidate);

			//Checkin only
			if (OWtasks.Count != 1) {
				Debug.LogWarning ("<color=orange>Abnormal count: " + OWtasks.Count +
				", existing :" + existingTD.ToString () + "</color>");
				//tkingless/Debug: in this case, occasionally
				//exisiting index = -1
			} else {
				//Normal expected case, Count==1 :

				var OWtask = OWtasks.First ();
				if (existingTD.GetState == ValidTDtask.State.Disposed) {
					//queue directly
//				Debug.Log ("Direct overwrite happen, candidate idx: " + candidate + ", existingIdx: " + existingTD);
					orderedTasks.Enqueue (OWtask);

				} else if (existingTD.GetState == ValidTDtask.State.Prioritized) {

					if (candidate.MinDistance () < existingTD.MinDistance ()) {

						//workaround
						bool repelExistingTDByDistCheck = true;
						orderedTasks.GetTasksByHost (existingTD).ForEach (errTsk => {
							//Checking only, seem not happening
							Debug.Log ("<color=red>it is not null: " + errTsk.ToString () + ", " + errTsk.Host + "</color>");
							if (errTsk.subOrder == 6) {
								repelExistingTDByDistCheck = false;
							}
						});

						#region extra hinder check
						bool thirdPrioHinderCheck = false;


						if (OWtask.Triggs.Any (x => x.DPobj.TopPriority != existingTD &&
						    x.DPobj.TopPriority != candidate && x.DPobj.TopPriority != null)) {
							thirdPrioHinderCheck = true;
						}

						//some indep task sub of 1, while an overwriting task pended as one of trigg already has the previous TP
						//9asd192 uncomment to see the special case, workarounded to avoid
//						OWtask.Triggs.FindAll (x => x.DPobj.TopPriority != existingTD &&
//							x.DPobj.TopPriority != candidate && x.DPobj.TopPriority != null).
//						ForEach (y => {
//							Debug.LogWarning ("<color=red> Spotted the alien TP: " + y.TopPrioirty +
//								" and the OWtask.HOST: " + OWtask.Host + ",existing: " + existingTD + " </color>");
//						});
						#endregion


						if (thirdPrioHinderCheck) {
							Debug.Log ("<color=white>such a bug deled</color>");
							promisTasks.RemoveAllByHost (candidate);
							livingTDs.Read (candidate);
						} else {
							
							//existingTD.MsgOffPrio (); 28df43a, not to call MsgXXX() from outside
							if (repelExistingTDByDistCheck) {
								ValidTDtask task = new ValidTDtask (6, existingTD, existingTD.OffPrio);
								task.TDorder = livingTDs.Read (existingTD);
								orderedTasks.Enqueue (task);
							}

							orderedTasks.Enqueue (OWtask);
							//Debug.Log (candidate.ToString () + " wins " + existingTD.ToString ());

						}

					} else {
						promisTasks.RemoveAllByHost (candidate);
						//Debug.Log (candidate.ToString () + " loses " + existingTD.ToString ());
					}
				}
			}
		});

		//azxci4q guaranteed no connected

		//Algo:
		/*
		 * for each of invalidTask in promis, get the Selected and Engaged blks
		 	* suborder 1 onConnected:
		 		* Check validLivingTD of {matched Selected and Engaged}
		 			* enqueue into preOrderedTask only if not found a matched TD is active/freezed
		 			* whereas raise warning...
		 		* Check validTask in orderedTask queue of {matched Selected and Engaged}
		 			* for orderTask's of order of 3, drop and cancel
		 		* 
		 	* suborder 2 onDisconnected:
		 		* just enqueue into preorderTask
		 */
		List<InvalidTDtask> invalidTasks = promisTasks.GetSortedInvalidTaskList;
		invalidTasks.ForEach (tsk => {
			DPblock _sel = tsk.GetSelectedBlk;
			DPblock _eng = tsk.GetEngagedBlk;

			switch (tsk.Host.GetStateNum) {
			case 1:
				bool shouldEnQ = true;
				var VTDs = livingTDs.GetTypeTDs<ValidTouchData> ().
					FindAll (itd => itd.GetSelectedBlk == _sel && itd.GetEngagedBlk == _eng).ToList ();

				if (VTDs.Count > 0) {
					if (!VTDs.TrueForAll (vtd => vtd.GetState != ValidTDtask.State.Active &&
					    vtd.GetState != ValidTDtask.State.Freezed)) {
						shouldEnQ = false;
						Debug.LogWarning ("not supposed, as filtered");
					}
				}

				if(orderedTasks.GetTasksOf(_sel,_eng).Any(x=>x.subOrder == 3)){
					Debug.Log("This is not supposed to happen");
					shouldEnQ = false;
				}
					
				if (shouldEnQ) {
					preorderTasks.Enqueue (tsk);
				}else{
					promisTasks.Remove(tsk);
				}

				break;
			case 2:
				preorderTasks.Enqueue (tsk);
				break;
			default:
				Debug.LogError ("impossible range");
				break;
			}
		});
		/* for each of orderedTask's or subOrder 2, get the Selected and Engaged blks
			 * for each of invalidTD in living of {matched Selected and Engaged}
				 * stepwise Purging
				 * or even there is suborder 1 of invalidTDtask from promis, purge, will really happen?
		 */
		orderedTasks.GetTasksOf (2).ForEach (otsk => {
			DPblock _sel = otsk.GetSelectedBlk;
			DPblock _eng = otsk.GetEngagedBlk;

//			Debug.Log("<color=cyan>x _sel blk: </color>" + _sel.name);
//			Debug.Log("<color=cyan>x _eng blk: </color>" + _eng.name);

			List<InvalidTouchData> ITDs = livingTDs.GetTypeTDs<InvalidTouchData>().
				FindAll(x=> {

//					Debug.Log("<color=cyan>x Sel blk: </color>" +  x.GetSelectedBlk.name + ", _sel: " + _sel.name);
//					Debug.Log("<color=cyan>x Eng blk: </color>" +  x.GetEngagedBlk.name + ", _eng: " + _eng.name);

					return x.GetSelectedBlk == _sel && x.GetEngagedBlk == _eng;
				}).ToList();

			ITDs.ForEach(x=>{
				if(x.GetState == InvalidTDtask.State.Connected){
					InvalidTDtask task = new InvalidTDtask (2, x, x.OffConnected);
					task.TDorder = livingTDs.Read (x);
					preorderTasks.Enqueue (task);
				}
			});

			List<InvalidTDtask> invalidTasks2 = promisTasks.GetSortedInvalidTaskList;
			invalidTasks2.FindAll(itsk => itsk.Host.GetSelectedBlk == _sel && 
				itsk.Host.GetEngagedBlk == _eng).ForEach(matched => {
					Debug.LogWarning("miraculous happened, not supposed..");
					promisTasks.Remove(matched);
				});
		});


		sTterm++;
		if (sTinit - sTterm != 0) {
			Debug.LogError ("strange, init:" + sTinit + ", term: " + sTterm);
			sTinit = 0;
			sTterm = 0;
		}
	}

	//Looping
	void _Update ()
	{
		SortTask ();

		while (updated ()) {
			SortTask ();
		}
	}

	void RegisterTD (TouchDataBase TD)
	{
		if (!livingTDs.ContainsKey (TD))
		if (livingTDs.Create (TD, indx)) {
			indx++;
		}
	}

	void registerTask (TDtaskBase task)
	{
		int tdOrder = -1;
		tdOrder = livingTDs.Read (task.Host);
		if (tdOrder != -1) {
			task.TDorder = tdOrder;

			if (promisTasks.CheckTaskAddable (task)) {
				promisTasks.Add (task);
			} else {
				//don't add equal TDtasks of same TDorder and suborder, and check if really happen
				//Debug.Log ("This must happen: " + existOvewrittenByCandidate.Values.Contains (task.host));
			}
		}
	}

	void Setup ()
	{
		if (indx != 1) {
			Debug.LogWarning ("indx not 1; " + indx);
		}

		if (DPBlockManager.Selected == null) {
			Debug.LogError ("fatal");
			return;
		}
			
		frozenTDs.GetTDsRelatedToSelected ().ForEach (frmSel => {
			RegisterTD (frmSel);
			frozenTDs.Remove (frmSel);

			if (frmSel.GetState != ValidTDtask.State.Freezed) {
				Debug.LogWarning ("<color=lime> supposed Freezed:  </color>" + frmSel.ToString ());
			}
		});

	}

	void Setdown ()
	{
		sTinit = 0;
		sTterm = 0;

		if (orderedTasks.Count != 0) {
			Debug.LogWarning ("orderedTasks supposed zero");
		}

		if (preorderTasks.Count != 0) {
			Debug.LogWarning ("preOrderedTasks supposed zero");
		}

		if (overwritingQ.Count > 0) {
//			Debug.Log ("<color=lime> overwriteQ > 0 </color>");
			overwritingQ.Keys.ForEach (existing => {
				var cand = overwritingQ.Read (existing);

				if (cand.GetState == ValidTDtask.State.Disposed) {
					promisTasks.RemoveAllByHost (cand);
					livingTDs.Remove (cand);
				} else {
					Debug.LogWarning ("<color=lime> not supposed state, cand: " + cand.ToString () + "</color>");
					Debug.LogWarning ("<color=lime> and, key: " + existing.ToString () + "</color>");
				}
			});
		}
			
		if (promisTasks.Count != 0) {
			promisTasks.GetAll.ForEach (x => {
//				Debug.Log ("<color=lime> promiscuousTasks != 0, which is :  </color>" + x.ToString ());
			});
		}
			
		//After clean overwriteQ, check living TD are left only frozen
		if (livingTDs.IsAllFrozen) {
			livingTDs.GetTypeTDs<ValidTouchData>().
			ForEach (x => frozenTDs.Add ((ValidTouchData)x));
		} else {
			//TODO tkingless/Debug/Grouping: supposed all TD frozen, and not PrioTD, occasionally happened, but not really fatal, it is logical flow control exception
			Debug.LogWarning ("<color=lime> livingTDkeyPair some TD not frozen </color>");
			livingTDs.GetLivingTDofNotState<ValidTouchData> ((int)ValidTDtask.State.Freezed).
			ForEach (x => Debug.Log ("<color=lime>not frozen:  </color>" + x.ToString ()));
		}

		//at last, living pair and promiscuous task are zero
		livingTDs.Clear ();
		promisTasks.Clear ();
		overwritingQ.Clear ();

		indx = 1;

//		Debug.Log ("fozen TD count: " + frozenActiveTDs.Count);
	}

	bool ShouldOverwrite (ValidTouchData cand, ValidTouchData exist)
	{
		bool should = true;

		//cand not supposed already existing
		if (overwritingQ.ContainsValue (cand) || livingTDs.ContainsKey (cand))
			should = false;

		if (overwritingQ.ContainsKey (exist)) {
			var oldCand = overwritingQ.Read (exist);

			if (oldCand != null) {
				if (cand.MinDistance () >= oldCand.MinDistance ()) {
					should = false;
				}
			}
		}

		return should;
	}

	void prioOverwrite (ValidTouchData cand, ValidTouchData exist)
	{
		if (overwritingQ.ContainsKey (exist)) {
			var oldCandidate = overwritingQ.Read (exist);
			promisTasks.RemoveAllByHost (oldCandidate);
		}

		overwritingQ.Upsert (exist, cand);
//		Debug.Log ("<color=yellow> overwritingQ add key : " + exist + ", value: " + cand+ "</color>");
	}

	bool ShouldForgetPendedTask (ValidTouchData candidate)
	{
		int _candTDorder = livingTDs.Read (candidate);
		return promisTasks.AnyHigherOrderAndIntersectTrigsWith (_candTDorder, candidate);
	}

	void ForgetPendedTaskAndTD (ValidTouchData candidate)
	{
		promisTasks.RemoveAllByHost (candidate);
	}

	void slottedPurge (DPblock selected)
	{
		IEnumerable<Trigger> selTriggs = selected.GetDPTriggs.Select (x => x.Trig);

		var _selTDs = livingTDs.GetIntersectedWith (selTriggs);
		_selTDs.ForEach (x => {
			livingTDs.Remove (x);
			promisTasks.RemoveAllByHost (x);

			if(x.GetType() == typeof(ValidTouchData)){
				overwritingQ.Remove ((ValidTouchData)x);
			}
		});

//		Debug.Log (String.Format ("After slot purging, livingTDkeyPair: {0}, " +
//			"overwriteQueue: {1}, promiscuousTasks: {2}",livingTDkeyPair.Count,overwriteQueue.Count,promiscuousTasks.Count));
	}

	#region public static methods for use

	/// <summary>
	/// warm up the TDflow
	/// </summary>
	/// <param name="selected">Selected.</param>
	public static void Vapourize (DPblock selected)
	{
//		Debug.Log ("Vapourize()");
		Instance.Setup ();
	}

	/// <summary>
	/// Cool down the control
	/// </summary>
	/// <param name="selected">Selected.</param>
	//Condense should be called later then ptr leave->mass rad off->
	public static void Condense (DPblock selected)
	{
//		Debug.Log ("Condense()");
		Instance.Setdown ();
	}

	//Design: must be called from outside, otherwise disturbe the sequence of Sorttask, like at 28df43a
	public static void RegisterTask (TDtaskBase task)
	{
		Instance.RegisterTD (task.Host);
		Instance.registerTask (task);
	}

	//This func only be one use of purpose: candidate TD to overwrite existing TD after minDistance check and latter is at (Prioritized or Disposed)
	public static void PrioOverwrite (ValidTouchData candidate, ValidTouchData existing)
	{
		if (Instance.ShouldOverwrite (candidate, existing)) {
			Instance.prioOverwrite (candidate, existing);
			candidate.MsgOnPrio (); //To hold this candidate not to be removed from livingTask first
//			Debug.Log ("<color=yellow> overwritingQ add existing : " + existing + "</color><color=blue>, cand: " + candidate + "</color>");
		}
	}

	/// <summary>
	/// Only called from external, mannually call TDflowCtrl to sort and update
	/// </summary>
	public static void ManUpdate ()
	{
		Instance._Update ();
	}

	public static int GetTDorder (TouchDataBase td)
	{
		if (Instance.livingTDs.ContainsKey (td)) {
			return Instance.livingTDs.Read (td);
		} else {
			return -1;
		}
	}

	public static void SlottedPurge (DPblock selected)
	{
		Instance.slottedPurge (selected);
	}

	#endregion
}