﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanDar
{

	List<DPtrigger> scannedSelTriggs_Debug = new List<DPtrigger> ();
	DPradar sender;

	public DPradar Sender {
		get {
			return sender;
		}
	}

	public ScanDar (DPradar _sender)
	{
		sender = _sender;
	}

	public List<DPtrigger> ScannedTriggs {
		get {
			return scannedSelTriggs_Debug;
		}
	}

	public void UpdateSelTrigs (List<DPtrigger> selList)
	{
		scannedSelTriggs_Debug = selList;
	}

	public bool IsThisRadared (DPtrigger trig)
	{
		return scannedSelTriggs_Debug.Contains (trig);
	}

}
