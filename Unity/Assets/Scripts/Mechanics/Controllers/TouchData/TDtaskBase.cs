﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Tkingless.DorisPuzzle.Models;

//Interface for TDflowCtrlr, so
public interface IStateableTask<TchDType,selfT>:IComparable<selfT>, IEquatable<selfT> 
{
	TchDType Host{ get; }
	bool IsInvoked {get;}
	bool IsInvokable {get;}
	void Invoke ();
	DPblock GetSelectedBlk {get;}
	DPblock GetEngagedBlk { get; }
}

public abstract class TDtaskBase
{
	public delegate void StateChangeDele ();

	protected StateChangeDele stateInvocation;

	protected TouchDataBase _host;
	//each cycle of vapourize and condense, TD reset
	public int TDorder = -1;
	public int subOrder = -1;
	protected bool invoked = false;

	#region Interface of TDflowCtrlr

	//no quite relevant in this class, this attribute
	public virtual List<Trigger> Triggs {
		get {
			return _host.Triggs;
		}
	}

	//Design: note subclass also defined T Host required by IStateableTask
	public virtual TouchDataBase Host{
		get{
			return _host;
		}
	}

	//Design: must be override by subclass for functioning
	public virtual int ProspectNum{
		get{
			return -1;
		}
	}

	//Syntax: for subclass if want to overide this interface func, really need 
	// public override int CompareTo (TDtaskBase other)
	public virtual int CompareTo (TDtaskBase other)
	{
		if (other == null) {
			//this obj is greater
			return 1;
		} else {
			if (this.TDorder == other.TDorder) {
				return this.subOrder.CompareTo (other.subOrder);
			}

			return this.TDorder.CompareTo (other.TDorder);
		}
	}

	public virtual bool Equals (TDtaskBase other)
	{
		if (other == null)
			return false;
		return (this.TDorder.Equals (other.TDorder) && this.subOrder.Equals (other.subOrder));
	}

	static int CompareAscById (TDtaskBase obj1, TDtaskBase obj2)
	{ 
		return obj1.TDorder.CompareTo (obj2.TDorder);
	}

	static int CompareDescById (TDtaskBase obj1, TDtaskBase obj2)
	{ 
		return obj2.TDorder.CompareTo (obj1.TDorder);
	}

	static int CompareAscBySub (TDtaskBase obj1, TDtaskBase obj2)
	{ 
		return obj1.subOrder.CompareTo (obj2.subOrder);
	}

	static int CompareDescBySub (TDtaskBase obj1, TDtaskBase obj2)
	{ 
		return obj2.subOrder.CompareTo (obj1.subOrder);
	}

	public virtual void Invoke(){
		stateInvocation ();
		invoked = true;
	}

	public virtual bool IsInvokable {
		get {
			return false;
		}
	}

	public virtual bool IsInvoked {
		get{
			return invoked;
		}
	}

	public virtual DPblock GetSelectedBlk {
		get{
			return _host.GetSelectedBlk;
		}
	}

	public virtual DPblock GetEngagedBlk {
		get{
			return _host.GetEngagedBlk;
		}
	}
	#endregion

	public override string ToString ()
	{
		return string.Format ("TDtask: {0}, id: {1}, sub: {2}", this.GetType(), TDorder, subOrder);
	}
}
