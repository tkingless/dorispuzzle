﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Tkingless.DorisPuzzle.constants;
using Tkingless.DorisPuzzle.Models;
using Tkingless.DorisPuzzle.Util;

public class ValidTouchData : TouchDataBase, IStateableTchData<ValidTDtask.State>
{
	ValidTDtask.State state = ValidTDtask.State.Disposed;

	public ValidTDtask.State SetState {
		set {
			state = value;
		}
	}

	public ValidTDtask.State GetState {
		get {
			return state;
		}
	}

	public override int GetStateNum {
		get {
			return (int)state;
		}
	}

	public override int SetStateNum {
		set {
			state = (ValidTDtask.State)value;
		}
	}

	public ValidTouchData (TouchingType _type) : base ()
	{
		ttype = _type;
	}

	#region connection construction

	public bool PreCheckAssemble ()
	{
		if (All_Connected ()) {
			return triggHashset.ToList ().TrueForAll (x => x.GetState == Trigger.State.ASSEMBLED_META);
		}
		return false;
	}

	public bool PreCheckDissemble ()
	{
		if (All_Disconnected ()) {
			return triggHashset.ToList ().TrueForAll (x => x.GetState == Trigger.State.DISSEMBLED_META);
		}
		return false;
	}

	public bool AllFreeTP ()
	{
		return triggHashset.ToList ().TrueForAll (x => x.TopPrioirty == null);
	}

	public bool AllTrigaFree ()
	{
		return triggHashset.ToList ().TrueForAll (x => x.Triga.GetState == Trigability.State.FREE);
	}

	#endregion

	#region flow control related

	public void MsgOnPrio ()
	{	
		//Design Block those TD have some trig frozen , never started from TDflowCtrl never
		if (triggHashset.ToList ().Any (trig => trig.IsFrozen)) {
			return;
		}

		ValidTDtask task = new ValidTDtask (1, this, OnPrio);
		TDflowCtrl.RegisterTask (task);
	}

	public void MsgOffPrio ()
	{
		ValidTDtask task = new ValidTDtask (6, this, OffPrio);
		TDflowCtrl.RegisterTask (task);
	}

	public void MsgOnConnected ()
	{
		ValidTDtask task = new ValidTDtask (2, this, OnAllConnected);
		TDflowCtrl.RegisterTask (task);
	}

	public void MsgOffConnected ()
	{
		ValidTDtask task = new ValidTDtask (5, this, OffAllConnected);
		TDflowCtrl.RegisterTask (task);
	}

	public void MsgOnFreezed ()
	{
		ValidTDtask task = new ValidTDtask (3, this, OnFreezed);
		TDflowCtrl.RegisterTask (task);
	}

	public void MsgOffFreezed ()
	{
		ValidTDtask task = new ValidTDtask (4, this, OffFreezed);
		TDflowCtrl.RegisterTask (task);
	}

	void OnPrio ()
	{
		//Design the order is fixed, TP and ActiveTD things set first then DPtrigger callbacks. As state depend on TP and ActiveTD
		triggHashset.ToList ().ForEach (x => x.SetTP (this));

		if (!AllAssignPrioCB ()) {
			Debug.LogError ("<color=red>AllAssignPrioCB faild</color> : " + ttype);
			triggHashset.ToList ().ForEach (x => {
				if ((x.GetState != Trigger.State.ASSEMBLED_META) &&
				    (x.GetState != Trigger.State.ASSEMBLING) &&
				    (x.GetState != Trigger.State.DISSEMBLED_STABLE)) {
					Debug.LogError ("<color=red>x :" + x.DPobj.PrintDebugInfo () + " trigger state</color> : " + x.GetState);
				}
				
			});


			triggHashset.ToList ().
			FindAll (x => x.DPobj.IsSelected).
			ForEach (x => {
				//workaround tkingless/Debug: tmp, force update hdr
				Debug.Log("this happened");
				x.DPobj.DefHdrsToListn ();
			});
		}
		 
		//what to do if all ASSEMBLED_META? MsgOnConnected? no need, it runs just magically well
	}

	public void OffPrio ()
	{
		triggHashset.ToList ().ForEach (x => {
			x.CB_ClearTP (this);
		});
			
		AllResignCB ();
	}

	//once all connected, triggers should remember their neigbour partners and their own connected triggers
	//regardless they are selected or engaged
	void OnAllConnected ()
	{
		bool postCheck;

		triggHashset.ToList ().ForEach (x => x.OnConnected ());
		AllAssignActiveCB ();
		postCheck = triggHashset.ToList ().TrueForAll (x => x.GetState == Trigger.State.ASSEMBLED_STABLE);

		if (!postCheck) {
			Debug.LogError ("OnAllConnected postCheck error, " + ttype);
		}
	}

	void OffAllConnected ()
	{
		bool postCheck;

		triggHashset.ToList ().ForEach (x => x.OnDisconnected ());

		if (!AllAssignPrioCB ()) {
			Debug.LogError ("<color=red>AllAssignPrioCB faild</color> : " + ttype);
		}

		postCheck = triggHashset.ToList ().TrueForAll (x => ((x.GetState == Trigger.State.DISSEMBLED_STABLE) ||
		(x.GetState == Trigger.State.ASSEMBLED_META) ||
		(x.GetState == Trigger.State.ASSEMBLING)));

		if (!postCheck) {
			Debug.LogWarning ("postCheck not right, wt mean?");
			triggHashset.ToList ().ForEach (x => {
				Debug.Log ("<color=yellow> state: " + x.DPobj.PrintDebugInfo () + ", " + x.GetState + "</color>");
			});

		}

	}

	void OnFreezed ()
	{
		triggHashset.ToList ().ForEach (x => {
			x.CB_ClearTP (this);
		});
		AllResignCB ();
	}

	void OffFreezed ()
	{
		sender = GetEngRadar;
		//no copying OnPrio, as on instant frame, the physics not updated,
		triggHashset.ToList ().ForEach (x => x.SetTP (this));
		AutoAssignCBbyDef ();
	}

	public bool AllAssignPrioCB ()
	{
		if (triggHashset.ToList ().TrueForAll (x =>
			(x.GetState == Trigger.State.ASSEMBLED_META) ||
		    (x.GetState == Trigger.State.ASSEMBLING) ||
		    (x.GetState == Trigger.State.DISSEMBLED_STABLE))) { 
			triggHashset.ToList ().
			FindAll (x => x.DPobj.IsSelected).
			ForEach (x => x.DPobj.DefHdrsToListn ());

			return true;
		}

		return false;
	}

	public bool AllAssignActiveCB ()
	{
		if (triggHashset.ToList ().TrueForAll (x =>
			(x.GetState == Trigger.State.DISSEMBLED_META) ||
		    (x.GetState == Trigger.State.DISSEMBLING) ||
		    (x.GetState == Trigger.State.ASSEMBLED_STABLE))) { 
			triggHashset.ToList ().
			FindAll (x => x.DPobj.IsSelected).
			ForEach (x => x.DPobj.DefHdrsToListn ());

			return true;
		}

		return false;
	}

	public void AllResignCB ()
	{
		AutoAssignCBbyDef ();
	}

	void AutoAssignCBbyDef ()
	{
		triggHashset.ToList ().
		FindAll (x => x.DPobj.IsSelected).
		ForEach (x => x.DPobj.DefHdrsToListn ());
	}

	public void StepwisePurge ()
	{
		if (GetState == ValidTDtask.State.Freezed) {
			MsgOffFreezed ();
			MsgOffConnected ();
			MsgOffPrio ();
		} else if (GetState == ValidTDtask.State.Active) {
			MsgOffConnected ();
			MsgOffPrio ();
		} else if (GetState == ValidTDtask.State.Prioritized) {
			MsgOffPrio ();
		}
		TDflowCtrl.ManUpdate ();
	}

	public void OnSlotUrgentPurge ()
	{
		StepwisePurge ();
	}

	public Transform RetreatMonoConncOverlapPivot ()
	{
		DPtrigger engTrig = GetAEngTrig;
		switch (ttype) {
		case TouchingType.eFull1sFull:
		case TouchingType.eFull2sHalf:
			if (engTrig.GetType () != typeof(DPvault)) {
				Debug.LogError ("Not supposed");
			}
			return engTrig.pivot;
		case TouchingType.eHalf1sFull:
		case TouchingType.e2Half2sHalf:
			if (engTrig.GetType () != typeof(DPhalf)) {
				Debug.LogError ("Not supposed");
			}
			return ((DPhalf)engTrig).vault.pivot;
		case TouchingType.eHalf1sHalf:
			if (engTrig.GetType () != typeof(DPhalf)) {
				Debug.LogError ("Not supposed");
			}
			return engTrig.pivot;
		case TouchingType.Point:
			if (engTrig.GetType () != typeof(DPvertex)) {
				Debug.LogError ("Not supposed");
			}
			return engTrig.pivot;
		default:
			Debug.LogError ("Not considered case");
			break;
		}

		return null;
	}

	#endregion

	#region Utility

	public bool SomeTrigOccupied ()
	{
		return !triggHashset.ToList ().TrueForAll (x => !x.IsOccupied);
	}

	public bool AllTrigOccupied ()
	{
		return triggHashset.ToList ().TrueForAll (x => x.IsOccupied);
	}

	#endregion

	public override string ToString ()
	{
		return string.Format ("[<color=green>VTouchData</color>: type={0} GetState={1}, livingIdx={2}]",
			ttype, GetState, TDflowCtrl.GetTDorder (this));
	}
}

public class ValidTouchDataFactory
{

	DPradar sender;
	ValidTouchData buffer = null;

	public ValidTouchDataFactory (DPradar senderEngaged)
	{
		sender = senderEngaged;
	}

	public void SetType (TouchingType _type)
	{
		if (buffer == null) {
			buffer = new ValidTouchData (_type);
			buffer.Sender = sender;
		} else {
			Debug.LogWarning ("Guess should be null here");
		}
	}

	public void AddConnPair (TrigConnection _conn)
	{
		if (buffer != null) {
			buffer.AddConnc (_conn);
		}
	}

	public ValidTouchData Instantiate ()
	{
		ValidTouchData tmp = null;

		if (buffer != null) {
			tmp = buffer;
			buffer = null;
		}

		return tmp;
	}
}
