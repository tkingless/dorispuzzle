﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tkingless.DorisPuzzle.Models;

public class ValidTDtask : TDtaskBase, IStateableTask<ValidTouchData,ValidTDtask>
{

	public enum State //Design like steep stair, must by step by step
	{
		//init
		Disposed = 1,
		//Enter by InvokeSetTP, Exit by InvokeClearTP
		Prioritized = 2,
		//Enter by OnAllConnected, Exit by OnAllDisconnected
		Active = 3,
		// Enter by OnAllFreezed, Exit by OnAllDefreezed
		Freezed = 4,
		Undef = 10
	}

	public new ValidTouchData Host {
		get {
			return (ValidTouchData)_host;
		}
	}

	//for order a TDs' callback: OnPrio (1), OnActive(2), OnFreeze(3), OffFreeeze(4), OffActive(5), OffPrio(6)
	int SetSubOrder {
		set {

			switch (value) {
			case 1:
				prospect = State.Prioritized;
				break;
			case 2:
				prospect = State.Active;
				break;
			case 3:
				prospect = State.Freezed;
				break;
			case 4:
				prospect = State.Active;
				break;
			case 5:
				prospect = State.Prioritized;
				break;
			case 6:
				prospect = State.Disposed;
				break;
			default:
				break;
			}

			subOrder = value;
		}
	}

	public State prospect = State.Undef;

	public override int ProspectNum {
		get {
			return (int)prospect;
		}
	}

	public ValidTDtask (int _order, ValidTouchData inithost, StateChangeDele CB)
	{
		SetSubOrder = _order;
		_host = inithost;
		stateInvocation = CB;
	}

	public override void Invoke ()
	{
		base.Invoke ();
		((ValidTouchData)_host).SetState = prospect;
	}

	public override bool IsInvokable {
		get {
			return IsStepwiseTransition ();
		}
	}

	bool IsStepwiseTransition ()
	{
		//when Msg happen, it checks, the callback would lead to a state
		//check if the state matches the current state transition, 
		//then choose the smallest sub order to act
		//raise error if after biggest sub-order act invoked there are still sub-order act pending

		if (prospect == ((ValidTouchData)_host).GetState) {
			Debug.LogWarning ("duplicated call");
		} else if ((prospect - ((ValidTouchData)_host).GetState) > 1 || (prospect - ((ValidTouchData)_host).GetState) < -1) {
			//			Debug.LogWarning ("wrong transition"); normal, as a lot overwriting task
		} else {
			return true;		
		}

		return false;
	}

	public int GetStepNum {
		get {
			return prospect - ((ValidTouchData)_host).GetState;
		}
	}

	public bool Equals (ValidTDtask other)
	{
		return base.Equals (other);
	}

	//Bnous Should also override == and != operators.

	public int CompareTo (ValidTDtask other)
	{
		return base.CompareTo (other);
	}


}
