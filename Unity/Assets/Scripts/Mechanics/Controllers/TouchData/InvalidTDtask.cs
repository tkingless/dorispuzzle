﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tkingless.DorisPuzzle.Models;

public class InvalidTDtask : TDtaskBase, IStateableTask<InvalidTouchData,InvalidTDtask>
{
	#region Hetergeneous part

	public enum State
	{
		Disposed = 1,
		Connected = 2,

		Undef = 10
	}

	#endregion

	public new InvalidTouchData Host {
		get {
			return (InvalidTouchData)_host;
		}
	}

	int SetSubOrder {
		set {
			switch (value) {
			case 1:
				prospect = State.Connected;
				break;
			case 2:
				prospect = State.Disposed;
				break;
			default:
				break;
			}

			subOrder = value;
		}
	}

	public State prospect = State.Undef;

	public override int ProspectNum {
		get {
			return (int)prospect;
		}
	}

	public InvalidTDtask (int _order, InvalidTouchData inithost, StateChangeDele CB)
	{
		SetSubOrder = _order;
		_host = inithost;
		stateInvocation = CB;
	}

	public override void Invoke()
	{
		base.Invoke ();
		((InvalidTouchData)_host).SetState = prospect;
	}

	public override bool IsInvokable {
		get {
			return prospect != ((InvalidTouchData)_host).GetState;
		}
	}

	public int CompareTo (InvalidTDtask other)
	{
		return base.CompareTo (other);
	}

	public bool Equals (InvalidTDtask other)
	{
		return base.Equals (other);
	}

	public override DPblock GetSelectedBlk {
		get {
			return _host.GetSelectedBlk;
		}
	}

	public override DPblock GetEngagedBlk {
		get {
			return _host.GetEngagedBlk;
		}
	}

}
