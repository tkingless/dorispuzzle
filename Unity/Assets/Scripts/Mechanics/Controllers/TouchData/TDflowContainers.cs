﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Tkingless.DorisPuzzle.Models;
using Tkingless.DorisPuzzle.constants;

namespace Tkingless.DorisPuzzle.TouchData
{

	class LivingTDs: CRUDdictBase<TouchDataBase,int>
	{

		public List<T> GetLivingTDofState<T> (int _state)
		{
			return GetKey<T> ().
				FindAll (td => td.GetStateNum.Equals (_state)).
				Cast<T> ().ToList ();
		}

		public List<T> GetLivingTDofNotState<T> (int _state)
		{
			return GetKey<T> ().
				FindAll (td => !td.GetStateNum.Equals (_state)).
				Cast<T> ().ToList ();
		}

		public List<TouchDataBase> Keys {
			get {
				return _dict.Keys.ToList ();
			}
		}

		public bool ContainsKey (TouchDataBase key)
		{
			return _dict.ContainsKey (key);
		}

		public bool ContainsValue (int val)
		{
			return _dict.ContainsValue (val);
		}

		public void Clear ()
		{
			_dict.Clear ();
		}

		#region exclusive to Valid pair

		public bool IsAllFrozen {
			get {
				var VTDs = GetKey<ValidTouchData> ().Cast<ValidTouchData> ().ToList ();
				return VTDs.TrueForAll (td => td.GetState == ValidTDtask.State.Freezed);
			}
		}

		#endregion

		List<TouchDataBase> GetKey<T> ()
		{
			try {
				List<TouchDataBase> TDs = new List<TouchDataBase> ();
				_dict.Keys.Where (td => td.GetType () == typeof(T)).ToList ().
				ForEach (vtd => TDs.Add (vtd));

				return TDs;
			} catch (System.Exception e) {
				Debug.LogError ("e: " + e);
				return null;
			}
		}

		public List<TouchDataBase> GetIntersectedWith (IEnumerable<Trigger> selTriggs)
		{
			return Keys.FindAll (x => x.Triggs.Intersect (selTriggs).Any ());
		}

		public List<T> GetTypeTDs<T> ()
		{
			return GetKey<T> ().Cast<T> ().ToList ();
		}
	}

	//<existing,candidate> pair
	class OverwriteQueue : CRUDdictBase<ValidTouchData,ValidTouchData>
	{

		public bool ContainsKey (ValidTouchData key)
		{
			return _dict.ContainsKey (key);
		}

		public bool ContainsValue (ValidTouchData key)
		{
			return _dict.ContainsValue (key);
		}

		public int Count {
			get {
				return _dict.Count;
			}
		}

		public List<ValidTouchData> Keys {
			get {
				return _dict.Keys.ToList ();
			}
		}

		public List<ValidTouchData> GetReplacealeTDs ()
		{
			return _dict.Keys.Where (td => td.GetState == ValidTDtask.State.Disposed ||
			td.GetState == ValidTDtask.State.Prioritized).ToList ();
		}

		public void Clear ()
		{
			_dict.Clear ();
		}

		#region Valid pair exclusive

		public List<ValidTDtask> ExceptPendedTask (List<ValidTDtask> sorted)
		{
			return sorted.FindAll (x => !_dict.Values.Contains ((ValidTouchData)x.Host)).ToList ();
		}

		#endregion
	}

	class PromiscuousTasks : TasksBase<TDtaskBase>
	{
		List<TDtaskBase> _list;

		public PromiscuousTasks ()
		{
			_list = new List<TDtaskBase> ();
			SetupNumerable (_list);
			//TODO tkingless/optimization: or pattern is like _numerables = new List<ValidTDtask();
		}

		public int Count {
			get {
				return _list.Count;
			}
		}

		public void Clear ()
		{
			_list.Clear ();
		}

		public List<TDtaskBase> GetAll {
			get {
				return _list;
			}
		}

		public void Add (TDtaskBase task)
		{
			_list.Add (task);
		}

		public bool CheckTaskAddable (TDtaskBase task)
		{
			var vtdts = GetTypeOf<ValidTDtask> ();
			return !vtdts.Exists (x => x.Equals (task)) &&
			task.Host.GetStateNum != task.ProspectNum;
		}

		public void Remove (TDtaskBase task)
		{
			_list.Remove (task);
		}

		//Mixed with Invalid and valid ones.
		public List<TDtaskBase> GetMixSorted {
			get {
				List<TDtaskBase> tmp = _list.ToList ();
				tmp.Sort ();
				return tmp;
			}
		}

		public List<TouchDataBase> GetIdleDisposedTDs (List<TouchDataBase> disposed)
		{
			
			List<TouchDataBase> taskedTDs = _list.Select (x => x.Host).ToList ();
			return disposed.Except (taskedTDs).ToList ();
		}

		public void DeregInvokedTasks ()
		{
			_list.RemoveAll (invokedTask => invokedTask.IsInvoked);
		}

		public List<TDtaskBase> GetTasksByHost (TouchDataBase _host)
		{
			return _list.FindAll (x => x.Host == _host).ToList ();
		}

		//TODO tkingless/optimization: consider move to parent class
		public void RemoveAllByHost (TouchDataBase _host)
		{
			_list.RemoveAll (x => x.Host == _host);
		}

		#region extend

		public List<ValidTDtask> GetSortedValidTaskList {
			get {
				List<ValidTDtask> tmp = GetTypeTaskList<ValidTDtask> ();
				tmp.Sort ();
				return tmp;
			}
		}

		public List<InvalidTDtask> GetSortedInvalidTaskList {
			get {
				List<InvalidTDtask> tmp = GetTypeTaskList<InvalidTDtask> ();
				tmp.Sort ();
				return tmp;
			}
		}

		List<TDtaskBase> GetTypeOf<T> ()
		{
			try {
				List<TDtaskBase> TDTs = new List<TDtaskBase> ();
				_list.Where (td => td.GetType () == typeof(T)).ToList ().
				ForEach (vtdt => TDTs.Add (vtdt));

				return TDTs;
			} catch (System.Exception e) {
				Debug.LogError ("e: " + e);
				return null;
			}
		}

		public List<T> GetTypeTaskList<T> ()
		{
			return GetTypeOf<T> ().Cast<T> ().ToList ();
		}

		#endregion

		#region exclusive to Valid task, touchData pairs

		public bool AnyHigherOrderAndIntersectTrigsWith (int candTDorder, ValidTouchData overwrittingTD)
		{
			var vtdts = GetTypeOf<ValidTDtask> ();
			if (vtdts.Any (td => td.TDorder > candTDorder &&
			    td.Triggs.Intersect (overwrittingTD.Triggs).Any ()
			    ))
				return true;

			return false;
		}

		public bool AnyLowerOrderAndIntersectTrigsWith (int transTDorder, ValidTouchData transTask)
		{
			var vtdts = GetTypeOf<ValidTDtask> ();
			if (vtdts.FindAll (x => x.TDorder < transTDorder).
				Any (x => x.Triggs.Intersect (transTask.Triggs).Any ())) {
				return true;
			}

			return false;
		}

		public List<ValidTouchData> AnyLowerOrderAndIntersectTrigsList (int transTDorder, ValidTouchData transTask)
		{
			var vtdts = GetTypeOf<ValidTDtask> ();
			return vtdts.FindAll (x => x.TDorder < transTDorder).
				FindAll (x => x.Triggs.Intersect (transTask.Triggs).Any ()).ToList ().
				Select (x => (ValidTouchData)x.Host).ToList ();
		}

		#endregion


	}

	class OrderedTasks : TasksBase<TDtaskBase>
	{
		Queue<TDtaskBase> _queue;

		public OrderedTasks ()
		{
			_queue = new Queue<TDtaskBase> ();
			SetupNumerable (_queue);
		}

		public void Enqueue (TDtaskBase task)
		{
			_queue.Enqueue (task);
		}

		public void ExecuteInOrder (out bool updated)
		{

			if (_queue.Count > 0) {
				updated = true;
			} else {
				updated = false;
			}

			while (_queue.Count > 0) {
				TDtaskBase cur = _queue.Dequeue ();
//				Debug.Log (cur.ToString () + ", " + cur.Host.ToString ());
				//TODO tkingless/message: this is tmp only, true design of coz not putting here
				#region tmp
//				MsgMng.Instance.show(new DPdiagloue("Function Not Ready!", DPMessage.STATE.danger));
				if (FullUISceneMng.isFullUIScene) {
					if (cur.GetType () == typeof(ValidTDtask)) {
						if (cur.subOrder == 2) {
							MsgMng.ShowGoodMsg(DPconstant.EDU_GOOD_MSG);
						}
					} else if (cur.GetType () == typeof(InvalidTDtask)) {
						if (cur.subOrder == 1) {
							MsgMng.ShowBadMsg(DPconstant.EDU_BAD_MSG);
						}
					}
				}
				#endregion
				cur.Invoke ();
			}
		}

		public List<TDtaskBase> GetTasksByHost (TouchDataBase _host)
		{
			return _queue.ToList ().FindAll (x => x.Host == _host).ToList ();
		}

		public List<TDtaskBase> GetTasksOf (DPblock sel, DPblock eng)
		{
			return _queue.ToList ().FindAll (x => x.GetSelectedBlk == sel &&
			x.GetEngagedBlk == eng).ToList ();
		}

		public List<TDtaskBase> GetTasksOf (int _subOrder)
		{
			return _queue.ToList ().FindAll (x => x.subOrder == _subOrder).ToList ();
		}

		public int Count {
			get {
				return _queue.Count;
			}
		}
	}

	class FrozenTDs : TasksBase<ValidTouchData>
	{
		HashSet<ValidTouchData> _hset;

		public FrozenTDs ()
		{
			_hset = new HashSet<ValidTouchData> ();
			SetupNumerable (_hset);
		}

		public List<ValidTouchData> GetTDsRelatedToSelected ()
		{
			return _hset.ToList ().FindAll (x => x.IsAnyFromSelected).ToList ();
		}

		public void Add (ValidTouchData td)
		{
			_hset.Add (td);
		}

		public void Remove (ValidTouchData td)
		{
			_hset.Remove (td);
		}
	}

	class TasksBase<T>
	{
		//TODO tkingless/optimization: this base should have a sense of "holding index"
		protected IEnumerable<T> _numerables;

		protected void SetupNumerable (IEnumerable<T> _num)
		{
			//TODO tkingless/optimization: better be List<> obj, so that no need so many ToList()
			_numerables = _num;
		}
	}

	//Note that Dictionary<K,V> : IEnumerable<KeyValuePair<Key,Val>>
	class CRUDdictBase<mKey,mVal>: TasksBase<KeyValuePair<mKey,mVal>>
	{

		//		IEnumerable<KeyValuePair<Key,Val>> _dict;
		protected Dictionary<mKey,mVal> _dict;

		public CRUDdictBase ()
		{
			_dict = new Dictionary<mKey, mVal> ();
			SetupNumerable (_dict);
		}

		public bool Create (mKey key, mVal val)
		{
			if (!_dict.ContainsKey (key)) {
				_dict.Add (key, val);
				return true;
			} else {
				Debug.LogWarning ("Already contains key");
				return false;
			}
		}

		public mVal Read (mKey key)
		{
			mVal val = default(mVal);

			if (_dict.ContainsKey (key)) {
				_dict.TryGetValue (key, out val);

			} else {
				Debug.LogWarning ("No such key");
			}

			return val;
		}

		public void Upsert (mKey key, mVal val)
		{
			if (_dict.ContainsKey (key)) {
				_dict [key] = val;
			} else {
//				Debug.Log ("Create() instead");
				Create (key, val);
			}
		}

		public bool Remove (mKey key)
		{
			if (_dict.ContainsKey (key)) {
				_dict.Remove (key);
				return true;
			} else {
				Debug.Log ("no such key");
				return false;
			}
		}

		public bool RemoveByVal (mVal val)
		{
			var revLookup = _dict.First (kvp => kvp.Value.Equals (val));

			if (!revLookup.Equals (default(mVal))) {
				_dict.Remove (revLookup.Key);
				return true;
			} else {
				return false;
			}
		}
			
	}
}
