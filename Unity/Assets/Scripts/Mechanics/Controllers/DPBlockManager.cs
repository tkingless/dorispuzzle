﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Tkingless.DorisPuzzle.constants;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DPBlockManager : SceneSingleton<DPBlockManager>
{
	//Bonus each instance callback can only attend an event once
	//ref: https://msdn.microsoft.com/en-us/library/system.delegate.getinvocationlist%28v=vs.110%29.aspx
	BlockObsvrs blkObsvr;
	TchDataObsvrs tchObsvr;
	TchDebugObsvrs tchDebugObsvr;
	RadarObsvrs radarObsvr;
	NoMsgObsvrs noMsgObsvr;
	TriggerObsvrs triggerObsvr;

	private static DPBlockManager blkMng;

	public Transform _DragAnchor, _PlaygroundAnchor;

	#region global Event messaging system & Instance

	void Init ()
	{
		if (blkObsvr == null) {
			blkObsvr = new BlockObsvrs ();
		}
		if (tchObsvr == null) {
			tchObsvr = new TchDataObsvrs ();
		}
		if (tchDebugObsvr == null) {
			tchDebugObsvr = new TchDebugObsvrs ();
		}
		if (radarObsvr == null) {
			radarObsvr = new RadarObsvrs ();
		}
		if (triggerObsvr == null) {
			triggerObsvr = new TriggerObsvrs ();
		}
		if (noMsgObsvr == null) {
			noMsgObsvr = new NoMsgObsvrs ();
		}

	}

	public static void StartListening (string eventName, UnityAction<DPblock> listener)
	{
		Instance.blkObsvr.StartListening (eventName, listener);
	}

	public static void StartListening (string eventName, UnityAction<ValidTouchData> listener)
	{
		Instance.tchObsvr.StartListening (eventName, listener);
	}

	public static void StartListening (string eventName, UnityAction<ScanDar> listener)
	{
		Instance.tchDebugObsvr.StartListening (eventName, listener);
	}

	public static void StartListening (string eventName, UnityAction<DPradar> listener)
	{
		Instance.radarObsvr.StartListening (eventName, listener);
	}

	public static void StartListening (string eventName, UnityAction<DPtrigger,DPtrigger> listener)
	{
		Instance.triggerObsvr.StartListening (eventName, listener);
	}

	public static void StartListening (string eventName, UnityAction listener)
	{
		Instance.noMsgObsvr.StartListening (eventName, listener);
	}

	public static void StopListening (string eventName, UnityAction<DPblock> listener)
	{
		Instance.blkObsvr.StopListening (eventName, listener);
	}

	public static void StopListening (string eventName, UnityAction<ValidTouchData> listener)
	{
		Instance.tchObsvr.StopListening (eventName, listener);
	}

	public static void StopListening (string eventName, UnityAction<ScanDar> listener)
	{
		Instance.tchDebugObsvr.StopListening (eventName, listener);
	}

	public static void StopListening (string eventName, UnityAction<DPradar> listener)
	{
		Instance.radarObsvr.StopListening (eventName, listener);
	}

	public static void StopListening (string eventName, UnityAction<DPtrigger,DPtrigger> listener)
	{
		Instance.triggerObsvr.StopListening (eventName, listener);
	}

	public static void StopListening (string eventName, UnityAction listener)
	{
		Instance.noMsgObsvr.StopListening (eventName, listener);
	}

	public static void TriggerEvent (string eventName, DPblock eventBlk)
	{
		Instance.blkObsvr.TriggerEvent (eventName, eventBlk);
	}

	public static void TriggerEvent (string eventName, ValidTouchData td)
	{
		Instance.tchObsvr.TriggerEvent (eventName, td);
	}

	public static void TriggerEvent (string eventName, ScanDar debugObj)
	{
		Instance.tchDebugObsvr.TriggerEvent (eventName, debugObj);
	}

	public static void TriggerEvent (string eventName, DPradar radar)
	{
		Instance.radarObsvr.TriggerEvent (eventName, radar);
	}

	public static void TriggerEvent (string eventName, DPtrigger trigg, DPtrigger other)
	{
		Instance.triggerObsvr.TriggerEvent (eventName, trigg, other);
	}

	public static void TriggerEvent (string eventName)
	{
		Instance.noMsgObsvr.TriggerEvent (eventName);
	}

	#endregion

	#region DP logic

	private DPblock selected = null;

	public static DPblock Selected {
		get {
			return DPBlockManager.Instance.selected;
		}
	}

	void OnEnable ()
	{
		StartListening (DPconstant.DP_REG_BLK, MngRegisterBlk);
		StartListening (DPconstant.DP_PTR_LEAVE_DEREG_BLK, MngDeregisterBlk);
	}

	void OnDisable ()
	{
		StopListening (DPconstant.DP_REG_BLK, MngRegisterBlk);
		StopListening (DPconstant.DP_PTR_LEAVE_DEREG_BLK, MngDeregisterBlk);
	}


	void OnApplicationFocus(bool hasFocus)
	{
//		Debug.Log ("OnApplicationFocus");
	}

	IEnumerator OnApplicationPause(bool pauseStatus)
	{
		//if actioning, pretend OnEndDrag
		if (selected != null) {
			if (selected.IsActioning) {
				selected.OnPause_ActionEndDrag ();
			}
		}

		while (selected != null) {
			yield return null;
		}

		yield return null;
	}

	IEnumerator HookOffActioning(){

		yield return null;
	}

	List<DPblock> allBlks;

	public List<DPblock> AllBlks {
		get {
			return allBlks;
		}
	}

	public static void SetAllBlks (List<DPblock> aList){
		Instance.allBlks = aList;
	}

	new void Awake(){

		base.Awake ();

		Init ();

		if (_DragAnchor == null) {
			Debug.LogWarning ("you seem to forget setting up DragAnchor, trying to fix...");
			_DragAnchor = GameObject.Find ("DragAnchor").transform;
			if (_DragAnchor == null) {
				Debug.LogError ("Fatal in scene in setup");
			}
		}

		if (_PlaygroundAnchor == null) {
			Debug.LogWarning ("you seem to forget setting up _PlaygroundAnchorPlaygroundAnchor, trying to fix...");
			_PlaygroundAnchor = GameObject.Find ("Playground").transform;
			if (_PlaygroundAnchor == null) {
				Debug.LogError ("Fatal in scene in setup");
			}
		}

		//workaround if not 24puzzle scene here
		if (!DPSceneManager.IsPuzScene()) {
			SetAllBlks(((DPblock[])GameObject.
				FindObjectsOfType (typeof(DPblock))).
				ToList ());
		}
	}

	/// <summary>
	/// incoming blk should never be null, but sometimes selected yes
	/// </summary>
	/// <param name="blk">Blk.</param>
	/// <param name="eventData">Event data.</param>
	void MngRegisterBlk (DPblock blk)
	{
		if (blk == null)
			return;

		if (selected != null) {
			if (!selected.Free) {
				return;
			}
		}
//		Debug.Log ("MngRegisterBlk()");
		if (blk != null && blk.Free) {
			Instance.selected = blk;
			Instance.selected.OnRegister ();
			/* ? Error */
			//selected.OnRegister ();
		}

	}

	/// <summary>
	/// incoming blk should never be null, but sometimes selected yes
	/// </summary>
	/// <param name="blk">Blk.</param>
	/// <param name="eventData">Event data.</param>
	void MngDeregisterBlk (DPblock blk)
	{
		if (blk == null)
			return;
		
		if (selected != null) {
			if (selected != blk) {
				Debug.LogWarning ("selected blk: " + selected + " is not equal to blk to release: " + blk);
				return;
			}
		}
//		Debug.Log ("MngDeregisterBlk()");
//		Debug.Log ("selection lock released: " + selected.name);
		Instance.selected.OnDeregister(null);
		Instance.selected = null;

		PostCheckAfterDeReg ();
	}

	void PostCheckAfterDeReg ()
	{
		if (allBlks == null) {
			return;
		}

		List<DPblock> inINNER = allBlks.FindAll (x => x.massager.CurrentReign == GestureMessager.ReignType.INNER);

		if (inINNER.Count >= 2) {
			Debug.LogWarning ("strange case");
		}

		if (inINNER.Count > 0) {
			DPblock hovered = inINNER.First ();

			if (!hovered.IsSelected)
				DPBlockManager.TriggerEvent (DPconstant.DP_REG_BLK, hovered);
		}
	}

	#endregion

	#region static
	/// <summary>
	/// Gets the get drag anchor.
	/// </summary>
	/// <value>The get drag anchor.</value>
	public static Transform GetDragAnchor {
		get{
			return ((DPBlockManager)Instance)._DragAnchor;
		}
	}

	public static Transform GetPlaygroundAnchor {
		get{
			return ((DPBlockManager)Instance)._PlaygroundAnchor;
		}
	}
	#endregion
}