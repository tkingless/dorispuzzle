﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Tkingless.DorisPuzzle.constants;

public abstract class evtListenTmpl <EventClass> : IDPevent where EventClass : class, new()
{

	protected Dictionary <string, EventClass> eventDictionary;

	public evtListenTmpl ()
	{
		eventDictionary = new Dictionary<string,EventClass> ();
	}

	public virtual void StartListening (string eventName, UnityAction listener)
	{
		EventClass thisEvent = null;
		if (eventDictionary.TryGetValue (eventName, out thisEvent)) {
			AddListener (thisEvent, listener);
		} else {
			thisEvent = new EventClass ();
			AddListener (thisEvent, listener);
			eventDictionary.Add (eventName, thisEvent);
		}
	}

	public virtual void StopListening (string eventName, UnityAction listener)
	{
		EventClass thisEvent = null;
		if (eventDictionary.TryGetValue (eventName, out thisEvent)) {
			RemoveListener (thisEvent, listener);
		}
	}

	public virtual void TriggerEvent (string eventName)
	{
		EventClass thisEvent = null;
		if (eventDictionary.TryGetValue (eventName, out thisEvent)) {
			Invoke (thisEvent);
		}
	}

	protected virtual EventClass CreateInstance ()
	{
		return default(EventClass);
	}

	public virtual void AddListener (EventClass evt, UnityAction call)
	{
	}

	public virtual void RemoveListener (EventClass evt, UnityAction call)
	{
	}

	public virtual void Invoke (EventClass evt)
	{
	}
}

public abstract class evtListenTmpl <EventClass, Param> : IDPevent<Param> where EventClass : class, new()
{

	protected Dictionary <string, EventClass> eventDictionary;

	public evtListenTmpl ()
	{
		eventDictionary = new Dictionary<string,EventClass> ();
	}

	public virtual void StartListening (string eventName, UnityAction<Param> listener)
	{
		EventClass thisEvent = null;
		if (eventDictionary.TryGetValue (eventName, out thisEvent)) {
			AddListener (thisEvent, listener);
		} else {
			thisEvent = new EventClass ();
			AddListener (thisEvent, listener);
			eventDictionary.Add (eventName, thisEvent);
		}
	}

	public virtual void StopListening (string eventName, UnityAction<Param> listener)
	{
		EventClass thisEvent = null;
		if (eventDictionary.TryGetValue (eventName, out thisEvent)) {
			RemoveListener (thisEvent, listener);
		}
	}

	public virtual void TriggerEvent (string eventName, Param msg)
	{
		EventClass thisEvent = null;
		if (eventDictionary.TryGetValue (eventName, out thisEvent)) {
			Invoke (thisEvent, msg);
		}
	}

	protected virtual EventClass CreateInstance ()
	{
		return default(EventClass);
	}

	public virtual void AddListener (EventClass evt, UnityAction<Param> call)
	{
	}

	public virtual void RemoveListener (EventClass evt, UnityAction<Param> call)
	{
	}

	public virtual void Invoke (EventClass evt, Param msg)
	{
	}
}

public abstract class evtListenTmpl <EventClass, Param1, Param2> : IDPevent<Param1, Param2> where EventClass : class, new()
{

	protected Dictionary <string, EventClass> eventDictionary;

	public evtListenTmpl ()
	{
		eventDictionary = new Dictionary<string,EventClass> ();
	}

	public virtual void StartListening (string eventName, UnityAction<Param1,Param2> listener)
	{
		EventClass thisEvent = null;
		if (eventDictionary.TryGetValue (eventName, out thisEvent)) {
			AddListener (thisEvent, listener);
		} else {
			thisEvent = new EventClass ();
			AddListener (thisEvent, listener);
			eventDictionary.Add (eventName, thisEvent);
		}
	}

	public virtual void StopListening (string eventName, UnityAction<Param1,Param2> listener)
	{
		EventClass thisEvent = null;
		if (eventDictionary.TryGetValue (eventName, out thisEvent)) {
			RemoveListener (thisEvent, listener);
		}
	}

	public virtual void TriggerEvent (string eventName, Param1 msg1, Param2 msg2)
	{
		EventClass thisEvent = null;
		if (eventDictionary.TryGetValue (eventName, out thisEvent)) {
			Invoke (thisEvent, msg1, msg2);
		}
	}

	protected virtual EventClass CreateInstance ()
	{
		return default(EventClass);
	}

	public virtual void AddListener (EventClass evt, UnityAction<Param1,Param2> call)
	{
	}

	public virtual void RemoveListener (EventClass evt, UnityAction<Param1,Param2> call)
	{
	}

	public virtual void Invoke (EventClass evt, Param1 msg1, Param2 msg2)
	{
	}
}

public interface IDPevent
{
	void StartListening (string name, UnityAction call);

	void TriggerEvent (string name);

	void StopListening (string name, UnityAction call);
}

public interface IDPevent <T>
{
	void StartListening (string name, UnityAction<T> call);

	void TriggerEvent (string name, T param);

	void StopListening (string name, UnityAction<T> call);
}

public interface IDPevent <P1, P2>
{
	void StartListening (string name, UnityAction<P1,P2> call);

	void TriggerEvent (string name, P1 param, P2 param2);

	void StopListening (string name, UnityAction<P1,P2> call);
}

//using the templete

#region first define the UnityEvent<T..> class
[System.Serializable]
public class DPBlkEventData : UnityEvent<DPblock>
{
}

[System.Serializable]
public class NoMsgEvent : UnityEvent
{
}

[System.Serializable]
public class DP_TDataEvent : UnityEvent<ValidTouchData>
{
}

[System.Serializable]
public class DP_TDataDebugEvent : UnityEvent<ScanDar>
{
}

[System.Serializable]
public class DPradarEvent : UnityEvent<DPradar>
{
}

[System.Serializable]
public class DPtriggEvent : UnityEvent<DPtrigger,DPtrigger>
{
}
#endregion

#region then define the tmpl classes
public class BlockObsvrs : evtListenTmpl<DPBlkEventData,DPblock>
{

	public override void AddListener (DPBlkEventData evt, UnityAction<DPblock> call)
	{
		evt.AddListener (call);
	}

	public override void RemoveListener (DPBlkEventData evt, UnityAction<DPblock> call)
	{
		evt.RemoveListener (call);
	}

	public override void Invoke (DPBlkEventData evt, DPblock msg)
	{
		evt.Invoke (msg);
	}
}

public class TchDataObsvrs : evtListenTmpl<DP_TDataEvent,ValidTouchData>
{

	public override void AddListener (DP_TDataEvent evt, UnityAction<ValidTouchData> call)
	{
		evt.AddListener (call);
	}

	public override void RemoveListener (DP_TDataEvent evt, UnityAction<ValidTouchData> call)
	{
		evt.RemoveListener (call);
	}

	public override void Invoke (DP_TDataEvent evt, ValidTouchData msg)
	{
		evt.Invoke (msg);
	}
}

public class TchDebugObsvrs : evtListenTmpl<DP_TDataDebugEvent,ScanDar>
{

	public override void AddListener (DP_TDataDebugEvent evt, UnityAction<ScanDar> call)
	{
		evt.AddListener (call);
	}

	public override void RemoveListener (DP_TDataDebugEvent evt, UnityAction<ScanDar> call)
	{
		evt.RemoveListener (call);
	}

	public override void Invoke (DP_TDataDebugEvent evt, ScanDar msg)
	{
		evt.Invoke (msg);
	}
}

public class RadarObsvrs : evtListenTmpl<DPradarEvent,DPradar>
{

	public override void AddListener (DPradarEvent evt, UnityAction<DPradar> call)
	{
		evt.AddListener (call);
	}

	public override void RemoveListener (DPradarEvent evt, UnityAction<DPradar> call)
	{
		evt.RemoveListener (call);
	}

	public override void Invoke (DPradarEvent evt, DPradar msg)
	{
		evt.Invoke (msg);
	}
}


public class TriggerObsvrs : evtListenTmpl<DPtriggEvent,DPtrigger,DPtrigger>
{

	public override void AddListener (DPtriggEvent evt, UnityAction<DPtrigger,DPtrigger> call)
	{
		evt.AddListener (call);
	}

	public override void RemoveListener (DPtriggEvent evt, UnityAction<DPtrigger,DPtrigger> call)
	{
		evt.RemoveListener (call);
	}

	public override void Invoke (DPtriggEvent evt, DPtrigger msg1, DPtrigger msg2)
	{
		evt.Invoke (msg1, msg2);
	}
}

public class NoMsgObsvrs : evtListenTmpl<NoMsgEvent>
{
	public override void AddListener (NoMsgEvent evt, UnityAction call)
	{
		evt.AddListener (call);
	}

	public override void RemoveListener (NoMsgEvent evt, UnityAction call)
	{
		evt.RemoveListener (call);
	}

	public override void Invoke (NoMsgEvent evt)
	{
		evt.Invoke ();
	}
}
#endregion



