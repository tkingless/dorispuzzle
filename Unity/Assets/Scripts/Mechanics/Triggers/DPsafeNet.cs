﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DPsafeNet : MonoBehaviour {

	public enum Role
	{
		Net,
		Ball
	}

	public Role mRole;
	public DPblock parentBlk;

	void OnTriggerEnter2D (Collider2D other)
	{
		DPsafeNet Net = other.GetComponent<DPsafeNet> ();

		if (Net.mRole != Role.Net) {
			return;
		}
		if (mRole != Role.Ball) {
			return;
		}

		if (parentBlk.IsActioning) {
			parentBlk.ActiveAction.OffOutBound ();
		}
			
	}

	void OnTriggerExit2D (Collider2D other)
	{
		DPsafeNet Net = other.GetComponent<DPsafeNet> ();

		if (Net.mRole != Role.Net) {
			return;
		}
		if (mRole != Role.Ball) {
			return;
		}

		if (parentBlk.IsActioning) {
			parentBlk.ActiveAction.OnOutBound ();
		}

	}
}
