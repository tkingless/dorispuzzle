﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Tkingless.DorisPuzzle.constants;

public class DPboundary : MonoBehaviour
{

	public DPblock parentBlk;
	HashSet<DPblock> engOverlap = new HashSet<DPblock> ();
	public bool isSlotted = false;

	void Start ()
	{
		//The scene may not have slot feature
		if (PuzzleSlot.CheckExistInScene ()) {
			if (parentBlk.transform.parent.Equals (PuzzleSlot.GetTrans)) {
				isSlotted = true;
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (!Validate (other))
			return;

		//Only trigger overlap event from perspective of the Selected
		if (DPBlockManager.Selected == parentBlk) {
			DPblock theEngaged = other.GetComponent<DPboundary> ().parentBlk;
			StartCoroutine ("DELAY_TRIGGER", theEngaged);
		}

	}

	//Why need this: when OntriggerEnter called() in both instances of gameobjects,
	//higher priorty instance execute first, so the selected trigger event before
	//non selected change state
	IEnumerator DELAY_TRIGGER (DPblock engaged)
	{
		yield return new WaitForEndOfFrame ();
		DPBlockManager.TriggerEvent (DPconstant.DP_OVERLAP_ON);
		engaged.uiObj.OnEngagedOverlap ();
		engOverlap.Add (engaged);
	}

	void OnTriggerExit2D (Collider2D other)
	{
		if (!Validate (other))
			return;

		if (DPBlockManager.Selected == parentBlk) {
			DPblock theEngaged = other.GetComponent<DPboundary> ().parentBlk;
			engOverlap.Remove (theEngaged);
			theEngaged.uiObj.OffEngagedOverlap ();
			DPBlockManager.TriggerEvent (DPconstant.DP_OVERLAP_OFF);
		}

	}

	bool Validate (Collider2D other)
	{
		
		if (other.tag.Equals ("PuzzleSlot"))
			return false;

		if (isSlotted)
			return false;

		if (DPBlockManager.Selected == null)
			return false;

		if (DPBlockManager.Selected.Free)
			return false;

		return true;
	}

	public void OnSlotted ()
	{
		CleanOverlapData ();
		isSlotted = true;
	}

	public void OffSlotted ()
	{
		isSlotted = false;
	}

	void CleanOverlapData ()
	{
		
		engOverlap.ToList ().ForEach (x => {
			x.uiObj.OffEngagedOverlap ();
		});
		engOverlap.Clear ();

		#if UNITY_EDITOR
		if(!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode){
		return;
		}
		#endif 

		DPBlockManager.TriggerEvent (DPconstant.DP_OVERLAP_OFF);
	}
}
