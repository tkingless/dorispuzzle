﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tkingless.DorisPuzzle.constants;
using Tkingless.DorisPuzzle.Models;
using UnityEngine;

[RequireComponent (typeof(CircleCollider2D))]
/// <summary>
/// DPvertex only do point touching, prohibit edge coming closed
/// </summary>
public class DPvertex : DPtrigger
{
	#region excluded triggers

	public DPtrigger Lvault, Lpt, LLvault, Rvault, Rpt, RRvault;

	#endregion

	void Awake ()
	{
//		excludeList = new List<DPtrigger> () {this, Lpt, Lvault, ((DPvault)Lvault).left, ((DPvault)Lvault).right,
//			LLvault, ((DPvault)LLvault).left, ((DPvault)LLvault).right,
//			Rpt, Rvault, ((DPvault)Rvault).left, ((DPvault)Rvault).right,
//			RRvault, ((DPvault)RRvault).left, ((DPvault)RRvault).right
//		};
//		excludeList = new List<DPtrigger> () {this, Lpt, Lvault, ((DPvault)Lvault).left, ((DPvault)Lvault).right,
//			((DPvault)LLvault).right,
//			Rpt, Rvault, ((DPvault)Rvault).left, ((DPvault)Rvault).right,
//			((DPvault)RRvault).left
//		};

		excludeList = new List<DPtrigger> ();
	}

	protected override void Start ()
	{
		base.Start ();
		markColor = Color.yellow;
	}
		
}

