﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tkingless.DorisPuzzle.constants;

public class DPgrpRadar : MonoBehaviour {

	public enum Role {
		Grouped,
		Obstabcle,
		Undef
	}

	public Role mRole;
	public DPblock parentBlk;

	//Countpler
	List<DPgrpRadar> _obstacles = new List<DPgrpRadar>();

	public bool IsForbidden {
		get{
			return _obstacles.Count > 0;
		}
	}

	public void Setup(Role _role){
		mRole = _role;
	}

	public void Clear(){
		_obstacles.Clear ();
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		DPgrpRadar Net = other.GetComponent<DPgrpRadar>();

		if (Net == null) {
			return;
		}

		if (Net.mRole != Role.Obstabcle || mRole != Role.Grouped || parentBlk == null) {
			return;
		}

		#region grouped blk
		_obstacles.Add(Net);
		DPBlockManager.TriggerEvent(DPconstant.DP_GRP_ACTIONING_ON_FORBIDDEN,parentBlk);
		#endregion
	}

	void OnTriggerExit2D (Collider2D other)
	{
		DPgrpRadar Net = other.GetComponent<DPgrpRadar>();

		if (Net == null) {
			return;
		}

		if (Net.mRole != Role.Obstabcle || mRole != Role.Grouped || parentBlk == null) {
			return;
		}

		#region grouped blk
		_obstacles.Remove(Net);
		if(_obstacles.Count == 0){
			DPBlockManager.TriggerEvent(DPconstant.DP_GRP_ACTIONING_OFF_FORBIDDEN,parentBlk);
		}
		#endregion
	}

}
