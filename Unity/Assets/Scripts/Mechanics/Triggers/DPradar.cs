﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tkingless.DorisPuzzle.constants;
using Tkingless.DorisPuzzle.Models;
using Tkingless.DorisPuzzle.Util;
using UnityEngine;

public class DPradar : MonoBehaviour
{
	#region commons

	/// <summary>
	/// The parent blk.
	/// </summary>
	public DPblock parentBlk;

	#region interface to Unity callbacks

	delegate void SelectedMemAct (Collider2D engaged);

	SelectedMemAct selOnEnterDele, selOnStayDele, selOnExitDele;

	void OnTriggerEnter2D (Collider2D free)
	{
		if (parentBlk == DPBlockManager.Selected && onOffLever != null) {
			selOnEnterDele (free);
		}
	}

	void OnTriggerStay2D (Collider2D engaged)
	{
		if (parentBlk == DPBlockManager.Selected && onOffLever != null) {
			selOnStayDele (engaged);
		}
	}

	void OnTriggerExit2D (Collider2D engaged)
	{
		if (parentBlk == DPBlockManager.Selected && onOffLever != null) {
			selOnExitDele (engaged);
		}
	}

	#endregion

	/// <summary>
	/// Gets a value indicating whether this <see cref="DPradar"/> is engaged.
	/// </summary>
	/// <value><c>true</c> if engaged; otherwise, <c>false</c>.</value>
	public bool Engaged {
		get {
			return parentBlk.Engaged;
		}
	}

	/// <summary>
	/// Gets a value indicating whether this <see cref="DPradar"/> is free.
	/// </summary>
	/// <value><c>true</c> if free; otherwise, <c>false</c>.</value>
	public bool Free {
		get {
			return parentBlk.Free;
		}
	}

	RadarCountpler onOffLever;

	public void TurnOnTriggs ()
	{
		parentBlk.GetDPTriggs.ForEach (x => {
			//Design all the callback are virtually for the selected block
			DPBlockManager.StartListening (DPconstant.DP_TRIG_CHANGED, x.OnPriorityUpdated);
			DPBlockManager.StartListening (DPconstant.DP_TRIG_CHANGED, x.OnTchUpdatedScanDar);

			DPBlockManager.StartListening (DPconstant.DP_TRIG_PTR_LVE_DECHL, x.OnTrigPtrLveDechannelled);
			DPBlockManager.StartListening (DPconstant.DP_TRIG_PHYS_LVE_DECHL, x.OnTrigPhysLveDechannelled);
			DPBlockManager.StartListening (DPconstant.DP_TRIG_SLOT_DECHL, x.OnTrigSlottedDechanneled);
			DPBlockManager.StartListening (DPconstant.DP_TRIG_GRP_DECHL, x.OnTrigGroupedDechanneled);

			x.SelOnChannelled ();
		});
	}

	public void TurnOffTriggs ()
	{
		parentBlk.GetDPTriggs.ForEach (x => {

			x.SelOnDechannel ();

			DPBlockManager.StopListening (DPconstant.DP_TRIG_CHANGED, x.OnPriorityUpdated);
			DPBlockManager.StopListening (DPconstant.DP_TRIG_CHANGED, x.OnTchUpdatedScanDar);

			DPBlockManager.StopListening (DPconstant.DP_TRIG_PTR_LVE_DECHL, x.OnTrigPtrLveDechannelled);
			DPBlockManager.StopListening (DPconstant.DP_TRIG_PHYS_LVE_DECHL, x.OnTrigPhysLveDechannelled);
			DPBlockManager.StopListening (DPconstant.DP_TRIG_SLOT_DECHL, x.OnTrigSlottedDechanneled);
			DPBlockManager.StopListening (DPconstant.DP_TRIG_GRP_DECHL, x.OnTrigGroupedDechanneled);
		});
	}

	#endregion

	#region selected blk funcs

	void selOnEnterRad (Collider2D free)
	{
		DPradar nonSelTrig = free.GetComponent<DPradar> ();
		
		if (nonSelTrig == null)
			return;
		
		if (nonSelTrig.Free) {
			SelOnFree (nonSelTrig);
		}
	}

	void selOnStayRad (Collider2D engaged)
	{
		DPradar nonSelTrig = engaged.GetComponent<DPradar> ();
		
		if (nonSelTrig == null)
			return;
		
		if (nonSelTrig.Free) {
			SelOnFree (nonSelTrig);
		} else if (nonSelTrig.Engaged) { //Better added else if
			nonSelTrig.OnEngaging ();
		}
	}

	void selOnExitRad (Collider2D engaged)
	{
		DPradar nonSelTrig = engaged.GetComponent<DPradar> ();
		
		if (nonSelTrig == null)
			return;
		
		if (nonSelTrig.Engaged) {
			SelOffEng (nonSelTrig);
		}
	}

	/// <summary>
	/// Inits the sel radared.
	/// </summary>
	public void InitSelRadared ()
	{
		BindSelDelegates ();
		onOffLever = new RadarCountpler (OnSelLever, OffSelLever);
	}

	/// <summary>
	/// Terms the sel radared.
	/// </summary>
	public void TermSelRadared ()
	{
		if (onOffLever.Destroyable) {
			onOffLever = null;
		} else {
			//Debug.LogWarning ("Selected onOffLever not Destoryable, wt mean? the action never engaged a free blk");
		}
		DeBindSelDelegates ();
	}

	void BindSelDelegates ()
	{
		selOnEnterDele = selOnEnterRad;
		selOnStayDele = selOnStayRad;
		selOnExitDele = selOnExitRad;
	}

	void DeBindSelDelegates ()
	{
		selOnEnterDele = null;
		selOnStayDele = null;
		selOnExitDele = null;
	}

	void SelOnFree (DPradar _nonSelRad)
	{
		onOffLever.LeverOn (_nonSelRad);
		_nonSelRad.parentBlk.SetState (DPblock.BLK_STATE.Engaged);
		DPBlockManager.TriggerEvent (DPconstant.DP_RADAR_ON, _nonSelRad.parentBlk);
		_nonSelRad.parentBlk.uiObj.OnEngagedInduced ();
	}

	void SelOffEng (DPradar _nonSelRad)
	{
		_nonSelRad.parentBlk.uiObj.OffEngagedInduced ();
		DPBlockManager.TriggerEvent (DPconstant.DP_RAD_PHYS_LEAVE, _nonSelRad.parentBlk);
		onOffLever.LeverOff (_nonSelRad);
	}

	//executed when sel first encounter free blk
	bool OnSelLever (DPradar _nonSelRad)
	{
		//the input _nonSelRad has no use, for engaged blk only
		DPBlockManager.StartListening (DPconstant.DP_GSTR_RAD_PTR_LEAVE, OnPtrLeaveSeleted);
		TurnOnTriggs ();

		return false;
	}

	//executed when sel removed all DPradar in the RadarCountpler
	bool OffSelLever (DPradar _nonSelRad)
	{
		//the input _nonSelRad has no use, for engaged blk only
		TurnOffTriggs ();
		DPBlockManager.StopListening (DPconstant.DP_GSTR_RAD_PTR_LEAVE, OnPtrLeaveSeleted);
		return false;
	}

	/// <summary>
	/// Raises the ptr leave seleted event.
	/// </summary>
	/// <param name="selected">Selected.</param>
	public void OnPtrLeaveSeleted (DPblock selected)
	{
		if (parentBlk.IsSelected && Free) {
			DPBlockManager.TriggerEvent (DPconstant.DP_RAD_MASS_PTR_LEAVE);
			OnTriggsFlushed ();
		}
	}

	public void OnGrouped ()
	{
		if (onOffLever != null) {
			//case that select blk swithced from default substate to grouped
			DPBlockManager.TriggerEvent (DPconstant.DP_GRP_SWITCH_ON);
			OnTriggsFlushed ();
			TermSelRadared ();
		}else {
			//InitSelRadared not called for PtrIn an occupied blk
		}
	}

	public void OffGrouped ()
	{
		if (onOffLever == null) {
			InitSelRadared ();
		} else {
			Debug.LogWarning ("logical exception");
		}
	}

	public void OnSlotted ()
	{
		if (parentBlk.IsSelected && parentBlk.IsActioning) {
			DPBlockManager.TriggerEvent (DPconstant.DP_RAD_MASS_ON_SLOTTED);
			OnTriggsFlushed ();
			gameObject.SetActive (false);
		} else {
			Debug.LogError ("Not proper use");
		}
	}

	public void OffSlotted ()
	{
		gameObject.SetActive (true);
	}

	/// <summary>
	/// Exclusive to selected blk
	/// </summary>
	void OnTriggsFlushed ()
	{
		//this func need to be reconsidered, need to set null? No need, as long as
		//onOffLever only instantiated onRegistered

		//clear all remembered engaged, and turn off selected triggers' listening
		onOffLever.ForceOff ();
	}

	#endregion

	#region func from engaged blk perspective

	/// <summary>
	/// The e domain.
	/// </summary>
	/// <summary>
	/// The p domain.
	/// </summary>
	public List<PolygonCollider2D> eDomain, pDomain;

	Radar _radar;

	/// <summary>
	/// Raises the engaged event.
	/// </summary>
	/// <param name="engBlk">Eng blk.</param>
	public void OnEngaged (DPblock engBlk)
	{
		if (engBlk != parentBlk) {
			return;
		}

		if (!Engaged) {
			return;
		}

		onOffLever = new RadarCountpler (OnEngLever, OffEngLever);
		onOffLever.LeverOn (DPBlockManager.Selected.radObj);

		_radar = new Radar (eDomain, pDomain);

		//start radaring
		if (_radar.ActiveDmnUpdated ()) {
			OnRadarChanged ();
		}
	}

	bool OnEngLever (DPradar _nonSelRad)
	{
		DPBlockManager.StartListening (DPconstant.DP_RAD_MASS_PTR_LEAVE, OnAllEngRadarOff_PtrLeave);
		DPBlockManager.StartListening (DPconstant.DP_RAD_PHYS_LEAVE, OnPhysLeave);
		DPBlockManager.StartListening (DPconstant.DP_RAD_MASS_ON_SLOTTED, OnSlottedLeave);
		DPBlockManager.StartListening (DPconstant.DP_GRP_SWITCH_ON, OnGroupedDechl);
		TurnOnTriggs ();

		return false;
	}

	bool OffEngLever (DPradar _nonSelRad)
	{

		TurnOffTriggs ();
		DPBlockManager.StopListening (DPconstant.DP_RAD_MASS_PTR_LEAVE, OnAllEngRadarOff_PtrLeave);
		DPBlockManager.StopListening (DPconstant.DP_RAD_PHYS_LEAVE, OnPhysLeave);
		DPBlockManager.StopListening (DPconstant.DP_RAD_MASS_ON_SLOTTED, OnSlottedLeave);
		DPBlockManager.StopListening (DPconstant.DP_GRP_SWITCH_ON, OnGroupedDechl);
		return false;
	}

	/// <summary>
	/// Raises the engaging event.
	/// </summary>
	public void OnEngaging ()
	{
		if (!Engaged) {
			return;
		}

		if (_radar.ActiveDmnUpdated ()) {
			OnRadarChanged ();
		} else {

			if (_radar.SelTriggRadarChanged (DPBlockManager.Selected)) {
				OnSelTriggsChanged ();
			}
		}
	}

	#region dechannelling func

	//=======================================================================
	void DechannelPtrLve ()
	{
		DPBlockManager.TriggerEvent (DPconstant.DP_TRIG_PTR_LVE_DECHL, this);
	}

	void DechannelPhysLve ()
	{
		DPBlockManager.TriggerEvent (DPconstant.DP_TRIG_PHYS_LVE_DECHL, this);
	}

	void DechannelSlotted ()
	{
		DPBlockManager.TriggerEvent (DPconstant.DP_TRIG_SLOT_DECHL, this);
	}

	void DechannelGrouped ()
	{
		DPBlockManager.TriggerEvent (DPconstant.DP_TRIG_GRP_DECHL, this);
	}

	//=======================================================================
	//callback of engaged, triggered by only the one selected radar
	void OnAllEngRadarOff_PtrLeave ()
	{
		DechannelPtrLve ();
		parentBlk.uiObj.OffEngagedInduced ();
		DPBlockManager.TriggerEvent (DPconstant.DP_RADAR_OFF, parentBlk);
	}

	void OnPhysLeave (DPblock engagedToOff)
	{

		if (engagedToOff != parentBlk) {
			return;
		}

		DechannelPhysLve ();
		parentBlk.uiObj.OffEngagedInduced ();
		DPBlockManager.TriggerEvent (DPconstant.DP_RADAR_OFF, parentBlk);
	}

	void OnSlottedLeave ()
	{
		DechannelSlotted ();
		parentBlk.uiObj.OffEngagedInduced ();
		DPBlockManager.TriggerEvent (DPconstant.DP_RADAR_OFF, parentBlk);
	}

	void OnGroupedDechl ()
	{
		//tkingless/Grouping: there are engaged blks when onGrouped, the engaged occupied blk need
		//to be fully dechannel
		DechannelGrouped ();
		parentBlk.uiObj.OffEngagedInduced ();
		DPBlockManager.TriggerEvent (DPconstant.DP_RADAR_OFF, parentBlk);
	}
	//=======================================================================

	#endregion

	/// <summary>
	/// called inside Blk.onDegaged()
	/// </summary>
	/// <param name="engBlk">Eng blk.</param>
	public void OnDegaged (DPblock engBlk)
	{
		if (engBlk != parentBlk) {
			return;
		}

		if (!Engaged) {
			Debug.LogError ("OnSelectedExit() should at engaged state still");
			return;
		}

		OnRadarSleep ();

	}

	void OnRadarSleep ()
	{
		_radar = null;

		onOffLever.LeverOff (DPBlockManager.Selected.radObj);

		if (onOffLever.Destroyable) {
			onOffLever = null;
		} else {
			Debug.LogWarning ("Engaged onOffLever not Destoryable, wt mean?");
		}
	}

	void OnRadarChanged ()
	{
		_radar.UpdateEngTrig (parentBlk);
		OnSelTriggsChanged ();
	}

	void OnDeocuppiedForceRadar (DPblock engaged)
	{
		if (parentBlk != engaged)
			return;

		_radar.UpdateEngTrig (parentBlk);
		OnSelTriggsChanged ();
	}

	public void OnSelTriggsChanged ()
	{
		_radar.UpdateSelTrig (DPBlockManager.Selected);

		List<ValidTouchData> listTouch = _radar.ReduceTouchData (this);

		ScanDar SD = new ScanDar (this);
		SD.UpdateSelTrigs (_radar.SelTriggs);
		DPBlockManager.TriggerEvent (DPconstant.DP_TRIG_CHANGED, SD);

		foreach (ValidTouchData td in listTouch) {
//			Debug.Log ("<color=white>td : " + td.ToString ()+"</color>");
			DPBlockManager.TriggerEvent (DPconstant.DP_TRIG_CHANGED, td);
		}
	}

	#endregion


}
