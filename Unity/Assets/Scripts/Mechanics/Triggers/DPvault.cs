﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tkingless.DorisPuzzle.constants;
using UnityEngine;
using Tkingless.DorisPuzzle.Models;

[RequireComponent (typeof(PolygonCollider2D))]
public class DPvault : DPtrigger
{
	public enum EdgeType
	{
		HALF,
		FULL
	}

	public EdgeType type;
	public DPhalf left;
	public DPhalf right;

	public DPColo LeftColo {
		get {
			if (type == EdgeType.FULL) {
				return DPColor;
			}
			return left.DPColor;
		}
	}

	public DPColo RightColo {
		get {
			if (type == EdgeType.FULL) {
				return DPColor;
			}
			return right.DPColor;
		}
	}

	#region excluded triggers

	public DPtrigger Lpt, Lvault, LLpt, Rpt, Rvault, RRpt;
	//also includes the L/R vault's halves

	#endregion

	void Awake ()
	{
//		excludeList = new List<DPtrigger> () {this, left, right, Lpt, Lvault, ((DPvault)Lvault).left, ((DPvault)Lvault).right, LLpt,
//			Rpt, Rvault, ((DPvault)Rvault).left, ((DPvault)Rvault).right, RRpt
//		};
		excludeList = new List<DPtrigger> ();
	}

	protected override void Start ()
	{
		base.Start ();
		markColor = Color.red;
	}

	protected override void GetColo ()
	{
		if (type == EdgeType.FULL) {
			base.GetColo ();
		}
	}

	public override bool IsValidityIgnore {
		get {
			return type == DPvault.EdgeType.HALF;
		}
	}
}

