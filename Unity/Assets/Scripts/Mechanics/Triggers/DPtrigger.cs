﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tkingless.DorisPuzzle.constants;
using Tkingless.DorisPuzzle.Models;
using UnityEngine;

/// <summary>
/// Deal with touching, prioritize point/edge touching
/// one DPtrigger only engaged in point/edge touching one time
/// </summary>
public class DPtrigger : MonoBehaviour
{

	protected new Collider2D collider;
	public Transform pivot;
	public DPblock parentBlk;
	//Initialized in Awake()
	protected List<DPtrigger> excludeList = null;

	public Trigability Triga {
		get {
			return _trig.Triga;
		}
	}

	public ValidTouchData TopPriority {
		get {
			return _trig.TopPrioirty;
		}
	}

	public ValidTouchData ActiveTD {
		get {
			return _trig.ActiveTD;
		}
	}

	protected Trigger _trig;

	public Trigger Trig {
		get {
			return _trig;
		}
	}

	public DPColo DPColor {
		get {
			return Trig.color;
		}
	}

	public bool IsSelected {
		get {
			return (parentBlk == DPBlockManager.Selected);
		}
	}

	public bool IsOccupied {
		get {
			return (Trig.IsOccupied);
		}
	}

	public bool IsFrozen {
		get {
			return Trig.IsFrozen;
		}
	}

	int colorLayerMask = DPconstant.COLOR_LAYER_MASK;
	Vector3 probeNml = Vector3.down;
	float probeLength = 0.2f;
	/// <summary>
	/// DPradar sender record, while still scanned by any
	///  domain of a engaged membrance, the mem in this sender list
	/// for the selected only
	/// </summary>
	RadarMemCountpler radMems;

	public bool StillMemAgainstTD (ValidTouchData td)
	{
		return radMems.GetTheSet.Contains (td.Sender);
	}

	public bool IsTrigRadarByEngOf(DPtrigger engTrig){
		return radMems.GetTheSet.Contains (engTrig.parentBlk.radObj);
	}

	public bool TouchedBy (DPtrigger selected)
	{
		return collider.IsTouching (selected.collider);
	}

	protected virtual void Start ()
	{
		_trig = new Trigger (this, excludeList);
		_trig.AssignTPdebugHdr (CB_DebugDrawTP);

		collider = GetComponent<Collider2D> ();
		GetColo ();
		radMems = new RadarMemCountpler (CB_DebugDrawRadMems, this);
	}

	public void SelOnChannelled ()
	{
		if (IsSelected) {
			_trig.OnChannelled ();
		}

		if (IsSelected) {
			SetInvalidEnterHdr = OnInvalidEngEnter;
			SetInvalidStayHdr = OnInvalidEngEnter;
			SetInvalidExitHdr = OnInvalidEngExit;
		}
	}

	public virtual void OnPriorityUpdated (ValidTouchData tchD)
	{
		if (tchD.Involves (this) && IsSelected) {
			_trig.UpdatePriority (tchD);
		}
	}

	public virtual void OnTchUpdatedScanDar (ScanDar scanDar)
	{
		radMems.OnUpdate (scanDar);

		if (IsSelected) {
			_trig.OnRadMemChanged (radMems.GetTheSet);
		}
	}

	//=======================================================================
	public void OnTrigPtrLveDechannelled (DPradar engagedSender)
	{
		radMems.OnDechannelled (engagedSender);
		if (IsSelected) {
			_trig.OnPtrLeaveDechannelled (engagedSender);
		}
	}

	public void OnTrigPhysLveDechannelled (DPradar engagedSender)
	{
		radMems.OnDechannelled (engagedSender);
		if (IsSelected) {
			_trig.OnPhysLeaveDechannelled (engagedSender);
		}
	}

	public void OnTrigSlottedDechanneled (DPradar engagedSender)
	{
		radMems.OnDechannelled (engagedSender);
		if (IsSelected) {
			//Using same Dechannel way as physical leave
			_trig.OnPhysLeaveDechannelled (engagedSender);
		}
	}

	public void OnTrigGroupedDechanneled(DPradar engagedSender){
		radMems.OnDechannelled (engagedSender);
		if (IsSelected) {
			//Using same Dechannel way as ptr leave
			_trig.OnPtrLeaveDechannelled (engagedSender);
		}
	}

	public void SelOnDechannel (){
		if (IsSelected) {
			SetInvalidEnterHdr = null;
			SetInvalidStayHdr = null;
			SetInvalidExitHdr = null;
		}
	}

	//=======================================================================

	#region trigger CBs

	protected virtual DPtrigger OnTriggerEnter2D (Collider2D other)
	{
		//Filter collision from same parentblk, DPhavles and DPvault keep trigger together
		DPtrigger nonSelTrig = other.GetComponent<DPtrigger> ();
		if (parentBlk == nonSelTrig.parentBlk) {
			return null;
		}

		if (EngEnterHdr != null) {
			nonSelTrig = EngEnterHdr (nonSelTrig);
		}

		if (InvalidationEnterHdr != null) {
			InvalidationEnterHdr (nonSelTrig);
		}

		return nonSelTrig;

	}

	protected virtual DPtrigger OnTriggerStay2D (Collider2D other)
	{
		//Filter collision from same parentblk, DPhavles and DPvault keep trigger together
		DPtrigger nonSelTrig = other.GetComponent<DPtrigger> ();
		if (parentBlk == nonSelTrig.parentBlk) {
			return null;
		}

		if (EngStayHdr != null) {
			nonSelTrig = EngStayHdr (nonSelTrig);
		}

		if (InvalidationStayHdr != null) {
			InvalidationStayHdr (nonSelTrig);
		}

		return nonSelTrig;
	}

	protected virtual DPtrigger OnTriggerExit2D (Collider2D other)
	{
		//Filter collision from same parentblk, DPhavles and DPvault keep trigger together
		DPtrigger nonSelTrig = other.GetComponent<DPtrigger> ();
		if (parentBlk == nonSelTrig.parentBlk) {
			return null;
		}

		if (EngExitHdr != null) {
			nonSelTrig = EngExitHdr (nonSelTrig);
		}

		if (InvalidationExitHdr != null) {
			InvalidationExitHdr (nonSelTrig);
		}

		return nonSelTrig;
	}

	delegate DPtrigger PriotiryEngagedDelegates (DPtrigger trig);

	PriotiryEngagedDelegates EngEnterHdr, EngStayHdr, EngExitHdr;
	PriotiryEngagedDelegates InvalidationEnterHdr, InvalidationStayHdr, InvalidationExitHdr;

	PriotiryEngagedDelegates SetInvalidEnterHdr {
		set {
//			if (value == null) {
//				Debug.Log ("InvalidationEnterHdr set null");
//			} else {
//				Debug.Log ("InvalidationEnterHdr set");
//			}
			InvalidationEnterHdr = value;
		}
	}

	PriotiryEngagedDelegates SetInvalidStayHdr {
		set{
			InvalidationStayHdr = value;
		}
	}

	PriotiryEngagedDelegates SetInvalidExitHdr {
		set {
//			if (value == null) {
//				Debug.Log ("InvalidationExitHdr set null");
//			} else {
//				Debug.Log ("InvalidationExitHdr set");
//			}
			InvalidationExitHdr = value;
		}
	}
	//handlers for selected blk view

	DPtrigger OnPrioEngEnter (DPtrigger trig)
	{
		if (Trig.TopPrioirty == null) {
			Debug.LogWarning ("OnPrioEngEnter null: " + PrintDebugInfo ());
			DefHdrsToListn ();
			return null;
		}

		if (Trig.TopPrioirty.Involves (trig)) {
			if (Trig.TopPrioirty.PreCheckAssemble ()) {
//				if (!DPBlockManager.Selected.IsOverlapping) {
					if (Trig.TopPrioirty.AllTrigaFree ()) {
						Trig.TopPrioirty.MsgOnConnected ();
						TDflowCtrl.ManUpdate ();
					} else {
//						Debug.LogWarning ("<color=yellow> DQ on connected() </color>");
					}
//				}
			}
		}
		return trig;
	}

	DPtrigger OnPrioEngStay (DPtrigger trig)
	{
		return OnPrioEngEnter (trig);
	}

	DPtrigger OnPrioEngExit (DPtrigger trig)
	{
		return trig;
	}

	DPtrigger OnActiveEngEnter (DPtrigger trig)
	{
		return trig;
	}

	DPtrigger OnActiveEngStay (DPtrigger trig)
	{
		return OnActiveEngExit (trig);
	}

	DPtrigger OnActiveEngExit (DPtrigger trig)
	{

		if (Trig.ActiveTD == null) {
			Debug.LogWarning ("OnActiveEngExit null: " + PrintDebugInfo ());
			DefHdrsToListn ();
			return null;
		}

		if (Trig.ActiveTD.Involves (trig)) {
			if (Trig.ActiveTD.PreCheckDissemble ()) {
				Trig.ActiveTD.MsgOffConnected ();
				TDflowCtrl.ManUpdate ();
			}
		}
		return trig;
	}

	//for DPhalf of full type edge, just be ignored
	//for DPvault of half type edge, just be ingored
	//Check if selected has a TP or activeTD involving the engaged, if yes ignored
	protected virtual DPtrigger OnInvalidEngEnter (DPtrigger trig)
	{
//		Debug.Log ("<color=blue> OnInvalidEngEnter called</color>");
		if (IsValidityIgnore || trig.IsValidityIgnore) {
			return trig;
		}

		//turn on to make less sensitive
//		if (!IsTrigRadarByEngOf (trig)) {
//			return trig;
//		}

		if (Trig.HasWrongTchesContains (trig.Trig)) {
			return trig;
		}

		//Filtering: azxci4q guaranteed no connected
		if (parentBlk.IsConnectedto (trig.parentBlk)) {
			return trig;
		}

		if (TopPriority != null) {
			if (TopPriority.Involves (trig)) {
				return trig;
			}
		}
			
		InvalidTouchData tmp = InValidTouchDataFactory.InvalidalityAnalysis(this.Trig,trig.Trig);
		if (tmp != null) {
//			Debug.Log ("<color=yellow> OnInvalidEngEnter called</color>" +
//				",selected:" + this.PrintDebugInfo() + ",\n eng: " + trig.PrintDebugInfo()); 
			tmp.MsgOnConnected ();
			TDflowCtrl.ManUpdate ();
		} else {
			return trig;
		}


		return trig;
	}

	protected virtual DPtrigger OnInvalidEngExit (DPtrigger trig)
	{
		if (IsValidityIgnore || trig.IsValidityIgnore) {
			return trig;
		}

		if (Trig.AllWrongTchesClear) {
			return trig;
		} else {
			var theWrongTch = Trig.WrongTches.ToList ().
				FindAll (x => x.Involves (this) && x.Involves (trig)).ToList ();

			if (theWrongTch.Count != 1) {
				//bonus tkingless/debug uncomment to see situation
//				Debug.LogWarning ("this is not expected: " + theWrongTch.Count + 
//					", and wrongTches:" + theWrongTch.Count);
				//this can happen when more than 1 wrong tches, 
			}

//			Debug.Log ("<color=yellow> OnInvalidEngExit called</color>"); 
			theWrongTch.ForEach(x=>	x.MsgOffConnected ());
			TDflowCtrl.ManUpdate ();
		}
			
		return trig;
	}

	public virtual bool IsValidityIgnore{
		get{
			return false;
		}
	}

	//By this def, this is called only after state chanaged
	public void DefHdrsToListn ()
	{
		if (IsOccupied) {
			if (TopPriority != null) {
				SetHrdsToListnActive ();
			} else {
				//onFreezed
				ResignAllCBs ();
			}
		} else if (TopPriority != null) {
			SetHdrsToListnPrio ();
		} else {
			ResignAllCBs ();
		}
	}

	void SetHdrsToListnPrio ()
	{
		EngEnterHdr = OnPrioEngEnter;
		EngStayHdr = OnPrioEngStay;
		EngExitHdr = OnPrioEngExit;
	}

	void SetHrdsToListnActive ()
	{
		EngEnterHdr = OnActiveEngEnter;
		EngStayHdr = OnActiveEngStay;
		EngExitHdr = OnActiveEngExit;
	}

	void ResignAllCBs ()
	{
		EngEnterHdr = null;
		EngStayHdr = null;
		EngExitHdr = null;
	}

	#endregion

	#region Selected view

	void Update ()
	{
		DebugDraw ();
	}

	void DebugDraw ()
	{
		radMems.DebugDraw ();
		_trig.DebugDrawPriority ();
		CB_DebugDrawOccupied ();
		CB_DebugDrawExcluded ();
	}

	void CB_DebugDrawRadMems (int cnt)
	{
		Vector3 targetDir = transform.TransformDirection (Vector3.down);
		Debug.DrawRay (transform.position, cnt * 0.1f * targetDir, Color.white);
	}

	protected Color markColor;

	void CB_DebugDrawTP (ValidTouchData tp)
	{

		if (!IsSelected) {
			return;
		}

		tp.connections.ForEach (x => {
			if (x.pair.Contains (Trig)) {
				Vector3 tmpDir = (x.GetOppTrig (Trig).DPobj.transform.position - transform.position);
				Debug.DrawRay (transform.position, tmpDir, markColor);
			}
		});

	}

	void CB_DebugDrawOccupied ()
	{

		if (IsOccupied) {
			Vector3 targetDir = transform.TransformDirection (Vector3.up);
			Debug.DrawRay (transform.position, 0.1f * targetDir, markColor);
		}

	}

	void CB_DebugDrawExcluded ()
	{

		if (Triga.Count > 0) {
			Vector3 targetDir = transform.TransformDirection (Vector3.down);
			Debug.DrawRay (transform.position + 0.3f * targetDir, Triga.Count * 0.1f * targetDir, Color.black);
		}

	}

	#endregion

	#region Utility

	public virtual Vector3 GetTchHeadDir ()
	{
		return GetHeadDirection ();
	}

	public Vector3 GetHeadDirection ()
	{
		return transform.TransformDirection (Vector3.up);
	}

	protected virtual void GetColo ()
	{
		probeNml = transform.TransformDirection (probeNml);

		RaycastHit2D[] colohits = new RaycastHit2D[1];

		//Raycast by colorLayerMask still works, ignoring Physics2D layer collision detection setting
		int i = collider.Raycast (probeNml, colohits, probeLength, colorLayerMask);

		if (i > 0) {
			try {
				Trig.color = colohits [0].collider.GetComponent<DPcolorDef> ().color;
			} catch (NullReferenceException e) {
				Debug.LogError ("The problem should be, this gameobject suppose to be hex,but no " +
				"DPcolorDef component found: " + e);
			}

		} else {
			Debug.LogError ("There may be some mistake in scene setting of DPtrigger, please check");
		}

	}

	public string PrintDebugInfo ()
	{

		if (Trig.TopPrioirty != null) {
			return String.Format ("Blk: {0}, Selected: {1}, TPtype: {2}, trigg: {3}",
				parentBlk.name, (IsSelected), Trig.TopPrioirty.ttype, name);
		}

		return String.Format ("Blk: {0}, Selected: {1}, trigg: {2}",
			parentBlk.name, (IsSelected), name);
		;
	}

	#endregion
}
