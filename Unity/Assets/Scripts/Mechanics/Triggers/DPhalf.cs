﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tkingless.DorisPuzzle.constants;
using UnityEngine;
using Tkingless.DorisPuzzle.Models;

[RequireComponent (typeof(BoxCollider2D))]
public sealed class DPhalf : DPtrigger
{
	public enum HalfSide
	{
		LEFT,
		RIGHT
	}

	public HalfSide sideType;
	// Use this for initialization

	public DPvault vault;

	void Awake ()
	{
		
//		excludeList = new List<DPtrigger> () {vault, vault.left, vault.right, Npt, Nvault, ((DPvault)Nvault).left, ((DPvault)Nvault).right, NNpt,
//			Fpt, FvaultButhalf
//		};
		excludeList = new List<DPtrigger> ();

		if (sideType == HalfSide.LEFT) {
			excludeList.Add (((DPvault)FvaultButhalf).left);
		} else if (sideType == HalfSide.RIGHT) {
			excludeList.Add (((DPvault)FvaultButhalf).right);
		}

	}

	protected override void Start ()
	{
		base.Start ();
		markColor = Color.blue;
	}

	#region excluded triggers

	public DPtrigger Npt, Nvault, NNpt, Fpt, FvaultButhalf;

	#endregion


	public DPhalf counterpart ()
	{
		return sideType == HalfSide.LEFT ? vault.right : vault.left;
	}

	public override Vector3 GetTchHeadDir ()
	{
		return vault.GetTchHeadDir ();
	}

	//From selected view
	public override void OnPriorityUpdated (ValidTouchData tchD)
	{

		if (tchD.Involves (this)) {

			if (vault.type == DPvault.EdgeType.HALF) {
				if (Trig.TopPrioirty != null) {

					/*
			 * special case: eHalf1sHalf not to overwrite e2Half2sHalf 
			 * if both (left & right) still radered by engaged blk 
			 * from selected view
			 */
					if (Trig.TopPrioirty.ttype == TouchingType.e2Half2sHalf)
					if (tchD.ttype == TouchingType.eHalf1sHalf) {
						if (!IsSelected) {
							return;
						}
						if (StillMemAgainstTD (Trig.TopPrioirty) && counterpart ().StillMemAgainstTD (Trig.TopPrioirty)) {
							return;
						}
					}

					if (tchD.ttype == TouchingType.eHalf1sHalf) {
						if (counterpart ().TopPriority != null) {
							ValidTouchData cntTP = counterpart ().TopPriority;
							if (cntTP.ttype == TouchingType.eHalf1sHalf) {
								if (tchD.MinDistance () < cntTP.MinDistance ()) {
									
//									Debug.LogWarning ("<color=orange> did happen, purge cntTP: " + cntTP.ToString() + "</color>");
									cntTP.StepwisePurge ();
								} else {
//									Debug.LogWarning ("<color=orange> did happen, ignore</color>");
								}
							}
						}
					}


					if (Trig.TopPrioirty.ttype == TouchingType.eHalf1sHalf) {
						if (tchD.ttype == TouchingType.e2Half2sHalf) {
//					Debug.Log ("the specific reverse competition and some already Occupied: " + tchD.SomeTrigOccupied());

							/*
			 * special case: cross case, two half1half crossing, handicapping h2h
			 */
							//workaround to jump the check
							if (tchD.SomeTrigOccupied ()) {
								if (IsOccupied) {
//								Debug.Log ("<color=red>should not happen, self urgent stop </color>" + PrintDebugInfo());
									Trig.ActiveTD.StepwisePurge ();
								}

								if (counterpart ().IsOccupied) {
//								Debug.Log ("<color=red>should not happen counterpart urgent stop </color>" + counterpart().PrintDebugInfo());
									counterpart ().Trig.ActiveTD.StepwisePurge ();
								}
							} else {
								if (StillMemAgainstTD (tchD) && counterpart ().StillMemAgainstTD (tchD)) {
									if (TopPriority != null) {
										TopPriority.StepwisePurge ();
									}

									if (counterpart ().TopPriority != null) {
										counterpart ().TopPriority.StepwisePurge ();
									}
								}
							}
						}

					}


					//as always standing from perspectives of halves:
//					TouchingType.eFull2sHalf
//					TouchingType.eHalf1sFull

				}
			}

			_trig.UpdatePriority (tchD);
		}
	}

	public override bool IsValidityIgnore {
		get {
			return vault.type == DPvault.EdgeType.FULL;
		}
	}

}
