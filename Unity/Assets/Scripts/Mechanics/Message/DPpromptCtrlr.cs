﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IDiagloue {
    //ITextMessage Content { get; }
    DPMessage.STATE state { get; }
    string content { get; }
    float duration { get; }
	//void Display();
	//void OverwrittenBy (IDiagloue newDiag);
}

//public interface ITextMessage {
//	string GetText {get;}
//	bool Seasoned {get;}
//}

public class DPpromptCtrlr : SceneSingleton<DPpromptCtrlr> {

	public static bool OnScene = false;

	//TODO tkingless/message make prefab on scene to define the anchor
	public Transform bottomDiagloueAnchor;
	public Transform slotDiagloueAnchor;


	new void Awake() {
		base.Awake ();

		if (GameObject.FindObjectOfType (typeof(DPpromptCtrlr)) != null) {
			Debug.Log ("it is on Scene");
			OnScene = true;
		} else {
			Debug.Log ("it is on Scene");
		}
	}

	List<DPprompt> queue = new List<DPprompt>();

	void Enqueue (DPprompt prmpt){
		queue.Add (prmpt);
	}

	public static void EntryRasie(DPprompt prmpt){
	}

	//TODO tkingless/message: handle the queue and state of prompts obj
}

//TODO tkingless/message: class definition according to thise type 001
public enum MsgType {
	TUTORIAL_PLAIN,
	INVALID_TOUCH,
	VALID_TOUCH,
	GESTURE,
	UNDEF
}

public abstract class DPprompt {
	


	[System.Flags]
	public enum DeathType
	{
		ByTime = 1,
		ByEvent = 2,

		//Or logic is like | (ByTime | ByEvent ).HasFlag(myFlags)
		Both = ByTime | ByEvent
	}

	public float length = 1f;
	public float timeline = 0f;

	//The larger take over: common, warning, danger
	public int Priority = 0;
	//Befored this prompt raised, prewait then if no other higher priority
	public float PreWait = .5f;
	//if 
	public float Duration = 3f;
	public string WitherEvent = "";

	//TODO tkingless/message: check if inherited class share the static int from base/abstrac class
	static int MaxOccurenceTime = 9999;
	//Counted only for succuessful display
	static int occuredTime = 0;

	public bool Repeatable {
		get{
			return occuredTime < MaxOccurenceTime;
		}
	}

}

public interface IPromptRepeatable {
	bool Repeatable{ get;}
}

public class DPpromptFactory{



	public DPprompt Build(){
		//TODO tkingless/message: impl
		return null;
	}
}

//TODO tkingless/message: define types of prompt (state of living)


//TODO tkingless/message: define message string place holders and image assets
