﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Tkingless.DorisPuzzle.constants
{

	public static class DPconstant
	{
		public const int POLYGON_SIDES = 8;

		//this defined constants should be checked in coherent with layers settings
		public const int INDUCE_LAYER_MASK = 1 << 13;
		public const int POINT_LAYER_MASK = 1 << 12;
		public const int VAULT_LAYER_MASK = 1 << 11;
		public const int BOUND_LAYER_MASK = 1 << 10;
		public const int COLOR_LAYER_MASK = 1 << 9;
		public const int HALVES_LAYER_MASK = 1 << 8;

		// ui tinting definition
		public static Color TRANS_HALO_COLOR { get { return new Color (0.394f, 0.98f, 1.0f, 0.6f); } }

		public static Color ROT_HALO_COLOR { get { return new Color (1.0f, 0.784f, 0.271f,0.6f); } }

		public static Color GEST_CUR_INACTVE_TRANSPARENT { get { return new Color (.5f, .5f, .5f, .6f); } }

		public static Color GEST_CUR_ACTVE_TRANSPARENT { get { return new Color (1, 1, 1); } }

		public static Color TRANS_CUR_OMINENT { get { return new Color (0.394f, 0.58f, 1.0f); } }

		public static Color ROT_CUR_OMINENT { get { return new Color (1.0f, 0.784f, 0.271f); } }

		public static Color OVERLAP_HALO_COLOR { get { return  new Color (1.0f, 0.3f, 0.3f); } }

		public static Color SNAP_HALO_COLOR { get { return  new Color (0.3f, 1.0f, 0.3f); } }

		public static Color INDUCED_HALO_COLOR { get { return new Color (0.95f, 0.95f, 0.5f, 0.8f); } }

		public static Color OCCUPIED_HALO_COLOR { get { return new Color (0.0f, 0.95f, 0.0f, 1f); } }

		public static Color INVALIDATED_HALO_COLOR { get { return new Color (1.0f, 0.3f, 0.3f); } }

		public static Color GRP_FORBIDDEN_HALO_COLOR { get { return OVERLAP_HALO_COLOR; } }

		//DPblkMng events
		public readonly static string DP_REG_BLK = "DP_REG_BLK";
		//trigger by DPgesture

		//DPblkMng & DPblock events
		public readonly static string DP_PTR_LEAVE_DEREG_BLK = "DP_PTR_LEAVE_DEREG_BLK";
		//trigger by DPgesture
		public readonly static string DP_JOB_DONE = "DP_JOB_DONE";
		//trigger by DPaction

		//DPblock events
		public readonly static string DP_GSTR_FREE_INZONE_ENTER = "DP_GSTR_FREE_INZONE_ENTER";
		//trigger by DPgesture
		public readonly static string DP_GSTR_FREE_INZONE_EXIT = "DP_GSTR_FREE_OUTZONE_ENTER";
		//trigger by DPgesture
		public readonly static string DP_GSTR_DRAG_BGN = "DP_MSG_GESTURE_DRAG_BEGIN";
		//trigger by DPgesture
		public readonly static string DP_GSTR_DRAGGING = "DP_MSG_GESTURE_DRAGGING";
		//trigger by DPgesture
		public readonly static string DP_GSTR_DRAG_END = "DP_MSG_GESTURE_DRAG_END";
		//trigger by DPgesture

		//GroupChecker & DPcurCtrl eventes
		public readonly static string DP_GRP_DRAG_BGN = "DP_GRP_ACTION_INIT";
		public readonly static string DP_GRP_ACTIONING_ON_FORBIDDEN = "DP_GRP_ACTIONING_ON_FORBIDDEN";
		public readonly static string DP_GRP_ACTIONING_OFF_FORBIDDEN = "DP_GRP_ACTIONING_OFF_FORBIDDEN";
		public readonly static string DP_GRP_DRAG_END = "DP_GRP_ACTION_END";
		//trigger by DPgesture
		public readonly static string DP_GRP_JOB_DONE = "DP_GRP_JOB_DONE";
		//trigger by DPgesture

		//DPblk & DPaction events
		public readonly static string DP_GSTR_PTR_DOWN = "DP_GSTR_PTR_DOWN";
		//trigger by DPgesture

		//GroupChecker events
		public readonly static string DP_BTN_GRP_CLK = "DP_BTN_GRP_CLK";

		//DPaction & DPui events
		public readonly static string DP_OVERLAP_ON = "DP_MSG_OVERLAP_ON";
		//trigger by DPboundary
		public readonly static string DP_OVERLAP_OFF = "DP_MSG_OVERLAP_OFF";
		//trigger by DPboundary

		//DPradar events
		public readonly static string DP_GSTR_RAD_PTR_LEAVE = "DP_GSTR_RAD_PTR_LEAVE";
		//trigger by DPgesture
		public readonly static string DP_RAD_MASS_PTR_LEAVE = "DP_RAD_MASS_PTR_LEAVE";
		public readonly static string DP_RAD_MASS_ON_SLOTTED = "DP_RAD_MASS_ON_SLOTTED";
		public readonly static string DP_GRP_PTR_LEAVE = "DP_GRP_PTR_LEAVE";
		//tkingless/Grouping: this msg ask neighbour occupied blk degaged as far occupied blk
		public readonly static string DP_GRP_SWITCH_ON = "DP_GRP_SWITCH_ON_GRP";
		public readonly static string DP_RAD_PHYS_LEAVE = "DP_RAD_PHYS_LEAVE";
		//trigger by DPradar

		//DPradar & DPui events, Induction means DPradar allow triggers to work
		public readonly static string DP_RADAR_ON = "DP_TRIG_INDUCE_ON";
		//trigger by DPradar

		//DPBlock & DPui events
		public readonly static string DP_RADAR_OFF = "DP_TRIG_INDUCE_OFF";
		//trigger by DPradar

		//DPtrigger events
		public readonly static string DP_TRIG_CHANGED = "DP_TRIG_MSG_CHANGED";
		//trigger by DPradar
		public readonly static string DP_TRIG_CLEAR_DECHL = "DP_TRIG_MSG_EXIT";
		public readonly static string DP_TRIG_PTR_LVE_DECHL = "DP_TRIG_PTR_LVE_DECHL";
		public readonly static string DP_TRIG_PHYS_LVE_DECHL = "DP_TRIG_PHYS_LVE_DECHL";
		public readonly static string DP_TRIG_SLOT_DECHL = "DP_TRIG_SLOT_DECHL";
		public readonly static string DP_TRIG_GRP_DECHL = "DP_TRIG_ON_GRP_DECHL";
		//trigger by DPradar


		#region EDU_MSG
		public readonly static string EDU_GOOD_MSG = "Good touch! You knew the rule well.";
		public readonly static string EDU_BAD_MSG = "Good Try but the touch is not acceptable. Can I show you the rules?";
		#endregion
	}

}
