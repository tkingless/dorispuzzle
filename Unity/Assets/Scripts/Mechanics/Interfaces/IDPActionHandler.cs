﻿using System;

//To be implemented by DPAction, DPUI
public interface IDPActionHandler
{
	void OnActionInit ();

	void OnActionProc ();

	void OnActionEnd ();
}


