﻿using System;
using UnityEngine.EventSystems;


public interface IDPGestureHandler : IPointerEnterHandler,
									IPointerExitHandler,
									IPointerDownHandler,
									IPointerClickHandler,
									IBeginDragHandler, 
									IDragHandler, 
									IEndDragHandler
{
}

