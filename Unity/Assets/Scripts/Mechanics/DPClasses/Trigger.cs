﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using Tkingless.DorisPuzzle.Models;

namespace Tkingless.DorisPuzzle.Models
{
	public class Trigger
	{

		public enum State
		{
			ASSEMBLING,
			ASSEMBLED_META,
			ASSEMBLED_STABLE,
			DISSEMBLING,
			DISSEMBLED_META,
			DISSEMBLED_STABLE,
			UNCONSIDER
		}

		//Def of design state logic
		public State GetState {
			get {

				if (IsOccupied) {

					if (ActiveTD.All_Disconnected ()) {
						return State.DISSEMBLED_META;
					}

					if (ActiveTD.All_Connected ()) {
						return State.ASSEMBLED_STABLE;
					}

					return State.DISSEMBLING;
				}
					
				if (TopPrioirty != null && !IsOccupied) {

					if (TopPrioirty.All_Disconnected ()) {
						return State.DISSEMBLED_STABLE;
					}

					if (TopPrioirty.All_Connected ())
						return State.ASSEMBLED_META;

					return State.ASSEMBLING;
				}

				Debug.Log ("<color=blue>there is state UNCONSIDER</color>");
				return State.UNCONSIDER;
			}
		}

		public bool IsFrozen {
			get {
				if (activeTD != null) {
					if (activeTD.GetState == ValidTDtask.State.Freezed)
						return true;
				}
				return false;
			}
		}

		ValidTouchData topPrioirty;
		public Trigability Triga;

		public ValidTouchData TopPrioirty {
			get {
				return topPrioirty;
			}
		}

		ValidTouchData activeTD;

		public ValidTouchData ActiveTD {
			get {
				return activeTD;
			}
		}

		HashSet<InvalidTouchData> wrongTches;

		public HashSet<InvalidTouchData> WrongTches{
			get{
				return wrongTches;
			}
		}

		public InvalidTouchData AddWrongTch {
			set {
				wrongTches.Add (value);
			}
		}

		public InvalidTouchData RmWrongTch {
			set {
				wrongTches.Remove (value);
			}
		}

		public void PurgeAllWrongTches () {
				wrongTches.ToList ().ForEach (x => x.MsgOffConnected ());
				TDflowCtrl.ManUpdate ();
		}

		public bool AllWrongTchesClear {
			get {
				return wrongTches.Count == 0;
			}
		}

		public bool HasWrongTchesContains(Trigger eng){
			return wrongTches.Any (x => x.Involves (eng.DPobj));
		}

		public bool IsOccupied {
			get {
				return (activeTD != null);
			}
		}

		public DPtrigger DPobj;
		public DPColo color = DPColo.Undefined;

		public Trigger (DPtrigger _obj, List<DPtrigger> _excludeList)
		{
			DPobj = _obj;
			Triga = new Trigability (_excludeList, this);
			wrongTches = new HashSet<InvalidTouchData> ();
		}

		public bool TouchedBy (Trigger other)
		{
			return DPobj.TouchedBy (other.DPobj);
		}

		public void OnConnected ()
		{
			activeTD = topPrioirty;
			Triga.OnOccupied (activeTD);

			DPobj.parentBlk.uiObj.OnEngagedOccupied ();
//			Debug.Log ("<color=cyan>This " + DPobj.PrintDebugInfo () + " on Connected()</color>");
		}

		public void OnDisconnected ()
		{
			Triga.OffOccupied (activeTD);
			activeTD = null;

			DPobj.parentBlk.uiObj.OffEngagedOccupied ();
//			Debug.Log ("<color=cyan>This " + DPobj.PrintDebugInfo () + " on Disconnected()</color>");
		}

		public void OnInvalidTched ()
		{
			if (!DPobj.IsSelected) {
				if (!AllWrongTchesClear) {
					DPobj.parentBlk.uiObj.OnEngagedInvalidated ();
				}
			} else {
				//Deal with action part
				if (DPobj.parentBlk.IsInvalidColoTched) {
					DPobj.parentBlk.ActiveAction.OnInvalidColorTched ();
				}

				if (DPobj.parentBlk.IsInvalidGeomTched) {
					DPobj.parentBlk.ActiveAction.OnInvalidGeomTched ();
				}
			}
		}

		public void OffInvalidTched ()
		{
			if (!DPobj.IsSelected) {
				if (AllWrongTchesClear) {
					DPobj.parentBlk.uiObj.OffEngagedInvalidated ();
				}
			} else {
				
				if (!DPobj.parentBlk.IsInvalidColoTched) {
					DPobj.parentBlk.ActiveAction.OffInvalidColorTched ();
				}

				if (!DPobj.parentBlk.IsInvalidGeomTched) {
					DPobj.parentBlk.ActiveAction.OffInvalidGeomTched ();
				}
			}
		}

		public void OnRadMemChanged (HashSet<DPradar> updated)
		{
			if (TopPrioirty != null) {
				
				if (!updated.Contains (TopPrioirty.Sender)) {

					TopPrioirty.MsgOffPrio ();
					TDflowCtrl.ManUpdate ();
				}
			}
		}

		public void OnChannelled ()
		{
			if (IsOccupied) {

				activeTD.MsgOffFreezed ();
				TDflowCtrl.ManUpdate ();

			}
		}

		public void OnPtrLeaveDechannelled (DPradar _sender)
		{
			if (!AllWrongTchesClear) {
				PurgeAllWrongTches ();
			}

			//bonus tkingless/Debug: somehow, need to purge topPrioirties too?
			
			if (IsOccupied) {

				if (!activeTD.All_Disconnected ()) {
					//ptr leave when free;
					activeTD.MsgOnFreezed ();
					TDflowCtrl.ManUpdate ();
				}

			} else {

				if (TopPrioirty != null) {
					if (TopPrioirty.Sender == _sender) {
						//ptr leave when free
						TopPrioirty.MsgOffPrio ();
						TDflowCtrl.ManUpdate ();
					}
				}

			}
		}

		public void OnPhysLeaveDechannelled (DPradar _sender)
		{

			//just a safe check
			if (!DPBlockManager.Selected.IsActioning) {
				Debug.Log ("supposed to be actioning");
			}

			if (!AllWrongTchesClear) {
				PurgeAllWrongTches ();
			}
			//bonus tkingless/Debug: somehow, need to purge topPrioirties too?

			//in fact on, when quick drag, the collider can still not be trigger yet, activeTD still all connected
			if (IsOccupied) {

				ValidTouchData theTD = activeTD;

				if (theTD.Sender == _sender) {
					theTD.MsgOffConnected ();

					if (theTD.GetState == ValidTDtask.State.Prioritized) {
						theTD.MsgOffPrio ();
						Debug.Log ("happened !?");
					}

					TDflowCtrl.ManUpdate ();
				}
				
			} else if (TopPrioirty != null) {
				
				if (TopPrioirty.Sender == _sender) {
					//ptr leave when free
					TopPrioirty.MsgOffPrio ();
					TDflowCtrl.ManUpdate ();
				}
			}
		}

		public void UpdatePriority (ValidTouchData newCandidate)
		{
			ValidTouchData existing = TopPrioirty;

			if (!newCandidate.Involves (DPobj))
				return;

			if (existing == newCandidate) {
				//case that other related Trigger recevied updateMsg again
				return;
			}

			if (existing == null) {
				
				if (newCandidate.AllFreeTP ()) {
					newCandidate.MsgOnPrio ();
					TDflowCtrl.ManUpdate ();
				} else {
					//case for 1full1
					var list = newCandidate.
						Triggs.FindAll (x => x.TopPrioirty != null);
					HashSet<ValidTouchData> TP = new HashSet<ValidTouchData> ();
					list.ForEach (x => {
						TP.Add (x.topPrioirty);
					});

					if (TP.Count > 1 || TP.Count == 0) {
						//9asd192 uncomment to see the special case, workarounded to avoid
//						Debug.LogError ("<color=red>Going crazy, what case? newCand: " + newCandidate + "</color>");
//						TP.ToList ().ForEach (x => Debug.LogError ("one of TP: " + x));
					} else if (TP.Count == 1) {
						TDflowCtrl.PrioOverwrite (newCandidate, TP.First ());
						TDflowCtrl.ManUpdate ();
					}
				}

			} else {

				if (existing.ttype == newCandidate.ttype) {
					return;
				}

				TDflowCtrl.PrioOverwrite (newCandidate, existing);
				TDflowCtrl.ManUpdate ();
			}
		}

		public void SetTP (ValidTouchData td)
		{
			topPrioirty = td;
		}

		public void CB_ClearTP (ValidTouchData verify)
		{
//			Debug.Log ("CB_ClearTP(): " + DPobj.PrintDebugInfo());

			if (verify != TopPrioirty) {
				
				if (TopPrioirty == null) {
					Debug.LogWarning ("TopPriority is null.");
				}

				if (verify == null) {
					Debug.Log ("<color=yellow> verify is null. </color>");
				}
				Debug.LogError (String.Format ("tpIdx: {0}, verifyIdx: {1}",
					TDflowCtrl.GetTDorder (TopPrioirty), TDflowCtrl.GetTDorder (verify)));

				return;
			}

			topPrioirty = null;
		}

		#region DebugDraw

		public delegate void voidTchData (ValidTouchData td);

		voidTchData tpDebugHdr;

		public void AssignTPdebugHdr (voidTchData cb)
		{
			tpDebugHdr = cb;
		}

		public void DebugDrawPriority ()
		{
			if (TopPrioirty != null) {
				tpDebugHdr (TopPrioirty);
			}
		}

		#endregion
	}

	public class Trigability
	{

		public enum State
		{
			FREE,
			EXCLUDED,
			OCCUPIED
		}

		State _state;

		public State GetState {
			get {
				if (host.IsOccupied) {
					_state = State.OCCUPIED;
					return _state;
				}

				if (countpler.Count > 0) {
					_state = State.EXCLUDED;
				}

				if (countpler.Count == 0) {
					_state = State.FREE;
				}

				return _state;
			}
		}

		public int Count {
			get {
				return countpler.Count;
			}
		}

		public bool IsExcluded {
			get {
				return (GetState == State.EXCLUDED);
			}
		}

		public bool IsFree {
			get {
				return (GetState == State.FREE);
			}
		}

		public bool IsOccupied {
			get {
				return (GetState == State.OCCUPIED);
			}
		}

		List<DPtrigger> excludeList;
		TrigaCountpler countpler;
		Trigger host;

		public Trigability (List<DPtrigger> _excludeList, Trigger _host)
		{
			
			if (_excludeList == null || _host == null) {
				Debug.LogError ("Should not be null");
			}

			countpler = new TrigaCountpler ();
			excludeList = _excludeList;
			host = _host;
		}

		#region Exertion

		public void OnOccupied (ValidTouchData activeTD)
		{
			excludeList.ForEach (x => {
				x.Triga.AddExcludeMark (activeTD);
			});
		}

		public void OffOccupied (ValidTouchData activeTD)
		{
			excludeList.ForEach (x => {
				x.Triga.DeductExcludeMark (activeTD);
			});

		}

		#endregion

		#region To be exerted

		public void AddExcludeMark (ValidTouchData activeTD)
		{
			if (activeTD.Involves (host.DPobj))
				return;

			countpler.Add (activeTD);
		}

		public void DeductExcludeMark (ValidTouchData activeTD)
		{

			if (activeTD.Involves (host.DPobj))
				return;

			countpler.Remove (activeTD);
		}

		#endregion
	}
}

