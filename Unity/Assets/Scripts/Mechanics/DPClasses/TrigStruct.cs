﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Tkingless.DorisPuzzle.Models
{
	//Bonus make these classes serialized for easier debugging

	[System.Serializable]
	class TrigStruct
	{

		List<DPtrigger> triggs;

		public List<DPtrigger> Triggs {
			get {
				return triggs;
			}
		}

		public TrigStruct (List<DPtrigger> _triggs)
		{
			triggs = _triggs;
		}

		public bool Equals (TrigStruct other)
		{
			return (triggs.SequenceEqual (other.Triggs));
		}

		public List<DPtrigger> GetAll<T> ()
		{
			return triggs.FindAll (trig => trig.GetType () == typeof(T));
		}

		public List<DPvault> GetFullSideMatchable (DPColo colo)
		{
			List<DPvault> result = new List<DPvault> ();
			foreach (DPvault v in GetAll<DPvault>()) {
				if (v.type == DPvault.EdgeType.FULL) {
					if (v.DPColor == colo)
						result.Add (v);
				}

				if (v.type == DPvault.EdgeType.HALF) {
					if (v.LeftColo == v.RightColo)
					if (v.LeftColo == colo) {
						result.Add (v);
					}
				}
			}
			return result;
		}

		public List<DPvault> GetFourHalves (DPColo Lcolo, DPColo Rcolo)
		{
			List<DPvault> result = new List<DPvault> ();
			foreach (DPvault v in GetAll<DPvault>()) {
				if (v.type == DPvault.EdgeType.HALF) {
					if (v.LeftColo == Rcolo)
					if (v.RightColo == Lcolo) {
						result.Add (v);
					}
				}
			}
			return result;
		}

		public List<DPvault> GetFullTypeVault (DPColo colo)
		{
			List<DPvault> result = new List<DPvault> ();
			foreach (DPvault v in GetAll<DPvault>()) {
				if (v.type == DPvault.EdgeType.FULL) {
					if (v.DPColor == colo) {
						result.Add (v);
					}
				}
			}
			return result;
		}

		public List<DPhalf> GetHalfFromHalfVault ()
		{
			List<DPhalf> result = new List<DPhalf> ();
			foreach (DPvault v in GetAll<DPvault>()) {
				if (v.type == DPvault.EdgeType.HALF) {
					result.Add (v.left);
					result.Add (v.right);
				}
			}
			return result;
		}

		public List<DPhalf> GetMatchRightsOf (DPColo colo)
		{
			List<DPhalf> result = new List<DPhalf> ();
			foreach (DPhalf h in GetHalfFromHalfVault()) {
				if (h.sideType == DPhalf.HalfSide.RIGHT)
				if (h.DPColor == colo) {
					result.Add (h);
				}
			}
			return result;
		}

		public List<DPhalf> GetMatchLeftsOf (DPColo colo)
		{
			List<DPhalf> result = new List<DPhalf> ();
			foreach (DPhalf h in GetHalfFromHalfVault()) {
				if (h.sideType == DPhalf.HalfSide.LEFT)
				if (h.DPColor == colo) {
					result.Add (h);
				}
			}
			return result;
		}


		public List<DPhalf> GetRightsOf (DPColo colo)
		{
			List<DPhalf> result = new List<DPhalf> ();
			foreach (DPhalf h in GetAll<DPhalf>()) {
				if (h.vault.type == DPvault.EdgeType.HALF)
				if (h.sideType == DPhalf.HalfSide.RIGHT)
				if (h.DPColor == colo) {
					result.Add (h);
				}
			}
			return result;
		}

		public List<DPhalf> GetLeftsOf (DPColo colo)
		{
			List<DPhalf> result = new List<DPhalf> ();
			foreach (DPhalf h in GetAll<DPhalf>()) {
				if (h.vault.type == DPvault.EdgeType.HALF)
				if (h.sideType == DPhalf.HalfSide.LEFT)
				if (h.DPColor == colo) {
					result.Add (h);
				}
			}
			return result;
		}
	}
		
}
