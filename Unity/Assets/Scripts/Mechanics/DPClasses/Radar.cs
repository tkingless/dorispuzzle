﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Tkingless.DorisPuzzle.Models
{
	[System.Serializable]
	class Radar
	{

		public enum TrigDomain
		{
			unset,
			e,
			p
		}

		/// <summary>
		/// Since point and edge domain should only rader their own type triggers
		/// </summary>
		public TrigDomain domainTypeIn = TrigDomain.unset;

		private PolygonCollider2D _radar = null;

		//to be initiailized
		List<PolygonCollider2D> eDomain, pDomain;

		TrigStruct selTriggs, engTriggs;

		public List<DPtrigger> SelTriggs {
			get {
				return selTriggs.Triggs;
			}
		}

		public PolygonCollider2D ActiveDmn {
			get {
				return _radar;
			}
			set {
				_radar = value;
				if (eDomain.Contains (_radar)) {
					domainTypeIn = TrigDomain.e;
				} else if (pDomain.Contains (_radar)) {
					domainTypeIn = TrigDomain.p;
				}
			}
		}

		public Radar (List<PolygonCollider2D> _eDomain, List<PolygonCollider2D> _pDomain)
		{
			eDomain = _eDomain;
			pDomain = _pDomain;
		}

		/// <summary>
		/// Returns true if radared domain changed
		/// </summary>
		/// <returns><c>true</c>, if domain was radared, <c>false</c> otherwise.</returns>
		public bool ActiveDmnUpdated ()
		{
			Vector3 selectedPos = DPBlockManager.Selected.transform.position;

			//Buffer check
			if (ActiveDmn != null) {
				if (ActiveDmn.OverlapPoint (selectedPos)) {
					return false;
				}
			}

			foreach (PolygonCollider2D p in pDomain) {
				if (p.OverlapPoint (selectedPos) && ActiveDmn != p) {
					ActiveDmn = p;
					return true;
				}
			}

			foreach (PolygonCollider2D e in eDomain) {
				if (e.OverlapPoint (selectedPos) && ActiveDmn != e) {
					ActiveDmn = e;
					return true;
				}
			}

			if (ActiveDmn == null) {
				//TODO tkingless/Debug: this can be triggered when slot ocassionally
				Debug.LogError ("No domain radared");
			}

			return false;
		}

		List<DPtrigger> ScanDPtriggs (PolygonCollider2D inRad, DPblock blk)
		{
			List<DPtrigger> triggs = new List<DPtrigger> ();

			foreach (DPtrigger t in blk.GetDPTriggs) {

				if (inRad == null) {
					Debug.Log ("this is null");
				}

				if (!inRad.OverlapPoint (t.transform.position))
					continue;

				if ((domainTypeIn == TrigDomain.e) && (t.GetType () == typeof(DPvertex)))
					continue;

				if ((domainTypeIn == TrigDomain.p) && (t.GetType () == typeof(DPvault)))
					continue;

				if ((domainTypeIn == TrigDomain.p) && (t.GetType () == typeof(DPhalf)))
					continue;

				triggs.Add (t);
			}

			return triggs;
		}

		void FilterOutwardsTrigs (PolygonCollider2D inRad, ref List<DPtrigger> trigs)
		{
			Vector3 radarHead = inRad.transform.TransformDirection (Vector3.up);

			trigs.RemoveAll (trig => Vector3.Dot (radarHead, trig.GetHeadDirection ()) >= -1 * Mathf.Cos (40f / 180f * Mathf.PI));
		}

		public bool SelTriggRadarChanged (DPblock Selected)
		{
			bool changed = false;

			TrigStruct tmp = MapSelTrig (Selected);

			if (!tmp.Equals (selTriggs)) {
				changed = true;
			}

			return changed;
		}

		public TrigStruct MapEngTrig (DPblock engagedBlk)
		{
			List<DPtrigger> engagedFallen = ScanDPtriggs (ActiveDmn, engagedBlk);
			return new TrigStruct (engagedFallen);
		}

		public TrigStruct MapSelTrig (DPblock selectedBlk)
		{
			List<DPtrigger> selectedFallen = ScanDPtriggs (ActiveDmn, selectedBlk);
			FilterOutwardsTrigs (ActiveDmn, ref selectedFallen);
			return new TrigStruct (selectedFallen);
		}

		public void UpdateEngTrig (DPblock engagedBlk)
		{
			engTriggs = MapEngTrig (engagedBlk);
		}

		public void UpdateSelTrig (DPblock selectedBlk)
		{
			selTriggs = MapSelTrig (selectedBlk);
		}

		/// <summary>
		/// This function defines the game rule
		/// </summary>
		/// <param name="selectedTs">Selected ts.</param>
		/// <param name="engagedTs">Engaged ts.</param>
		public List<ValidTouchData> ReduceTouchData (DPradar _radar)
		{
			List<ValidTouchData> touchData = new List<ValidTouchData> ();
			ValidTouchDataFactory factory = new ValidTouchDataFactory (_radar);

			switch (domainTypeIn) {
			case TrigDomain.e:

				DPvault engVault = (DPvault)engTriggs.GetAll<DPvault> () [0]; //as I know there should only be one Vault
				DPvault.EdgeType edgeT = engVault.type;

				if (edgeT == DPvault.EdgeType.FULL) {

					DPColo toMatchColor = engVault.DPColor;
					selTriggs.GetFullSideMatchable (toMatchColor).ForEach (delegate(DPvault trig) {
						if (trig.type == DPvault.EdgeType.FULL) {
							
							factory.SetType (TouchingType.eFull1sFull);
							factory.AddConnPair (new TrigConnection (trig, engVault));
							touchData.Add (factory.Instantiate ());
						}

						if (trig.type == DPvault.EdgeType.HALF) {
							
							factory.SetType (TouchingType.eFull2sHalf);
							factory.AddConnPair (new TrigConnection (trig.left, engVault));
							factory.AddConnPair (new TrigConnection (trig.right, engVault));
							touchData.Add (factory.Instantiate ());
						}
					});
				} 

				if (edgeT == DPvault.EdgeType.HALF) {

					if (engVault.LeftColo == engVault.RightColo) {
						selTriggs.GetFullTypeVault (engVault.LeftColo).ForEach (delegate(DPvault trig) {
							
							factory.SetType (TouchingType.eHalf1sFull);
							factory.AddConnPair (new TrigConnection (trig, engVault.left));
							factory.AddConnPair (new TrigConnection (trig, engVault.right));
							touchData.Add (factory.Instantiate ());
						});
					}

					selTriggs.GetFourHalves (engVault.LeftColo, engVault.RightColo).ForEach (delegate(DPvault trig) {
						
						factory.SetType (TouchingType.e2Half2sHalf);
						factory.AddConnPair (new TrigConnection (trig.right, engVault.left));
						factory.AddConnPair (new TrigConnection (trig.left, engVault.right));
						touchData.Add (factory.Instantiate ());
					});


				}

				engTriggs.GetAll<DPhalf> ().ForEach (delegate(DPtrigger eTrig) {
					DPhalf eHalf = (DPhalf)eTrig;

					if (eHalf.vault.type == DPvault.EdgeType.FULL)
						return;

					if (eHalf.sideType == DPhalf.HalfSide.LEFT) {
						selTriggs.GetLeftsOf (eHalf.DPColor).ForEach (delegate(DPhalf sTrig) {

							factory.SetType (TouchingType.eHalf1sHalf);
							factory.AddConnPair (new TrigConnection (sTrig, eHalf));
							touchData.Add (factory.Instantiate ());
						});
					} else if (eHalf.sideType == DPhalf.HalfSide.RIGHT) {
						selTriggs.GetRightsOf (eHalf.DPColor).ForEach (delegate(DPhalf sTrig) {

							factory.SetType (TouchingType.eHalf1sHalf);
							factory.AddConnPair (new TrigConnection (sTrig, eHalf));
							touchData.Add (factory.Instantiate ());
						});
					}
				});
					
				break;

			case TrigDomain.p:
				DPvertex engVertex = (DPvertex)engTriggs.GetAll<DPvertex> () [0]; //as I know there should only be one vertex

				selTriggs.GetAll<DPvertex> ().ForEach (delegate(DPtrigger sTrig) {
					if (engVertex.DPColor == sTrig.DPColor) {

						factory.SetType (TouchingType.Point);
						factory.AddConnPair (new TrigConnection (sTrig, engVertex));
						touchData.Add (factory.Instantiate ());
					}
				});
				break;

			default:
				Debug.LogError ("Check domainIn error");
				break;
			}

			return touchData;
		}
	}
}

