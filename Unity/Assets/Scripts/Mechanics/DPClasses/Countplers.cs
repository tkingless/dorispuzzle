﻿using System;
using System.Collections.Generic;
using Tkingless.DorisPuzzle.Util;

namespace Tkingless.DorisPuzzle.Models
{
	[System.Serializable]
	class RadarCountpler : Countpler<DPradar>
	{
		Lever<bool,DPradar> onC, offC;
		public bool Destroyable = false;

		public RadarCountpler (Lever<bool,DPradar>.delegateType onCdele, Lever<bool,DPradar>.delegateType offCdele) : base ()
		{
			//set OnC, offC predicate
			onC = new Lever<bool, DPradar> (onCdele);
			offC = new Lever<bool, DPradar> (offCdele);

			onC.SetPredicate (IsCountZero);
			offC.SetPredicate (IsCountZero);
		}

		//the predicate
		//ref: https://msdn.microsoft.com/en-us/library/bfcke1bz(v=vs.110).aspx
		bool IsCountZero (ICollection<DPradar> mSet)
		{
			return (mSet.Count == 0);
		}

		public bool LeverOn (DPradar free)
		{
			bool levered = false;

			if (onC.Enactable (_set)) {
				levered = true;
				Destroyable = false;

				onC.Enact (free);
			}

			_set.Add (free);

			return levered;
		}

		public bool LeverOff (DPradar engaged)
		{
			bool levered = false;

			_set.Remove (engaged);

			if (offC.Enactable (_set)) {
				levered = true;
				Destroyable = true;

				offC.Enact (engaged);
			}

			return levered;
		}

		public void ForceOff ()
		{
			_set.Clear ();

			Destroyable = true;

			offC.Enact (null);
		}

	}

	[System.Serializable]
	class RadarMemCountpler : Countpler<DPradar>
	{
		public delegate void voidType (int cnt);

		voidType debugDraw;
		DPtrigger host;

		public RadarMemCountpler (voidType _debugDraw, DPtrigger _host) : base ()
		{
			debugDraw = _debugDraw;
			host = _host;
		}

		public void OnUpdate (ScanDar scanDar)
		{

			DPradar sender = scanDar.Sender;
			if (scanDar.IsThisRadared (host)) {
				_set.Add (sender);
			} else {
				if (_set.Contains (sender)) {
					_set.Remove (sender);
				}
			}
			
		}

		public HashSet<DPradar> GetTheSet {
			get {
				return _set;
			}
		}

		public void OnDechannelled (DPradar sender)
		{
			if (_set.Contains (sender)) {
				_set.Remove (sender);
			}
		}

		public bool DebugDraw ()
		{
			bool Enactable = (_set.Count > 0);

			if (Enactable) {
				debugDraw (_set.Count);
			}

			return Enactable;
		}

	}

	[System.Serializable]
	class TrigaCountpler : Countpler<ValidTouchData>
	{

		//the predicate
		bool IsCountZero (ICollection<ValidTouchData> mSet)
		{
			return (mSet.Count == 0);
		}

		public int Count {
			get {
				return _set.Count;
			}
		}

	}
}

