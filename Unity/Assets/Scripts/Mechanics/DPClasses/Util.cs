﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

//there are "delegates" of input and output specific type, "enum" for pointing state and "checking" for each delegate
//ref: DOC ModeUtil.cs

namespace Tkingless.DorisPuzzle.Util
{

	//ref: https://msdn.microsoft.com/en-us/library/bb397687.aspx
	[System.Serializable]
	class Countpler<T>
	{

		protected HashSet<T> _set;

		public Countpler ()
		{
			_set = new HashSet<T> ();
		}

		public bool Add (T ele)
		{
			return _set.Add (ele);
		}

		public bool Remove (T ele)
		{
			return _set.Remove (ele);
		}
		
	}

	[System.Serializable]
	class Lever<Tout,Tin>
	{

		public delegate Tout delegateType (Tin input);

		delegateType dele;
		Predicate<ICollection<Tin>> p;

		public Lever (delegateType _dele)
		{
			dele = _dele;
		}

		public void SetPredicate (Predicate<ICollection<Tin>> _p)
		{
			//Predicate that funcs to enact
			p = _p;
		}

		public bool Enactable (ICollection<Tin> target)
		{
			//either one is right, enact the delegate
			return p (target);
		}

		public Tout Enact (Tin input)
		{
			return dele (input);
		}

	}

	class Mode<T, ID>
	{
		T mObj;
		Type mModeIDType;

		delegate void ModeFunc ();

		ModeFunc[] mInitFunc;
		ModeFunc[] mProcFunc;
		ModeFunc[] mTermFunc;

		int mPrev = -1;
		int mCurr = -1;

		public Mode (T ob)
		{
			mObj = ob;
			mModeIDType = typeof(ID);
			SetUp ();
			mCurr = 0;
		}

		void SetUp ()
		{
			if (mModeIDType.IsEnum) {
				Array ary = Enum.GetValues (mModeIDType);
				int len = ary.Length;
				mInitFunc = new ModeFunc[len];
				mProcFunc = new ModeFunc[len];
				mTermFunc = new ModeFunc[len];

				foreach (int enumVal in ary) {
					string enumName = Enum.GetName (mModeIDType, enumVal);
					mInitFunc [enumVal] = (ModeFunc)Delegate.CreateDelegate (typeof(ModeFunc), mObj, enumName + "_Init", false, false);
					mProcFunc [enumVal] = (ModeFunc)Delegate.CreateDelegate (typeof(ModeFunc), mObj, enumName + "_Proc", false, false);
					mTermFunc [enumVal] = (ModeFunc)Delegate.CreateDelegate (typeof(ModeFunc), mObj, enumName + "_Term", false, false);
				}
			}

		}

		public void Init (Enum modeID)
		{
			Set (modeID);
			init ();
		}

		public void Proc ()
		{
			while (mPrev != mCurr) {
				term ();
				init ();
			}
			proc ();
		}

		public void Term ()
		{
			term ();
		}


		void init ()
		{
			if (mCurr != -1) {
				mPrev = mCurr;
				if (mInitFunc [mCurr] != null)
					mInitFunc [mCurr] ();
			}
		}

		void proc ()
		{
			if (mProcFunc [mCurr] != null)
				mProcFunc [mCurr] ();
		}

		void term ()
		{
			if (mPrev != -1) {
				if (mTermFunc [mPrev] != null)
					mTermFunc [mPrev] ();
			}
			mPrev = -1;
		}

		public static Mode<T, ID> operator ++ (Mode<T, ID> mode)
		{
			++mode.mCurr;
			return mode;
		}

		public Mode<T, ID> Set (Enum modeID)
		{
			mCurr = (int)(ValueType)modeID;
			return this;
		}

		public int Get ()
		{
			return mCurr;
		}
	}

	class UtilFunc
	{

		public static Vector2 TruncV3toV2 (Vector3 V3)
		{
			return new Vector2 (V3.x, V3.y);
		}

		public static Vector2 TransformDirection2D (Transform trans, Vector2 V2)
		{
			return TruncV3toV2 (trans.TransformDirection (V2.x, V2.y, 0));
		}

	}
}


