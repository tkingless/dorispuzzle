﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public enum DPColo
{
	Red,
	Blue,
	Green,
	Undefined
}

[RequireComponent (typeof(Image))]
public class DPcolorDef : MonoBehaviour
{
	[SerializeField]
	public DPColo color;
}
