﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PuzzleSlot : SceneSingleton<PuzzleSlot>
{

	void OnTriggerEnter2D (Collider2D other)
	{
		if(!Validate(other))
			return;

//		Debug.Log ("OnTriggerEnter2D()");
		DPBlockManager.Selected.SetSubState = DPblock.BLK_SUB_STATE.Slotted;
	}

	void OnTriggerExit2D (Collider2D other)
	{
		if(!Validate(other))
			return;

//		Debug.Log ("OnTriggerExit2D()");
		DPBlockManager.Selected.SetSubState = DPblock.BLK_SUB_STATE.Default;
	}

	bool Validate (Collider2D other)
	{
		if (DPBlockManager.Selected != null) {
			//TODO tkingless/Debug: null exception can happen, unknown condition
			DPblock otherBlk;
			if (other.GetComponent<DPboundary> () != null) {
				otherBlk = other.GetComponent<DPboundary> ().parentBlk;
			} else {
//				Debug.LogWarning ("catch this logical excpetion");
				return false;
			}

			if(otherBlk != null)
			if (otherBlk == DPBlockManager.Selected)
			if (otherBlk.IsActioning && otherBlk.GetActType == DPblock.ActType.TRANSLATE) {
				return true;
			}

		}
		return false;
	}

	public static void OnDropPuzzleSlot (Transform selected){
		selected.SetParent (PuzzleSlot.Instance.transform); 
		//workaround
		selected.SetAsLastSibling ();
	}

	public static void OffDropPuzzleSlot (Transform selected){
		selected.SetParent (DPBlockManager.GetPlaygroundAnchor);
		selected.SetAsLastSibling ();
	}

	public static bool CheckExistInScene (){
		return (FindObjectOfType (typeof(PuzzleSlot)) != null);
	}

	public static Transform GetTrans {
		get{
			return Instance.transform;
		}
	}
}
