﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tkingless.DorisPuzzle.constants;

public class DPcurCtrl : MonoBehaviour
{

	DPblock.BLK_SUB_STATE subState {
		get {
			return parentUI.parentBlk.SubState;
		}
	}

	DPUIeffect parentUI;
	public GameObject RotCurGO, TransCurGO, DetachBtnGO;
	Transform detachBtnIcon;
	DPcursorAnim rotCur, transCur;

	public void Setup (DPUIeffect _UI)
	{
		parentUI = _UI;
		detachBtnIcon = DetachBtnGO.transform.FindChild ("icon");
		rotCur = RotCurGO.GetComponent<DPcursorAnim> ();
		transCur = TransCurGO.GetComponent<DPcursorAnim> ();
		DPBlockManager.StartListening (DPconstant.DP_GSTR_DRAG_BGN, OnGrpActionInit);
	}

	public void OnGrpBtnClk ()
	{
		DPBlockManager.TriggerEvent (DPconstant.DP_BTN_GRP_CLK, parentUI.parentBlk);
	}

	public void ConfigByMode ()
	{
		switch (subState) {
		case DPblock.BLK_SUB_STATE.Default:
			SetupDefaultConfig ();
			break;
		case DPblock.BLK_SUB_STATE.Grouped:
			SetupGroupConfig ();
			break;
		case DPblock.BLK_SUB_STATE.Slotted:
			SetupSlotConfig ();
			break;
		default:
			break;
		}
	}

	public void SetupDefaultConfig ()
	{
		rotCur.SetActive (true);
		transCur.SetActive (true);

		TransCurGO.transform.localScale = Vector3.one;

		detachBtnIcon.transform.localRotation = Quaternion.Euler (0f, 0f, 180f);
	}

	public void SetupGroupConfig ()
	{
		transCur.SetActive (true);
		rotCur.SetActive (false);

		TransCurGO.transform.localScale = 2 * Vector3.one;
		detachBtnIcon.transform.localRotation = Quaternion.Euler (0f, 0f, 0f);
	}

	public void SetupSlotConfig ()
	{
		transCur.SetActive (true);
		rotCur.SetActive (false);
	}

	public void SetToggleBtn(bool onOff){
		DetachBtnGO.SetActive (onOff);
	}

	void OnGrpActionInit ()
	{
		DetachBtnGO.SetActive (false);
	}
}
