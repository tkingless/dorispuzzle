﻿using Tkingless.DorisPuzzle.constants;
using Tkingless.DorisPuzzle.Util;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DPUIeffect : MonoBehaviour
{

	public DPblock parentBlk;

	public DPhaloAnim halo;
	public GameObject cursorPrefab;

	#region Selected Effect

	//Runtime vars
	DPcursorAnim rotCur, transCur;
	DPcurCtrl cursorPrefabInstance = null;
	Transform mGestureIn = null;

	public void OnSelected (Transform gestureIn)
	{
		mGestureIn = gestureIn;
		halo.SetActive (true);
		halo.SetColor (Color.clear);

		SetupCursorFrmObj (gestureIn);
		cursorPrefabInstance.ConfigByMode ();
	}

	public void OnDeselected ()
	{
		ReleaseCursors ();
		halo.SetActive (false);
		mGestureIn = null;
	}

	public void RespawnCursor ()
	{
		ReleaseCursors ();
		SetupCursorFrmObj (mGestureIn);
		cursorPrefabInstance.ConfigByMode ();
	}

	public void ShowDetachGOToggle (bool onOff)
	{
		cursorPrefabInstance.SetToggleBtn (onOff);
	}

	public void SetupCursorFrmObj (Transform gestureIn)
	{
		var curInstance = GameObject.Instantiate (cursorPrefab, gestureIn.position,
			                  Quaternion.identity, gestureIn);

		cursorPrefabInstance = curInstance.GetComponent<DPcurCtrl> ();
		cursorPrefabInstance.Setup ((DPUIeffect)this);
		rotCur = cursorPrefabInstance.transform.Find ("rotate").GetComponent<DPcursorAnim> ();
		transCur = cursorPrefabInstance.transform.Find ("translate").GetComponent<DPcursorAnim> ();
	}

	void ReleaseCursors ()
	{
		Destroy (cursorPrefabInstance.gameObject);
		rotCur = null;
		transCur = null;
		cursorPrefabInstance = null;
	}

	DPblock.ActType SetCursorsType {
		set {
			try {
				if (value == DPblock.ActType.TRANSLATE) {
					transCur.OnActive (true);
					rotCur.OnActive (false);
				} else if (value == DPblock.ActType.ROTATE) {
					transCur.OnActive (false);
					rotCur.OnActive (true);
				}
			} catch (System.Exception e) {
				Debug.LogError ("blk: " + parentBlk.name +
				"error: " + e);
			}
		}
	}

	public void OnSwitchAct (DPblock.ActType _type)
	{
		halo.OnSwitchAction (_type);
		SetCursorsType = _type;
	}

	public void OnOverlap ()
	{
		halo.LerpColor (DPconstant.OVERLAP_HALO_COLOR);
	}

	public void OffOverlap ()
	{
		if (DPBlockManager.Selected.IsOverlapping) {
			return;
		}

		halo.ResetColor (parentBlk.GetActType);
	}

	public void OnBeginDrag ()
	{
		if (parentBlk.GetActType == DPblock.ActType.TRANSLATE) {
			transCur.LerpColor (DPconstant.TRANS_CUR_OMINENT);
			rotCur.LerpColor (new Color (1f, 1f, 1f, 0.01f));
		}

		if (parentBlk.GetActType == DPblock.ActType.ROTATE) {
			transCur.LerpColor (new Color (1f, 1f, 1f, 0.01f));
			rotCur.LerpColor (DPconstant.ROT_CUR_OMINENT);
		}
	}

	public void OnEndDrag ()
	{
		SetCursorsType = parentBlk.GetActType;
	}

	#endregion

	#region Engaged effect

	public void OnEngagedOverlap ()
	{

		halo.SetActive (true);
		halo.LerpColor (DPconstant.OVERLAP_HALO_COLOR);
	}

	public void OffEngagedOverlap ()
	{
		//workaround: overwritting for slotted case but overlapping
		if (DPBlockManager.Selected != null)
		if (DPBlockManager.Selected.SubState == DPblock.BLK_SUB_STATE.Slotted) {
			halo.SetActive (false);
			return;
		}

		if (parentBlk.IsOverlapping) {
			//TODO tkingless/Debug: this can happen if offslotting, occasionally, not significant
			Debug.LogError ("Should not happen");
			return;
		}

		if (IsEngConnectedToSel) {
			halo.LerpColor (DPconstant.OCCUPIED_HALO_COLOR);
		} else if (!parentBlk.IsAllTrigPurgeWrgTches) {
			halo.LerpColor (DPconstant.INVALIDATED_HALO_COLOR);
		} else {
			halo.LerpColor (DPconstant.INDUCED_HALO_COLOR);
		}
	}

	public void OnEngagedInduced ()
	{
		
		halo.SetActive (true);
		halo.LerpColor (DPconstant.INDUCED_HALO_COLOR);

		if (IsEngConnectedToSel) {
			halo.LerpColor (DPconstant.OCCUPIED_HALO_COLOR);
		} else {
			halo.LerpColor (DPconstant.INDUCED_HALO_COLOR);
		}
	}

	public void OffEngagedInduced ()
	{

//		halo.ResetColor (parentBlk.GetActType);
		halo.SetActive (false);
	}

	public void OnEngagedOccupied ()
	{

		if (!IsEngConnectedToSel)
			return;

		halo.LerpColor (DPconstant.OCCUPIED_HALO_COLOR);
	}

	public void OffEngagedOccupied ()
	{
		
		if (parentBlk.IsOverlapping) {
			return;
		}

		if (IsEngConnectedToSel)
			return;

		if (parentBlk.IsSelected)
			return;

		halo.LerpColor (DPconstant.INDUCED_HALO_COLOR);
	}

	public void OnEngagedInvalidated ()
	{
		if (IsEngConnectedToSel)
			return;

		halo.LerpColor (DPconstant.INVALIDATED_HALO_COLOR);
	}

	public void OffEngagedInvalidated ()
	{

		if (parentBlk.IsAllTrigPurgeWrgTches) {

			if (!parentBlk.IsOverlapping)
				halo.LerpColor (DPconstant.INDUCED_HALO_COLOR);
		}
	}

	bool IsEngConnectedToSel {
		get {
			return parentBlk.GetDPTriggs.
			FindAll (x => x.IsOccupied).
			FindAll (x => !x.IsSelected).
			Find (x => x.Trig.ActiveTD.HasSomeFromSel) != null;
		}
	}

	#endregion

	#region subState effect

	public void OnSlotted ()
	{
		//workaround: overwritting for slotted case but overlapping
		if (parentBlk.IsOverlapping) {
			halo.LerpColor (DPconstant.TRANS_HALO_COLOR);
		} 
		cursorPrefabInstance.ConfigByMode ();
	}

	public void OffSlotted ()
	{
		cursorPrefabInstance.ConfigByMode ();
	}

	public void OnGrouped ()
	{
		if (parentBlk.IsSelected) {
			cursorPrefabInstance.ConfigByMode ();
			return;
		}

		//engaged part
		halo.SetActive (true);
		halo.LerpColor (DPconstant.OCCUPIED_HALO_COLOR);
	}

	public void OnGroupedForbidden(){
		halo.LerpColor (DPconstant.GRP_FORBIDDEN_HALO_COLOR);
	}

	public void OffGroupedForbidden (){
		halo.LerpColor (DPconstant.OCCUPIED_HALO_COLOR);
	}

	public void OffGrouped ()
	{
		if (parentBlk.IsSelected) {
			cursorPrefabInstance.ConfigByMode ();
			return;
		}

		//engaged part
		//tkingless/Grouping: this is not complete logic
		if (!DPBlockManager.Selected.IsConnectedto (parentBlk))
			halo.SetActive (false);
	}

	public void PtrOffGrouped(){
		
		//engaged part
		halo.SetActive (false);
	}

	#endregion
}
