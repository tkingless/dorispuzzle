﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Tkingless.DorisPuzzle.constants;

public class DPhaloAnim : DPanimBase
{
	protected override void OnEnable ()
	{
		SetColor (Color.clear);
	}

	public void OnSwitchAction (DPblock.ActType type)
	{
		ConfigActColor (type);
		colorLerpTime = 0f;
	}

	public Color GetActColor (DPblock.ActType type){
		
		if (type == DPblock.ActType.TRANSLATE) {
			return DPconstant.TRANS_HALO_COLOR;
		} else if (type == DPblock.ActType.ROTATE) {
			return DPconstant.ROT_HALO_COLOR; 
		}

		Debug.LogWarning ("Strange case happened");
		return DPconstant.TRANS_HALO_COLOR;
	}

	void ConfigActColor (DPblock.ActType _type)
	{
		targetColor = GetActColor (_type);
	}

	public void ResetColor (DPblock.ActType _type)
	{
		ConfigActColor (_type);
		colorLerpTime = 0f;
	}
}
