﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Tkingless.DorisPuzzle.constants;

public class DPanimBase : MonoBehaviour
{
	protected Image image;

	void Awake ()
	{
		image = GetComponent<Image> ();
	}

	protected virtual void OnEnable ()
	{
	}

	protected virtual void OnDisable ()
	{
	}

	public void SetActive (bool val)
	{
		gameObject.SetActive (val);
	}

	/// <summary>
	/// Sorry, Color.clear is not accepted!!!!
	/// </summary>
	/// <param name="target">Target.</param>
	public void LerpColor (Color target)
	{
		targetColor = target;
		colorLerpTime = 0f;
	}

	public void SetColor (Color target)
	{
		image.color = target;
	}

	protected float colorLerpTime = 0.0f;
	protected float LerpTimeLength = 1.0f;
	protected Color targetColor = Color.clear;
	//Color lerp
	void FixedUpdate ()
	{

		if (targetColor.Equals (Color.clear))
			return;

		image.color = Color.LerpUnclamped (image.color, targetColor, colorLerpTime);

		if (colorLerpTime <= LerpTimeLength)
			colorLerpTime += Time.fixedDeltaTime;
	}
}


