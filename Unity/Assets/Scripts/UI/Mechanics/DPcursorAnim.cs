﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tkingless.DorisPuzzle.constants;

public class DPcursorAnim : DPanimBase {

	protected override void OnEnable ()
	{
		SetColor (DPconstant.GEST_CUR_INACTVE_TRANSPARENT);
	}

	public void OnActive (bool val) {
		if (val) {
			LerpColor (DPconstant.GEST_CUR_ACTVE_TRANSPARENT);
		} else {
			LerpColor (DPconstant.GEST_CUR_INACTVE_TRANSPARENT);
		}
	}
}
