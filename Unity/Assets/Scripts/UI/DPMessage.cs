﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DPMessage : MonoBehaviour {

    public Text txt_msg;
    public float duration = 2.0f;
    public enum STATE
    {
        info,
        danger,
        warning
    }

    private STATE _state;
    public STATE state
    {
        set
        {
            foreach (STYLE _s in styles)
                _s.rect.gameObject.SetActive(_s.state == value);
            _state = value;
        }
        get
        {
            return _state;
        }
    }

    [System.Serializable]
    public class STYLE
    {
        public STATE state;
        public RectTransform rect;
    }
    public STYLE[] styles;

    public string text
    {
        get
        {
            return txt_msg.text;
        }
        set
        {
            RectTransform rect = getStyle(state).rect;
            float bg_width = (getExpectedWidth(value) + 3 * 20) / rect.localScale.x; // 20 is the container padding
            rect.sizeDelta = new Vector2(bg_width, rect.sizeDelta.y);
            txt_msg.text = value;


            rightAdjust();

        }
    }

    private STYLE getStyle(STATE state)
    {
        foreach(STYLE _s in styles)
        {
            if (_s.state == state)
                return _s;
        }
        return styles[0];
    }

    private int getExpectedWidth(string value)
    {
        int width = 0;
        
        txt_msg.font.RequestCharactersInTexture(value, txt_msg.fontSize, FontStyle.Normal);
        CharacterInfo characterInfo;
        for (int i = 0; i < value.Length; i++)
        {
            txt_msg.font.GetCharacterInfo(value[i], out characterInfo, txt_msg.fontSize);
            width += characterInfo.advance;
        }
        return width;
    }

    private void rightAdjust()
    {
        var s_rt = getStyle(state).rect;
        var adjust = s_rt.sizeDelta.x * s_rt.localScale.x;
        s_rt.anchoredPosition = new Vector2(s_rt.anchoredPosition.x - adjust, s_rt.anchoredPosition.y);
        var txt_rt = txt_msg.rectTransform;
        txt_rt.anchoredPosition = new Vector2(txt_rt.anchoredPosition.x - adjust, txt_rt.anchoredPosition.y);
    }

    #region Enter & Exit Animation (Fade In Out)

    public void Enter()
    {
        EnterAnimation(txt_msg);
        foreach (STYLE _s in styles)
            EnterAnimation(_s.rect.GetComponent<Image>());
        StartCoroutine("ExitTimeout");
    }

    void EnterAnimation(Graphic ele)
    {
        if (ele.gameObject.activeSelf)
        {
            ele.GetComponent<CanvasRenderer>().SetAlpha(0.0f);
            ele.CrossFadeAlpha(1, 0.25f, true);
        }
    }

    private IEnumerator ExitTimeout()
    {
        yield return new WaitForSeconds(duration);
        Exit();
    }

    void Exit()
    {
        ExitAnimation(txt_msg);
        foreach (STYLE _s in styles)
            ExitAnimation(_s.rect.GetComponent<Image>());
    }

    void ExitAnimation(Graphic ele)
    {
        if (ele.gameObject.activeSelf)
        {
            ele.CrossFadeAlpha(0, 0.25f, true);
            StartCoroutine("ExitAnimationTimeout");
        }
    }

    private IEnumerator ExitAnimationTimeout()
    {
        yield return new WaitForSeconds(0.25f);
        Destroy(gameObject);
    }

    #endregion

}
